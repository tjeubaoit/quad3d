#ifndef PROGRESSRING_H
#define PROGRESSRING_H

#include <QLabel>
#include "customwidgets_global.h"

class ProgressRingPrivate;

class CUSTOMWIDGETS_EXPORT ProgressRing : public QLabel
{
    Q_OBJECT
public:
    explicit ProgressRing(QWidget *parent = 0);
    ~ProgressRing();

    void setPixmapUrl(const QString&);
    void setSpeed(double);

public slots:
    void start();
    void stop();

protected:
    virtual void paintEvent(QPaintEvent*);

protected slots:
    virtual void rotateTimer_timeout();

private:
    ProgressRingPrivate* const d;

};

#endif // PROGRESSRING_H
