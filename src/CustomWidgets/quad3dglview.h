#ifndef QUAD3DGLVIEW_H
#define QUAD3DGLVIEW_H

#include "customwidgets_global.h"

#include <QGLView>

class QGLAbstractScene;
class QGLSceneNode;
class QSurfaceFormat;

class CUSTOMWIDGETS_EXPORT Quad3DGlView : public QGLView
{
    Q_OBJECT
    Q_PROPERTY(float angleX READ angleX WRITE setAngleX)
    Q_PROPERTY(float angleY READ angleY WRITE setAngleY)
    Q_PROPERTY(float angleZ READ angleZ WRITE setAngleZ)
    Q_PROPERTY(float angleWing READ angleWing WRITE setAngleWing)
public:
    enum AngleKind {
        Angle_X = 0x100,
        Angle_Y = 0x010,
        Angle_Z = 0x001
    };

    Quad3DGlView(const QSurfaceFormat &surface, QWindow *parent = 0);
    Quad3DGlView(QWindow *parent = 0);
    ~Quad3DGlView();

    float angleX() const;
    float angleY() const;
    float angleZ() const;
    float angleWing() const;

Q_SIGNALS:
    void _angle_changed(int angleKind, float val);

public Q_SLOTS:
    void setAngleX(float angle);
    void setAngleY(float angle);
    void setAngleZ(float angle);
    void setOriginalAngleZ(float angle);
    void setWingAnimationSpeed(float speed);
    void setAngleWing(float angle);

protected:
    void update();
    void initializeGL(QGLPainter *painter);
    void paintGL(QGLPainter *painter);

    void mouseMoveEvent(QMouseEvent*) {}
    void mousePressEvent(QMouseEvent*) {}
    void mouseReleaseEvent(QMouseEvent*) {}
    void mouseDoubleClickEvent(QMouseEvent*) {}
    void wheelEvent(QWheelEvent*) {}

private:
    class Quad3DGlViewPrivate;
    Quad3DGlViewPrivate* const d;
};

#endif // QUAD3DGLVIEW_H
