#ifndef CUSTOMWIDGETS_GLOBAL_H
#define CUSTOMWIDGETS_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QDebug>

#if defined(CUSTOMWIDGETS_LIBRARY)
#   define CUSTOMWIDGETS_EXPORT Q_DECL_EXPORT
#   define MESSAGE_BOX_TITLE "Apollo Flybarless Configuration"
#   define MESSAGE_BOX_TEXT "A new firmware version is available, do you want to download it ?"
#else
#  define CUSTOMWIDGETS_EXPORT Q_DECL_IMPORT
#endif

#define RX_LOCATION_LEFT -5
#define RX_LOCATION_RIGHT 5

#define PROGRESS_RING_URL "images/progress_ring.png"

#endif // CUSTOMWIDGETS_GLOBAL_H
