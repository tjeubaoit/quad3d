#include "trimswashcyclic.h"
#include "ui_trimswashcyclic.h"
#include <QDebug>
#include <QPropertyAnimation>

#define MAX_TRIM_TYPE 3

class SwashTrimCyclic::SwashTrimCyclicPrivate : public AbstractTrimSwashPrivate {
public:
    ~SwashTrimCyclicPrivate();
    SwashTrimCyclic::TrimType currentTrimType;
    int *trimValue;
};

SwashTrimCyclic::SwashTrimCyclicPrivate::~SwashTrimCyclicPrivate()
{
    delete[] trimValue;
}

SwashTrimCyclic::SwashTrimCyclic(QWidget *parent) :
    AbstractTrimSwash(*new SwashTrimCyclicPrivate, parent),
    ui(new Ui::TrimSwash)
{
    ui->setupUi(this);

    Q_D(SwashTrimCyclic);

    d->currentTrimType = Cyclic;
    d->trimValue = new int[MAX_TRIM_TYPE*2];
    for (int i = 0; i < MAX_TRIM_TYPE*2; i++) {
        d->trimValue[i] = 0;
    }

    updateVisibleByTrimType();
}

SwashTrimCyclic::~SwashTrimCyclic()
{
    delete ui;
    delete d_ptr;
}

void SwashTrimCyclic::setToolTipForClearButton(const QString &tt)
{
    ui->btnClear->setToolTip(tt);
}

void SwashTrimCyclic::setTrimMonitorValue(int channelId, int val)
{
    switch (channelId) {
    case Channel1:
        updateMonitorUI(val, ui->progressCh1_Up, ui->progressCh1_Down);
        break;
    case Channel2:
        updateMonitorUI(val, ui->progressCh2_Up, ui->progressCh2_Down);
        break;
    case Channel3:
        updateMonitorUI(val, ui->progressCh3_Up, ui->progressCh3_Down);
        break;
    case Channel4:
        updateMonitorUI(val, ui->progressCh4_Up, ui->progressCh4_Down);
        break;
    default:
        return;
    }
    emit _monitorChannelValueChanged(channelId, val);
}

void SwashTrimCyclic::setTrimValue(TrimType type, TrimDirection direction,
                                       int val)
{
    Q_D(SwashTrimCyclic);

    const int index = type * 2 + direction;
    d->trimValue[index] = val;

    updateTrimCollectiveMonitorValue();
}

void SwashTrimCyclic::setTrimRange(int min, int max)
{
    AbstractTrimSwash::setTrimRange(min, max);

    const int middle = (max - min)/2 + min;
    ui->progressAileronDown->setRange(0, max - middle);
    ui->progressAileronUp->setRange(0, max - middle);
    ui->progressElevatorDown->setRange(0, max - middle);
    ui->progressElevatorUp->setRange(0, max - middle);
}

void SwashTrimCyclic::setTrimMonitorRange(int min, int max)
{
    QList<QProgressBar*> listProgress;
    listProgress << ui->progressCh1_Down << ui->progressCh1_Up
                    << ui->progressCh2_Down << ui->progressCh2_Up
                    << ui->progressCh3_Down << ui->progressCh3_Up
                    << ui->progressCh4_Down << ui->progressCh4_Up;

    Q_D(SwashTrimCyclic);

    d->monitorMin = min;
    d->monitorMax = max;

    const int middle = (max - min)/2 + min;
    for (int i = 0; i < listProgress.size(); i++) {
        if (i % 2 == 0) {
            listProgress[i]->setRange(0, middle - min);
        }
        else {
            listProgress[i]->setRange(0, max - middle);
        }
    }
}

void SwashTrimCyclic::on_btnClear_clicked()
{
    Q_D(SwashTrimCyclic);

    const qint32 middle = (d->trimMax - d->trimMin)/2 + d->trimMin;

    for (int i = 0; i < MAX_TRIM_TYPE; i++) {
        d->trimValue[i*2] = middle;
        d->trimValue[i*2 + 1] = middle;

        emit _trimValueChanged(i, Horizontal, middle, true);
        emit _trimValueChanged(i, Vertical, middle, true);
    }

    updateTrimCollectiveMonitorValue();
}

void SwashTrimCyclic::on_btnNorth2_clicked()
{
    updateTrimValue(Vertical, 5);
}

void SwashTrimCyclic::on_btnNorth1_clicked()
{
    updateTrimValue(Vertical, 1);
}

void SwashTrimCyclic::on_btnWest2_clicked()
{
    updateTrimValue(Horizontal, -5);
}

void SwashTrimCyclic::on_btnWest1_clicked()
{
    updateTrimValue(Horizontal, -1);
}

void SwashTrimCyclic::on_btnEast2_clicked()
{
    updateTrimValue(Horizontal, 5);
}

void SwashTrimCyclic::on_btnEast1_clicked()
{
    updateTrimValue(Horizontal, 1);
}

void SwashTrimCyclic::on_btnSouth2_clicked()
{
    updateTrimValue(Vertical, -5);
}

void SwashTrimCyclic::on_btnSouth1_clicked()
{
    updateTrimValue(Vertical, -1);
}

void SwashTrimCyclic::updateVisibleByTrimType()
{
    Q_D(SwashTrimCyclic);

    switch (d->currentTrimType) {
    case MaxCollective:
    case MinCollective:
        ui->progressAileronDown->show();
        ui->progressAileronUp->show();
        ui->progressElevatorDown->show();
        ui->progressElevatorUp->show();
        ui->lbAileron->show();
        ui->lbElevator->show();

        break;

    case Cyclic:
        ui->progressAileronDown->hide();
        ui->progressAileronUp->hide();
        ui->progressElevatorDown->hide();
        ui->progressElevatorUp->hide();
        ui->lbAileron->hide();
        ui->lbElevator->hide();

        break;
    }
}

void SwashTrimCyclic::on_btnTrimTypeLeft_clicked()
{
    Q_D(SwashTrimCyclic);

    d->currentTrimType = (TrimType)(
                (d->currentTrimType - 1) < 0 ? 2 : (d->currentTrimType - 1));

    ui->lbMainHeader->setText(getHeaderWithCurrentTrimType());
    updateVisibleByTrimType();
    updateTrimCollectiveMonitorValue();
}

void SwashTrimCyclic::on_btnTrimTypeRight_clicked()
{
    Q_D(SwashTrimCyclic);

    d->currentTrimType = (TrimType)(
                (d->currentTrimType + 1) > 2 ? 0 : (d->currentTrimType + 1));

    ui->lbMainHeader->setText(getHeaderWithCurrentTrimType());
    updateVisibleByTrimType();
    updateTrimCollectiveMonitorValue();
}

QString SwashTrimCyclic::getHeaderWithCurrentTrimType()
{
    Q_D(SwashTrimCyclic);

    if (d->currentTrimType == MaxCollective) return "Max Collective";
    else if (d->currentTrimType == MinCollective) return "Min Collective";
    else if (d->currentTrimType == Cyclic) return "Cyclic";
    else return QString();
}

void SwashTrimCyclic::updateTrimCollectiveMonitorValue()
{
    Q_D(SwashTrimCyclic);

    if (d->currentTrimType != Cyclic) {
        int trimMiddle = (d->trimMax - d->trimMin)/2 + d->trimMin;

        updateMonitorUI(d->trimValue[2 * d->currentTrimType],
                ui->progressAileronUp, ui->progressAileronDown, trimMiddle);

        updateMonitorUI(d->trimValue[2 * d->currentTrimType + 1],
                ui->progressElevatorUp, ui->progressElevatorDown, trimMiddle);
    }
}


void SwashTrimCyclic::updateTrimValue(int direction, int dVal)
{
    Q_D(SwashTrimCyclic);

    int index = 2 * d->currentTrimType + direction;
    int newVal = d->trimValue[index] + dVal;

    if (newVal < d->trimMin) newVal = d->trimMin;
    if (newVal > d->trimMax) newVal = d->trimMax;

    d->trimValue[index] = newVal;

    updateTrimCollectiveMonitorValue();

    emit _trimValueChanged(d->currentTrimType, direction, newVal);
}















