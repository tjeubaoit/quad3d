#ifndef BUTTONGROUP_H
#define BUTTONGROUP_H

#include <QButtonGroup>
#include "customwidgets_global.h"

class QLabel;
class QAbstractButton;

class CUSTOMWIDGETS_EXPORT ButtonGroup : public QButtonGroup {
    Q_OBJECT

public:
    explicit ButtonGroup(const QString&, QWidget*);

    QAbstractButton* lastButtonChecked() const;
    int lastButtonCheckedId() const;

    void setLastButtonChecked(QAbstractButton *lastButtonChecked = NULL);

    QLabel *checkedIcon() const;

protected:
    virtual QPoint getCheckedIconPosition();

private:
    class ButtonGroupPrivate;
    ButtonGroupPrivate* const d;
};

#endif // BUTTONGROUP_H
