#include "buttongroup.h"
#include <QLabel>
#include <QObjectData>
#include <QAbstractButton>

class ButtonGroup::ButtonGroupPrivate {
public:
    QAbstractButton *lastButtonChecked;
    QLabel *checkedIcon;
};

ButtonGroup::ButtonGroup(const QString& url, QWidget *parent)
    : QButtonGroup(parent),
      d(new ButtonGroupPrivate)
{    
    d->checkedIcon = new QLabel(parent);
    d->checkedIcon->setFixedSize(32, 32);
    d->checkedIcon->setPixmap(QPixmap(url));

    d->lastButtonChecked = NULL;
}

QAbstractButton *ButtonGroup::lastButtonChecked() const
{
    return d->lastButtonChecked;
}

int ButtonGroup::lastButtonCheckedId() const
{
    return id(d->lastButtonChecked);
}

void ButtonGroup::setLastButtonChecked(QAbstractButton *lastButtonChecked)
{
    if (lastButtonChecked != 0) {
        d->lastButtonChecked = lastButtonChecked;
        d->checkedIcon->move(getCheckedIconPosition());
        d->checkedIcon->show();
    }
    else {
        d->lastButtonChecked = 0;
        d->checkedIcon->hide();
    }
}

QLabel *ButtonGroup::checkedIcon() const
{
    return d->checkedIcon;
}

QPoint ButtonGroup::getCheckedIconPosition()
{
    QAbstractButton *lastChecked = lastButtonChecked();
    QPoint point(lastChecked->pos().x() + lastChecked->width() - 22,
                 lastChecked->pos().y() - 13);
    return point;
}
