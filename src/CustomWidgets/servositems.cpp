#include "servositems.h"
#include <QColor>
#include <QPen>
#include <QRadialGradient>
#include <QDebug>
#include <QPainter>
#include <math.h>

ServosItem::ServosItem(int parentWidth, int parentHeight)
    : m_center(parentWidth/2, parentHeight/2)
{
    m_servosWidth = parentWidth;
    m_servosHeight = parentHeight;
    m_radius = m_servosWidth/3;
    m_angle = 0;
}

void ServosItem::setAngle(double angle)
{
    m_angle = angle;
    QList<int> emitResult;
    emitResult.append((int) m_angle);

    foreach (const double channelAngle, m_channelAngleList) {
        if (channelAngle < 0) {
            emitResult.append(0);
        }
        else {
            int newAngle = (int) (angle + channelAngle);
            if (newAngle >= 360) {
                newAngle = newAngle - 360;
            }
            emitResult.append(newAngle);
        }
    }

    emit _angleChanged(emitResult);
}

double ServosItem::getAngle() const
{
    return m_angle;
}

void ServosItem::resetChannelDefaultAngle(const QList<int>& listAngle)
{
    m_channelAngleList.clear();
    m_channelPosList.clear();
    m_channelAngleList.append(listAngle);

    for (int i = 0; i < listAngle.size(); i++) {
        if (listAngle.at(i) < 0) {
            m_channelPosList.append(QPointF(0, 0));
            continue;
        }
        m_channelPosList.append(getPositionByAngle(listAngle.at(i)));
    }
}

QRectF ServosItem::boundingRect() const
{
    return QRectF(0, 0, m_servosWidth, m_servosHeight);
}

void ServosItem::paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(Qt::NoPen);

    drawBackground(painter);
    drawChannelItems(painter);
}

void ServosItem::drawBackground(QPainter *painter)
{
    painter->save();

    painter->setBrush(QColor("#D1EEFC"));
    painter->drawEllipse(m_center, m_radius, m_radius);

    QPen pen;
    pen.setWidth(1);
    pen.setBrush(QColor("#191919"));
    painter->setPen(pen);

    QVector<QLineF> lines;
    lines.append(QLineF(m_servosWidth/2, 5, m_servosWidth/2, m_servosHeight - 5));
    lines.append(QLineF(5, m_servosHeight/2, m_servosWidth - 5, m_servosHeight/2));
    painter->drawLines(lines);

    painter->restore();
}

void ServosItem::drawChannelItems(QPainter *painter)
{
    painter->save();
    painter->translate(m_center);
    painter->setBrush(Qt::red);
    painter->setFont(QFont("Segoe UI", 8));

    QPen pen;
    pen.setWidth(1);

    for (int i = 0; i < m_channelPosList.size(); i++) {
        if (m_channelAngleList.at(i) < 0) {
            continue;
        }

        QPointF point = m_channelPosList.at(i);

        painter->save();

        painter->rotate(m_angle);
        painter->drawEllipse(point, 6, 6);

        pen.setBrush(Qt::red);
        painter->setPen(pen);
        painter->drawLine(point, QPointF(0, 0));

        pen.setBrush(Qt::black);
        painter->setPen(pen);

        painter->translate(point.x(), point.y());
        painter->rotate(-m_angle);
        painter->drawText(0, - 10, "Ch" + QString::number(i+1));

        painter->restore();
    }

    painter->restore();
}

QPointF ServosItem::getPositionByAngle(double angle)
{
    double pi = 3.141592654;
    double x = m_radius * sin(pi*angle / 180);
    double y = m_radius * cos(pi*angle / 180) * (-1);
    return QPointF(x, y);
}
