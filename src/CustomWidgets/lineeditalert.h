#ifndef LINEEDITALERT_H
#define LINEEDITALERT_H

#include <QLabel>
#include "customwidgets_global.h"

class CUSTOMWIDGETS_EXPORT LineEditAlert : public QLabel
{
    Q_OBJECT
public:
    explicit LineEditAlert(QWidget *parent = 0);
    ~LineEditAlert();

    void showMe();
    void hideMe();

private slots:
    void tooltipTimer_timeout();

private:
    class LineEditAlertPrivate;
    LineEditAlertPrivate* const d;
};

#endif // LINEEDITALERT_H
