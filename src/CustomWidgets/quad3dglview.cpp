#include "quad3dglview.h"

#include <QGLAbstractScene>
#include <QGLSceneNode>
#include <QSurfaceFormat>
#include <QWindow>
#include <QDebug>
#include <QPainter>

struct Position3D {
    double x;
    double y;
    double z;
};

double pos_data[] = { 0.13787f, 2.48832f, 4.94424,
                      4.95751f, 2.48833f, 0.12459f,
                      0.13787f, 2.48833f, -4.69504f,
                      -4.68177f, 2.48833f, 0.12459f };

void FixNodesRecursive(int matIndex, QGLSceneNode* pNode)
{
    if (pNode) {
        pNode->setMaterialIndex(matIndex);
        pNode->setEffect(QGL::FlatReplaceTexture2D);
        foreach (QGLSceneNode* pCh, pNode->children()) {
            FixNodesRecursive(matIndex, pCh);
        }
    }
}

void TryEnableGlAntialising()
{
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_SMOOTH);
    glEnable (GL_POLYGON_SMOOTH);

    glEnable (GL_POINT_SMOOTH);
    glEnable (GL_LINE_SMOOTH);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
}

class Quad3DGlView::Quad3DGlViewPrivate
{
public:
    QGLAbstractScene *pFrameScene, *pHeadScene;
    QGLAbstractScene *ppWingScene[4];
    Position3D ppWingPosition[4];

    float angle_x, angle_y, angle_z;
    float angleWing, wingAnimSpeed;
    float originalAngle_z;

    ~Quad3DGlViewPrivate()
    {
//        delete pFrameScene;
//        delete pHeadScene;
//        for (int i = 0; i < 4; i++) {
//            delete ppWingScene[i];
//        }
    }
};

Quad3DGlView::Quad3DGlView(const QSurfaceFormat &surface, QWindow *parent)
    : QGLView(surface, parent),
    d(new Quad3DGlViewPrivate)
{
    d->angle_x = 0.0f;
    d->angle_y = 0.0f;
    d->angle_z = 0.0f;
    d->originalAngle_z = 0.0f;
}

Quad3DGlView::Quad3DGlView(QWindow *parent)
    : QGLView(parent),
    d(new Quad3DGlViewPrivate)
{
    d->angle_x = 0.0f;
    d->angle_y = 0.0f;
    d->angle_z = 0.0f;
    d->originalAngle_z = 0.0f;
}

Quad3DGlView::~Quad3DGlView()
{
    delete d;
}

float Quad3DGlView::angleX() const
{
    return d->angle_x;
}

float Quad3DGlView::angleY() const
{
    return d->angle_y;
}

float Quad3DGlView::angleZ() const
{
    return d->angle_z;
}

float Quad3DGlView::angleWing() const
{
    return d->angleWing;
}

void Quad3DGlView::setWingAnimationSpeed(float speed)
{
    d->wingAnimSpeed = speed;
}

void Quad3DGlView::setAngleWing(float angle)
{
    d->angleWing = angle;
}

void Quad3DGlView::update()
{
    if (isVisible()) QGLView::update();
}

void Quad3DGlView::setAngleX(float angle)
{
    d->angle_x = angle;
    update();

    emit _angle_changed(Angle_X, angle);
}

void Quad3DGlView::setAngleY(float angle)
{
    d->angle_y = angle;
    update();

    emit _angle_changed(Angle_Y, angle);
}

void Quad3DGlView::setAngleZ(float angle)
{
    d->angle_z = angle;
    update();

    emit _angle_changed(Angle_Z, angle);
}

void Quad3DGlView::setOriginalAngleZ(float angle)
{
    d->originalAngle_z = angle;
    update();
}

void Quad3DGlView::initializeGL(QGLPainter *painter)
{
    if (!painter) return;
    painter->setClearColor(QColor(250, 250, 250, 0));
    setOpacity(0);

    d->pFrameScene = QGLAbstractScene::loadScene(":/3dmodel/quad_frame.obj");
    d->pHeadScene = QGLAbstractScene::loadScene(":/3dmodel/quad_head.obj");

    Q_ASSERT(d->pFrameScene);
    Q_ASSERT(d->pHeadScene);

    for (int i = 0; i < 4; i++) {
        d->ppWingPosition[i].x = pos_data[i*3];
        d->ppWingPosition[i].y = pos_data[i*3 + 1];
        d->ppWingPosition[i].z = pos_data[i*3 + 2];

        QString objFileName = ":/3dmodel/quad_fly" + QString::number(i+1) + ".obj";
        d->ppWingScene[i] = QGLAbstractScene::loadScene(objFileName);
        Q_ASSERT(d->ppWingScene[i]);
    }

    QGLCamera *ca = camera();
    ca->setFieldOfView(25);
    ca->setNearPlane(1);
    ca->setFarPlane(1000);

    ca->setCenter(QVector3D(0, 0, 0));
    ca->setEye(QVector3D(27.0f, 0.8f, 0));
}

void Quad3DGlView::paintGL(QGLPainter *painter)
{
    if (!painter) return;
    if (!painter->begin()) return;

    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.scale(1.0);
    matrix.rotate((-1) * d->angle_z, 0, 1, 0);
    matrix.rotate(d->angle_x, 0, 0, 1);
    matrix.rotate((-1) * d->angle_y, 1, 0, 0);
    matrix.rotate(d->originalAngle_z, 0, 1, 0);
    matrix.translate(0, -1.2f, 0);
    painter->modelViewMatrix() = camera()->modelViewMatrix() * matrix;

    d->pFrameScene->mainNode()->draw(painter);
    d->pHeadScene->mainNode()->draw(painter);

    for (int i = 0; i < 4; i++) {
        QMatrix4x4 wingMatrix(matrix);
        wingMatrix.translate(d->ppWingPosition[i].x, d->ppWingPosition[i].y,
                d->ppWingPosition[i].z);
        wingMatrix.rotate(d->angleWing, 0, 1, 0);
        if (i == 2) wingMatrix.rotate(90, 0, 1, 0);
        wingMatrix.translate((-1)*d->ppWingPosition[i].x,
                         (-1)*d->ppWingPosition[i].y,
                         (-1)*d->ppWingPosition[i].z);
        painter->modelViewMatrix() = camera()->modelViewMatrix() * wingMatrix;

        d->ppWingScene[i]->mainNode()->draw(painter);
    }
}
