#ifndef PROGRESSRING_P_H
#define PROGRESSRING_P_H

#include <QTimer>

class QPixmap;

class ProgressRingPrivate
{
public:
    ProgressRingPrivate()
        : currentAngle(0),
          speed(1),
          pixmap(NULL)
    {
        rotateTimer.setSingleShot(false);
    }

    ProgressRingPrivate(double angle, double speed)
        : currentAngle(angle),
          speed(speed),
          pixmap(NULL)
    {
        rotateTimer.setSingleShot(false);
    }

    double currentAngle;
    double speed;

    QTimer rotateTimer;
    QPixmap *pixmap;
};

#endif // PROGRESSRING_P_H
