#ifndef RXMONITORVIEW_H
#define RXMONITORVIEW_H

#include <QGraphicsView>
#include <QGraphicsItem>
#include <QPainter>
#include "customwidgets_global.h"


class SensorItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
    Q_INTERFACES(QGraphicsItem)

public:
    explicit SensorItem(int width, int height)
    {
        m_width = width;
        m_height = height;
    }

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option, QWidget *widget)
    {
        Q_UNUSED(option);
        Q_UNUSED(widget);

        QRectF rect = boundingRect();
        painter->setPen(Qt::NoPen);
        painter->setBrush(QColor(Qt::red));
        painter->drawEllipse(rect.x() + 2, rect.y() + 2,
                             rect.width() - 4, rect.height() - 4);
    }

    QRectF boundingRect() const
    {
        return QRectF(-m_width/2, -m_height/2, m_width, m_height);
    }

private:
    int m_width, m_height;
};


class RxItem : public QGraphicsItem
{
public:
    explicit RxItem(int parentWidth, int parentHeight)
    {
        m_rxWidth = parentWidth;
        m_rxHeight = parentHeight;
    }

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option, QWidget *widget)
    {
        Q_UNUSED(option);
        Q_UNUSED(widget);

        QPen pen;
        pen.setWidth(1);
        pen.setColor(Qt::black);
        painter->setPen(pen);

        QVector<QLineF> lines;
        lines.append(QLineF(m_rxWidth/2, 0, m_rxWidth/2, m_rxHeight));
        lines.append(QLineF(0, m_rxHeight/2, m_rxWidth, m_rxHeight/2));

        qreal startX = m_rxWidth/2 - 5;
        qreal endX = m_rxWidth/2 + 5;
        qreal startY = m_rxHeight/2 - 5;
        qreal endY = m_rxHeight/2 + 5;

        for (qint8 i = 1; i <= 9; i++) {
            if (i == 5) continue;
            qreal dx = m_rxWidth/10 * i;
            qreal dy = m_rxHeight/10 * i;
            lines.append(QLineF(startX, dy, endX, dy));
            lines.append(QLineF(dx, startY, dx, endY));
        }

        painter->drawLines(lines);
    }

    QRectF boundingRect() const
    {
        return QRectF(0, 0, m_rxWidth, m_rxHeight);
    }

private:
    int m_rxWidth, m_rxHeight;
};

class CUSTOMWIDGETS_EXPORT RxMonitorView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit RxMonitorView(int, int, int, int, QWidget *parent = 0);
    ~RxMonitorView();

    void setRangeValue(int, int);

    bool animFinished() const;

    void setLocation(int location);
    int getLocation() const;

    void moveSensor();

    qreal x, y;
    bool need_move;

signals:
    void _animationFinished();
    void _sensorValueChanged(int, int);

private slots:
    void handleAnimFinished();

private:
    class RxMonitorViewPrivate;
    RxMonitorViewPrivate* const d;

    friend class VtRxMonitorView;
};


#endif // RXMONITORVIEW_H
