#ifndef TRIMSWASHCOLLECTIVE_H
#define TRIMSWASHCOLLECTIVE_H

#include <QWidget>
#include "customwidgets_global.h"
#include "trimswashbase.h"

namespace Ui {
class TrimSwashCollective;
}

class CUSTOMWIDGETS_EXPORT SwashTrimCollective : public AbstractTrimSwash
{
    Q_OBJECT

public:
    explicit SwashTrimCollective(QWidget *parent = 0);
    ~SwashTrimCollective();

    void setToolTipForClearButton(const QString&);

public slots:
    void setTrimMonitorValue(int, int);
    void setTrimMonitorRange(int, int);

    void setTrimValue(int);

signals:
    void _trimValueChanged(int);

private slots:
    void on_btnNorth2_clicked();

    void on_btnNorth1_clicked();

    void on_btnSouth1_clicked();

    void on_btnSouth2_clicked();

    void on_btnClear_clicked();

private:
    class SwashTrimCollectivePrivate;
    Q_DECLARE_PRIVATE(SwashTrimCollective)

    void updateTrimValue(int newVal);

    Ui::TrimSwashCollective *ui;
};

#endif // TRIMSWASHCOLLECTIVE_H
