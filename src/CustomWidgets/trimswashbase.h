#ifndef TRIMSWASHBASE_H
#define TRIMSWASHBASE_H

#include <QProgressBar>
#include "customwidgets_global.h"
#include <QWidget>

class AbstractTrimSwashPrivate
{
public:
    int trimMin, trimMax;
    int monitorMin, monitorMax;
};

class CUSTOMWIDGETS_EXPORT AbstractTrimSwash : public QWidget
{
public:
    enum MonitorChannel
    {
        Channel1 = 217,
        Channel2 = 218,
        Channel3 = 219,
        Channel4 = 220
    };

    AbstractTrimSwash(QWidget* parent = 0);

public slots:
    void setTrimRange(int, int);

    virtual void setTrimMonitorRange(int, int) = 0;
    virtual void setTrimMonitorValue(int, int) = 0;

protected:
    void updateMonitorUI(int, QProgressBar*, QProgressBar*, int = -1);

    AbstractTrimSwash(AbstractTrimSwashPrivate &d_ptr, QWidget* parent = 0);
    AbstractTrimSwashPrivate* const d_ptr;
    Q_DECLARE_PRIVATE(AbstractTrimSwash)
};

#endif // TRIMSWASHBASE_H
