#-------------------------------------------------
#
# Project created by QtCreator 2014-05-17T11:40:08
#
#-------------------------------------------------

QT       += widgets 3d

TARGET = vtcustomwidgets
TEMPLATE = lib

DEFINES += CUSTOMWIDGETS_LIBRARY

SOURCES += rxmonitorview.cpp \
    servositems.cpp \
    servosview.cpp \
    buttongroup.cpp \
    progressring.cpp \
    progressdialog.cpp \
    doubledirectslider.cpp \
    trimswashcyclic.cpp \
    trimswashcollective.cpp \
    trimswashbase.cpp \
    flystyleview.cpp \
    lineeditalert.cpp \
    quad3dglview.cpp

HEADERS += rxmonitorview.h\
    customwidgets_global.h \
    servositems.h \
    servosview.h \
    buttongroup.h \
    progressring.h \
    progressdialog.h \
    doubledirectslider.h \
    trimswashcyclic.h \
    trimswashcollective.h \
    flystyleview.h \
    lineeditalert.h \
    progressring_p.h \
    trimswashbase.h \
    quad3dglview.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    progressdialog.ui \
    doubledirectslider.ui \
    trimswashcyclic.ui \
    trimswashcollective.ui \
    flystyleview.ui

RESOURCES += \
    quad_obj.qrc
