#include "servosview.h"
#include <QObjectData>

class AdvanceServosView::AdvanceServosViewPrivate {
public:
    AdvanceServosViewPrivate(int w, int h, double as)
        : servosWidth(w),
          servosHeight(h),
          angleStep(as)
    {

    }

    int servosWidth;
    int servosHeight;
    double angleStep;
    ServosItem *servos;
};

AdvanceServosView::AdvanceServosView(int width, int height,
                                                   QWidget *parent, double angleStep)
    : QGraphicsView(parent),
      d(new AdvanceServosViewPrivate(width, height, angleStep))
{
    qRegisterMetaType< QList<int> >("QList<int>");

    d->servosWidth = width;
    d->servosHeight = height;
    d->angleStep = angleStep;

    setFixedSize(d->servosWidth, d->servosHeight);

    setRenderHint(QPainter::Antialiasing);
    setDragMode(RubberBandDrag);
    setOptimizationFlags(DontSavePainterState);
    setViewportUpdateMode(SmartViewportUpdate);
    setTransformationAnchor(AnchorUnderMouse);
    setInteractive(false);

    d->servos = new ServosItem(d->servosWidth, d->servosHeight);
    d->servos->setPos(5, 5);

    QGraphicsScene *scene = new QGraphicsScene;
    scene->addItem(d->servos);
    setScene(scene);

    connect(d->servos, SIGNAL(_angleChanged(QList<int>)),
            this, SIGNAL(_angleChanged(QList<int>)));
}

AdvanceServosView::~AdvanceServosView()
{
    delete d;
}

void AdvanceServosView::rotateLeft()
{
    double newAngle = d->servos->getAngle() - d->angleStep;
    if (newAngle > 360) {
        newAngle = newAngle - 360;
    }
    else if (newAngle < 0) {
        newAngle = newAngle + 360;
    }

    setAngle(newAngle);
    update();
}

void AdvanceServosView::rotateRight()
{
    double newAngle = d->servos->getAngle() + d->angleStep;
    if (newAngle > 360) {
        newAngle = newAngle - 360;
    }
    else if (newAngle < 0) {
        newAngle = newAngle + 360;
    }

    setAngle(newAngle);
    update();
}

void AdvanceServosView::resetChannelDefaultAngle(const QList<int>& listAngle)
{
    d->servos->resetChannelDefaultAngle(listAngle);
    update();
}

int AdvanceServosView::angle() const
{
    return static_cast<int> (d->servos->getAngle());
}

void AdvanceServosView::setAngle(int angle)
{
    d->servos->setAngle(angle);
    update();
}
