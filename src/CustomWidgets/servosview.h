#ifndef SERVOSVIEW_H
#define SERVOSVIEW_H

#include <QGraphicsView>
#include "customwidgets_global.h"
#include "servositems.h"

class CUSTOMWIDGETS_EXPORT AdvanceServosView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit AdvanceServosView(int width, int height,
                                      QWidget *parent = 0, double angleStep = 5);
    ~AdvanceServosView();

    void rotateLeft();
    void rotateRight();
    void resetChannelDefaultAngle(const QList<int>&);

    int angle() const;
    void setAngle(int);

signals:
    void _angleChanged(const QList<int>&);

private:
    class AdvanceServosViewPrivate;
    AdvanceServosViewPrivate* const d;
};

#endif // SERVOSVIEW_H
