#include "trimswashcollective.h"
#include "ui_trimswashcollective.h"

class SwashTrimCollective::SwashTrimCollectivePrivate
        : public AbstractTrimSwashPrivate
{
public:
    int trimValue;
};

SwashTrimCollective::SwashTrimCollective(QWidget *parent) :
    AbstractTrimSwash(*new SwashTrimCollectivePrivate, parent),
    ui(new Ui::TrimSwashCollective)
{
    ui->setupUi(this);

    Q_D(SwashTrimCollective);
    d->trimValue = 0;
}

SwashTrimCollective::~SwashTrimCollective()
{
    delete ui;
    delete d_ptr;
}

void SwashTrimCollective::setToolTipForClearButton(const QString &tt)
{
    ui->btnClear->setToolTip(tt);
}

void SwashTrimCollective::setTrimMonitorValue(int channelId, int val)
{
    switch (channelId) {
    case Channel1:
        updateMonitorUI(val, ui->progressCh1_Up, ui->progressCh1_Down);
        break;
    case Channel2:
        updateMonitorUI(val, ui->progressCh2_Up, ui->progressCh2_Down);
        break;
    case Channel3:
        updateMonitorUI(val, ui->progressCh3_Up, ui->progressCh3_Down);
        break;
    case Channel4:
        updateMonitorUI(val, ui->progressCh4_Up, ui->progressCh4_Down);
        break;
    default:
        return;
    }
}

void SwashTrimCollective::setTrimMonitorRange(int min, int max)
{
    QList<QProgressBar*> listProgress;
    listProgress << ui->progressCh1_Down << ui->progressCh1_Up
                    << ui->progressCh2_Down << ui->progressCh2_Up
                    << ui->progressCh3_Down << ui->progressCh3_Up
                    << ui->progressCh4_Down << ui->progressCh4_Up;

    Q_D(SwashTrimCollective);

    d->monitorMin = min;
    d->monitorMax = max;

    int middle = (max - min)/2 + min;
    for (int i = 0; i < listProgress.size(); i++) {
        if (i % 2 == 0) {
            listProgress[i]->setRange(0, middle - min);
        }
        else {
            listProgress[i]->setRange(0, max - middle);
        }
    }
}

void SwashTrimCollective::setTrimValue(int val)
{
    Q_D(SwashTrimCollective);

    d->trimValue = val;
}

void SwashTrimCollective::on_btnNorth2_clicked()
{
    updateTrimValue(5);
}

void SwashTrimCollective::on_btnNorth1_clicked()
{
    updateTrimValue(1);
}

void SwashTrimCollective::on_btnSouth1_clicked()
{
    updateTrimValue(-1);
}

void SwashTrimCollective::on_btnSouth2_clicked()
{
    updateTrimValue(-5);
}

void SwashTrimCollective::on_btnClear_clicked()
{
    Q_D(SwashTrimCollective);

    d->trimValue = (d->trimMax - d->trimMin)/2 + d->trimMin;
    emit _trimValueChanged(d->trimValue);
}

void SwashTrimCollective::updateTrimValue(int newVal)
{
    Q_D(SwashTrimCollective);

    if (newVal < 0) {
        if (d->trimValue == d->trimMin) {
            return;
        }
        d->trimValue = (d->trimValue + newVal < d->trimMin)
                ? d->trimMin : d->trimValue + newVal;
    }
    else {
        if (d->trimValue == d->trimMax) {
            return;
        }
        d->trimValue = (d->trimValue + newVal > d->trimMax)
                ? d->trimMax : d->trimValue + newVal;
    }
    emit _trimValueChanged(d->trimValue);
}
