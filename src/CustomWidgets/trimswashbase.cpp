#include "trimswashbase.h"

AbstractTrimSwash::AbstractTrimSwash(QWidget *parent)
    : QWidget(parent),
      d_ptr(new AbstractTrimSwashPrivate)
{
}

AbstractTrimSwash::AbstractTrimSwash(AbstractTrimSwashPrivate &d_ptr, QWidget *parent)
    : QWidget(parent),
      d_ptr(&d_ptr)
{
}

void AbstractTrimSwash::setTrimRange(int min, int max)
{
    d_ptr->trimMin = min;
    d_ptr->trimMax = max;
}

void AbstractTrimSwash::updateMonitorUI(int val, QProgressBar *up,
                                    QProgressBar *down, int middle)
{
    Q_D(AbstractTrimSwash);

    if (middle < 0) {
        middle = (d->monitorMax - d->monitorMin)/2 + d->monitorMin;
    }

    if (val >= middle) {
        up->setValue(val - middle);
        down->setValue(0);
    }
    else if (val < middle) {
        up->setValue(0);
        down->setValue(middle - val);
    }
    else {
        up->setValue(0);
        down->setValue(0);
    }

    up->repaint();
    down->repaint();
}




