#ifndef DOUBLEDIRECTSLIDER_H
#define DOUBLEDIRECTSLIDER_H

#include <QWidget>
#include "customwidgets_global.h"

namespace Ui {
class DoubleDirectSlider;
}

class QSlider;
class QProgressBar;
class QLineEdit;

class CUSTOMWIDGETS_EXPORT DoubleDirectSlider : public QWidget
{
    Q_OBJECT

public:
    explicit DoubleDirectSlider(QWidget *parent = 0);
    ~DoubleDirectSlider();

    void setSliderValue(int = 0);
    int sliderValue() const;

    QSlider *getSlider();
    QLineEdit* getLineEdit();

public Q_SLOTS:
    void setSliderRange(int = -100, int = 100);

private Q_SLOTS:
    void updateProgressOnSlider();

private:
    Ui::DoubleDirectSlider *ui;

};

#endif // DOUBLEDIRECTSLIDER_H
