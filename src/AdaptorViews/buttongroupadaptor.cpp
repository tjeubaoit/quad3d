#include "buttongroupadaptor.h"
#include <QButtonGroup>

ButtonGroupAdaptor::ButtonGroupAdaptor(QObject *field, QObject *parent)
    : AbstractFieldAdaptor(field, parent)
{
    QButtonGroup *bg = qobject_cast<QButtonGroup*> (field);
    connect(bg, SIGNAL(buttonClicked(int)),
            this, SLOT(buttonClicked(int)));
}

QVariant ButtonGroupAdaptor::getValue() const
{
    QButtonGroup *bg = qobject_cast<QButtonGroup*> (m_field);
    return bg->checkedId();
}

void ButtonGroupAdaptor::setValue(const QVariant &value)
{
    QButtonGroup *bg = qobject_cast<QButtonGroup*> (m_field);
    int id = GET_VALUE(value);
    QAbstractButton *btt = bg->button(id);
    if (btt != NULL) {
        btt->setChecked(true);
    }
}

void ButtonGroupAdaptor::buttonClicked(int id)
{
    Q_UNUSED(id);
    emit _adaptorValueChanged(getValue());
}
