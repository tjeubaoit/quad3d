#include "comboboxadaptor.h"
#include <QComboBox>

ComboBoxAdaptor::ComboBoxAdaptor(QObject *field, QObject *parent)
    : AbstractFieldAdaptor(field, parent)
{
    QComboBox *cb = qobject_cast<QComboBox*> (field);
    connect(cb, SIGNAL(activated(int)), this, SLOT(activated(int)));
}

QVariant ComboBoxAdaptor::getValue() const
{
    QComboBox *cbBox = qobject_cast<QComboBox*> (m_field);
    return cbBox->currentData();
}

void ComboBoxAdaptor::setValue(const QVariant &value)
{
    QComboBox *cbBox = qobject_cast<QComboBox*> (m_field);
    int val = GET_VALUE(value);
    for (int i = 0; i < cbBox->count(); i++) {
        if (cbBox->itemData(i) == val) {
            cbBox->setCurrentIndex(i);
            return;
        }
    }
}

void ComboBoxAdaptor::activated(int val)
{
    Q_UNUSED(val);
    emit _adaptorValueChanged(getValue());
}
