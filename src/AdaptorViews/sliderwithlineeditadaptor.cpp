#include "sliderwithlineeditadaptor.h"
#include "lineeditadaptor.h"

#include <QLineEdit>
#include <QSlider>
#include <QEvent>
#include <QDebug>

SliderWithLineEditAdaptor::SliderWithLineEditAdaptor(
        QObject *field, QObject *extraField, QObject *parent)
    : SliderAdaptor(field, parent)
{
    m_edit = qobject_cast<QLineEdit*> (extraField);
    m_editAdaptor = new LineEditAdaptor(m_edit, this);

    connect(m_edit, SIGNAL(returnPressed()),
            this, SLOT(editReturnPressed()));
}

QLineEdit *SliderWithLineEditAdaptor::getLineEdit() const
{
    return m_edit;
}

LineEditAdaptor *SliderWithLineEditAdaptor::getLineEditAdaptor() const
{
    return m_editAdaptor;
}

void SliderWithLineEditAdaptor::sliderValueChanged(int val)
{
    SliderAdaptor::sliderValueChanged(val);
    m_edit->setText(QString::number(val));
}

void SliderWithLineEditAdaptor::editReturnPressed()
{
    QSlider *slider = qobject_cast<QSlider*> (m_field);
    slider->setValue(m_edit->text().toInt());

    emit _adaptorValueChanged(getValue());
}
