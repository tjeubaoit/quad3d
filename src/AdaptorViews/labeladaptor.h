#ifndef LABELADAPTOR_H
#define LABELADAPTOR_H

#include "abstractfieldadaptor.h"

class ADAPTORVIEWSSHARED_EXPORT LabelAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT
public:
    LabelAdaptor(QObject* field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);
};

#endif // LABELADAPTOR_H
