#ifndef BUTTONGROUPADAPTOR_H
#define BUTTONGROUPADAPTOR_H

#include "abstractfieldadaptor.h"
#include <QAbstractButton>

class ADAPTORVIEWSSHARED_EXPORT ButtonGroupAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT

public:
    ButtonGroupAdaptor(QObject* field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

protected Q_SLOTS:
    virtual void buttonClicked(int);

};

#endif // BUTTONGROUPADAPTOR_H
