#include "adaptormodel.h"
#include "abstractfieldadaptor.h"

#include <QHash>

AdaptorModel::AdaptorModel(QObject *parent)
    : QObject(parent)
{

}

AdaptorModel::~AdaptorModel()
{
    foreach (AbstractFieldAdaptor* const adaptor, getListAdaptors()) {
        delete adaptor;
    }
}

int AdaptorModel::size() const
{
    return m_adaptorHash.size();
}

QList<QObject *> AdaptorModel::getListFields() const
{
    QList<QObject*> result;
    foreach (AbstractFieldAdaptor* adaptor, getListAdaptors()) {
        result.append(adaptor->inputField());
    }

    return result;
}

AbstractFieldAdaptor *AdaptorModel::getAdaptor(int addr) const
{
    return m_adaptorHash.value(addr, 0);
}

QList<AbstractFieldAdaptor *> AdaptorModel::getListAdaptors() const
{
    return m_adaptorHash.values();
}

QList<int> AdaptorModel::getKeys() const
{
    return m_adaptorHash.keys();
}

void AdaptorModel::addAdaptor(int addr, AbstractFieldAdaptor *adaptor)
{
    if (adaptor != 0) {
        if (m_adaptorHash.contains(addr)) {
            AbstractFieldAdaptor* a = m_adaptorHash[addr];
            a->deleteLater();
        }

        m_adaptorHash.insert(addr, adaptor);

        connect(adaptor, SIGNAL(_adaptorValueChanged(QVariant,bool)),
                this, SLOT(adaptorValueChanged(QVariant,bool)));
    }
}

int AdaptorModel::getAdaptorValue(int addr) const
{
    Q_ASSERT(m_adaptorHash.contains(addr));
    return m_adaptorHash[addr]->getValue().toInt();
}

void AdaptorModel::setAdaptorValue(int addr, int val)
{
    AbstractFieldAdaptor *adaptor = m_adaptorHash.value(addr);
    if (adaptor && !adaptor->isReadOnly() && GET_VALUE(adaptor->getValue()) != val) {
        m_adaptorHash[addr]->setValue(val);
    }
}

void AdaptorModel::setAdaptorReadOnly(int addr, bool val)
{
    Q_ASSERT(m_adaptorHash.contains(addr));
    m_adaptorHash[addr]->setReadOnly(val);
}

bool AdaptorModel::isAdaptorReadOnly(int addr) const
{
    Q_ASSERT(m_adaptorHash.contains(addr));
    return m_adaptorHash.value(addr)->isReadOnly();
}

void AdaptorModel::adaptorValueChanged(const QVariant &val, bool isBlocking)
{
    AbstractFieldAdaptor *adaptor = qobject_cast<AbstractFieldAdaptor*> (sender());
    int key = m_adaptorHash.key(adaptor, -1);
    if (key > 0) {
        emit _adaptorValueChanged(key, val.toInt(), isBlocking);
    }
}
