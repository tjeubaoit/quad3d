#include "lineeditadaptor.h"
#include <QLineEdit>
#include <QEvent>

LineEditAdaptor::LineEditAdaptor(QObject *field, QObject *parent)
    : AbstractFieldAdaptor(field, parent),
      m_needUndo(false)
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (m_field);

    connect(edit, SIGNAL(textEdited(QString)),
            this, SLOT(textEdited(QString)));

    connect(edit, SIGNAL(returnPressed()),
            this, SLOT(returnPressed()));

    edit->installEventFilter(this);
}

QVariant LineEditAdaptor::getValue() const
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (m_field);
    return edit->text().toInt();
}

void LineEditAdaptor::setValue(const QVariant &value)
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (m_field);
    QString text = QString::number(GET_VALUE(value));
    edit->setText(text);
}

bool LineEditAdaptor::eventFilter(QObject *obj, QEvent *event)
{
    if (qobject_cast<QLineEdit*> (obj)) {
        QLineEdit *edit = qobject_cast<QLineEdit*> (obj);
        if (event->type() == QEvent::FocusOut) {
            if (m_needUndo) {
                m_needUndo = false;
                m_readOnly = false;
                edit->setText(m_tmpText);
                emit _editLostFocus(m_field);
            }
        }
        else if (event->type() == QEvent::FocusIn) {
            m_tmpText = edit->text();
        }
    }

    return false;
}

void LineEditAdaptor::emitValueChanged()
{
    emit _adaptorValueChanged(getValue());
}

void LineEditAdaptor::returnPressed()
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (m_field);
    m_tmpText = edit->text();
    m_needUndo = false;
    m_readOnly= false;

    emitValueChanged();
}

void LineEditAdaptor::textEdited(const QString &)
{
    m_readOnly = true;
    m_needUndo = true;
}
