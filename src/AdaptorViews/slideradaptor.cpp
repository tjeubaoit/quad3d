#include "slideradaptor.h"
#include <QSlider>

SliderAdaptor::SliderAdaptor(QObject *field, QObject* parent)
    : AbstractFieldAdaptor(field, parent)
{
    QSlider *slider = qobject_cast<QSlider*> (field);

    connect(slider, SIGNAL(sliderPressed()),
            this, SLOT(sliderPressed()));

    connect(slider, SIGNAL(sliderReleased()),
            this, SLOT(sliderReleased()));

    connect(slider, SIGNAL(actionTriggered(int)),
            this, SLOT(sliderActionTriggered(int)));

    connect(slider, SIGNAL(valueChanged(int)),
            this, SLOT(sliderValueChanged(int)));

    m_sendDelayTimer.setInterval(500);
    m_sendDelayTimer.setSingleShot(true);

    connect(&m_sendDelayTimer, SIGNAL(timeout()),
            this, SLOT(sendDelay_timeout()));
}

QVariant SliderAdaptor::getValue() const
{
    QSlider *slider = qobject_cast<QSlider*> (m_field);
    return slider->value();
}

void SliderAdaptor::setValue(const QVariant &value)
{
    QSlider *slider = qobject_cast<QSlider*> (m_field);
    slider->setValue(GET_VALUE(value));
}

void SliderAdaptor::sliderPressed()
{
    m_readOnly = true;
}

void SliderAdaptor::sliderReleased()
{
    m_readOnly = false;
    emit _adaptorValueChanged(getValue());
}

void SliderAdaptor::sliderActionTriggered(int action)
{
    if (action >= 1 && action <=4) {
        m_readOnly = true;
    }
}

void SliderAdaptor::sliderValueChanged(int)
{
    QSlider *slider = qobject_cast<QSlider*> (m_field);
    if (m_readOnly && !slider->isSliderDown()) {
        m_readOnly = true;
        emitValueChangedDelay();
    }
}

void SliderAdaptor::emitValueChangedDelay()
{
    m_sendDelayTimer.start();
}

void SliderAdaptor::sendDelay_timeout()
{
    emit _adaptorValueChanged(getValue());
}
