#ifndef SLIDERWITHLINEEDITADAPTOR_H
#define SLIDERWITHLINEEDITADAPTOR_H

#include "slideradaptor.h"

class QLineEdit;
class LineEditAdaptor;

class ADAPTORVIEWSSHARED_EXPORT SliderWithLineEditAdaptor : public SliderAdaptor
{
    Q_OBJECT
public:
    SliderWithLineEditAdaptor(QObject* field, QObject* extraField, QObject* parent = 0);

    QLineEdit* getLineEdit() const;
    LineEditAdaptor* getLineEditAdaptor() const;

protected Q_SLOTS:
    virtual void sliderValueChanged(int);
    virtual void editReturnPressed();

private:
    QLineEdit* m_edit;
    LineEditAdaptor *m_editAdaptor;

};

#endif // SLIDERWITHLINEEDITADAPTOR_H
