#include "labeladaptor.h"

#include <QLabel>

LabelAdaptor::LabelAdaptor(QObject *field, QObject *parent)
    : AbstractFieldAdaptor(field, parent)
{
}

QVariant LabelAdaptor::getValue() const
{
    QLabel *lb = qobject_cast<QLabel*> (m_field);
    return lb->text();
}

void LabelAdaptor::setValue(const QVariant &value)
{
    QLabel *lb = qobject_cast<QLabel*> (m_field);
    lb->setText(value.toString());
}
