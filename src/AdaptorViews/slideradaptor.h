#ifndef SLIDERADAPTOR_H
#define SLIDERADAPTOR_H

#include "abstractfieldadaptor.h"

#include <QTimer>

class ADAPTORVIEWSSHARED_EXPORT SliderAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT
public:
    SliderAdaptor(QObject *field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

protected Q_SLOTS:
    virtual void sliderPressed();
    virtual void sliderReleased();
    virtual void sliderActionTriggered(int);
    virtual void sliderValueChanged(int);

protected:
    void emitValueChangedDelay();

private Q_SLOTS:
    void sendDelay_timeout();

private:
    QTimer m_sendDelayTimer;
};

#endif // SLIDERADAPTOR_H

