#ifndef ABSTRACTFIELDADAPTOR_H
#define ABSTRACTFIELDADAPTOR_H

#include "adaptorviews_global.h"
#include <QObject>
#include <QVariant>

class ADAPTORVIEWSSHARED_EXPORT AbstractFieldAdaptor : public QObject
{
    Q_OBJECT

public:
    AbstractFieldAdaptor(QObject* obj, QObject* parent = 0);

public Q_SLOTS:
    void setInputField(QObject* field);
    QObject* inputField() const;
    void removeField();

    virtual QVariant getValue() const = 0;
    virtual void setValue(const QVariant& val) = 0;

    bool isReadOnly() const;
    void setReadOnly(bool isReadOnly);

Q_SIGNALS:
    void _adaptorValueChanged(const QVariant& val, bool emitBlocking = false);

protected:
    QObject* m_field;
    bool m_readOnly;

};

#endif // ABSTRACTFIELDADAPTOR_H
