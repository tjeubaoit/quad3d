#ifndef COMBOBOXADAPTOR_H
#define COMBOBOXADAPTOR_H

#include "abstractfieldadaptor.h"

class ADAPTORVIEWSSHARED_EXPORT ComboBoxAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT

public:
    ComboBoxAdaptor(QObject* field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

protected Q_SLOTS:
    virtual void activated(int);

};

#endif // COMBOBOXADAPTOR_H
