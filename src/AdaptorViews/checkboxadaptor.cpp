#include "checkboxadaptor.h"
#include <QCheckBox>

CheckBoxAdaptor::CheckBoxAdaptor(QObject *field, QObject *parent)
    : AbstractFieldAdaptor(field, parent)
{
    QCheckBox *check = qobject_cast<QCheckBox*> (field);
    connect(check, SIGNAL(clicked(bool)), this, SLOT(clicked(bool)));
}

QVariant CheckBoxAdaptor::getValue() const
{
    QCheckBox *check = qobject_cast<QCheckBox*> (m_field);
    return (check->isChecked() ? CHECKABLE_BUTTON_CHECKED
                               : CHECKABLE_BUTTON_UNCHECKED);
}

void CheckBoxAdaptor::setValue(const QVariant &value)
{
    QCheckBox *check = qobject_cast<QCheckBox*> (m_field);
    bool val = GET_VALUE(value);
    check->setChecked(val);
}

void CheckBoxAdaptor::clicked(bool checked)
{
    Q_UNUSED(checked);
    emit _adaptorValueChanged(getValue());
}
