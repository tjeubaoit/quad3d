#include "libserial_global.h"

#include <QSerialPort>
#include <QDebug>

bool configureForPort(QSerialPort *port)
{
    if (!port->setDataBits(QSerialPort::Data8)) {
        qDebug() << "cannot set databits";
        return false;
    }

    if (!port->setBaudRate(QSerialPort::Baud115200)) {
        qDebug() << "cannot set baudrate";
        return false;
    }

    if (!port->setStopBits(QSerialPort::OneStop)) {
        qDebug() << "cannot set stopbits";
        return false;
    }

    if (!port->setParity(QSerialPort::NoParity)) {
        qDebug() << "cannot set parity";
        return false;
    }

    return true;
}

bool openPort(QSerialPort* port)
{
    if (!port->isOpen() && !port->open(QIODevice::ReadWrite))  {
        qDebug() << "port cannot open" << port->errorString();
        return false;
    }

    if (! configureForPort(port)) {
        port->close();
        return false;
    }

    return true;
}
