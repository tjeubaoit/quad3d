#ifndef ABSTRACTCOMMANDPARSER_H
#define ABSTRACTCOMMANDPARSER_H

#include "serialbuffer.h"
#include "libserial_global.h"

#include <QObject>

class LIBSERIALSHARED_EXPORT AbstractCommandParserPrivate
{
public:
    SerialBuffer serialBuffer;
};

class LIBSERIALSHARED_EXPORT AbstractCommandParser : public QObject
{
    Q_OBJECT
public:
    explicit AbstractCommandParser(QObject *parent)
        : d(new AbstractCommandParserPrivate)
    {
        Q_UNUSED(parent);
    }
    virtual ~AbstractCommandParser() { delete d; }

    void appendData(const char *data, quint32 count)
    {
        d->serialBuffer.append(data, count);
    }

    bool bufferEmpty() const
    {
        return d->serialBuffer.size() == 0;
    }

    virtual bool nextResult(qint16 *address, qint16 *value, char *extra = 0) = 0;
    virtual quint32 generateCommand(qint16 address, qint16 value, char *result) = 0;
    virtual quint32 generateCommand(qint16 address, char *result) = 0;

protected:
    AbstractCommandParserPrivate* const d;
};

#endif // ABSTRACTCOMMANDPARSER_H
