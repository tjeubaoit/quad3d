#include "quad3dserialtransfer.h"
#include "quad3dcommandparser.h"

#include <QSerialPort>
#include <QVariant>
#include <QDebug>
#include <QCoreApplication>
#include <QThread>


class Quad3DSerialTransfer::Quad3DSerialTransferPrivate
{
public:
    Quad3DSerialTransferPrivate() : cmdParser(new Quad3DCommandParser) {}
    ~Quad3DSerialTransferPrivate() { delete cmdParser; }

    Quad3DCommandParser *cmdParser;
};

Quad3DSerialTransfer::Quad3DSerialTransfer(QObject *parent)
    : AbstractSerialTransfer(parent),
      d(new Quad3DSerialTransferPrivate)
{
}

Quad3DSerialTransfer::~Quad3DSerialTransfer()
{
    delete d;
}

void Quad3DSerialTransfer::writeRequestSingleAddress(const QVariant &addr)
{
    QSerialPort *port = getPort();
    if (!port) return;

    char result[32];
    quint32 len = d->cmdParser->generateCommand(addr.toInt(), result);
    if (len > 0) {
        port->write(result, len);
        SLEEP(5);
    }
}

void Quad3DSerialTransfer::writeApplyConfig(const QVariant &addr, const QVariant &val)
{
    QSerialPort *port = getPort();
    if (!port) return;

    char result[32];
    quint32 len = d->cmdParser->generateCommand(addr.toInt(), val.toInt(), result);
    if (len > 0) {
        qDebug() << "write" << addr.toInt() << val.toInt();
        port->write(result, len);
        SLEEP(150);
    }
}

void Quad3DSerialTransfer::readResponseData()
{
    QSerialPort *port = getPort();
    if (!port || !port->isOpen()) return;

    while (!port->atEnd()) {
        char bytes[256];
        int bytesRead = port->read(bytes, sizeof(bytes));
        if (bytesRead <= 0) {
            qDebug() << "read error";
            continue;
        }
        d->cmdParser->appendData(bytes, bytesRead);
    }

    qint16 addr = 0, val = 0;
    char buffer[32] = { 0 };
    while (d->cmdParser->nextResult(&addr, &val, buffer)) {
        if (buffer[0] != 0) {
            emit _responseToUpdateUi(QString::fromLocal8Bit(buffer));
            memset(buffer, 0, sizeof(buffer));
        } else {
            emit _responseToUpdateUi(addr, val);
            qDebug() << "emit ->" << addr << val;
        }
    }
}
