#ifndef ABSTRACTSERIALFACTORY_H
#define ABSTRACTSERIALFACTORY_H

class AbstractSerialTransfer;
class AbstractPortChecker;

class AbstractSerialFactory
{
public:
    virtual AbstractSerialTransfer *createSerialTransfer() = 0;
    virtual AbstractPortChecker *createPortChecker() = 0;
};

#endif // ABSTRACTSERIALFACTORY_H
