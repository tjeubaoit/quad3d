#ifndef ABSTRACTSERIALMANAGER_H
#define ABSTRACTSERIALMANAGER_H

#include "libserial_global.h"
#include <QThread>

class LIBSERIALSHARED_EXPORT AbstractSerialManager : public QThread
{
    Q_OBJECT

public:
    virtual bool isPortConnected() const = 0;

Q_SIGNALS:
    void _portStatusChanged(int);
    void _responseToUpdateUi(const QVariant&);

    void _flashProgressChanged(int);
    void _flashStatusChanged(int);

public slots:
    virtual void startUpdateFirmware(const QByteArray&) = 0;

    virtual void readSingleAddress(const QVariant&) = 0;
    virtual void readMultipleAddress(const QVariant&) = 0;
    virtual void writeConfigAsync(const QVariant&, const QVariant&) = 0;
    virtual void writeConfigBlocking(const QVariant&, const QVariant&) = 0;
};

#endif // ABSTRACTSERIALMANAGER_H
