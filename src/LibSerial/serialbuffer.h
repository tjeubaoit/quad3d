#ifndef SERIALBUFFER_H
#define SERIALBUFFER_H

#include "libserial_global.h"

#include <QObject>

class LIBSERIALSHARED_EXPORT SerialBuffer
{
public:
    SerialBuffer();

    void append(const char *data, int len);
    void remove(int numberElementsToDelete);
    char *data();
    const char *constData() const;
    char at(int pos) const;
    int size() const;

private:
    int m_head, m_tail;
    char m_buffer[512];
};

#endif // SERIALBUFFER_H
