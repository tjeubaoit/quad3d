#include "flashhelper.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QSerialPort>

FlashHelper::FlashHelper(QObject *parent) :
    QObject(parent),
    mPortLock(new QMutex)
{
    m_currentFlashPercent = 0;
    cka = 0;ckb = 0;state =0; mId = 0;tmpData = 0;
    rcka = 0;rckb = 0;
    mFileLen = 0;
    mBuffSize = 0;
    mBuffLen = 0;
    mBuffType = 0;
    responFlag = false;
    timeOutToRespon = 0;
    mTimer.setInterval(100);
    connect(&mTimer,SIGNAL(timeout()),this,SLOT(SLOT_timeOut()));
}

FlashHelper::~FlashHelper()
{
    delete mPortLock;
    closePort();
}

void FlashHelper::setFlashData(const QByteArray &data)
{
    this->mByteToWrite = data;
}

void FlashHelper::setPortName(const QString &portName)
{
    this->m_port = new QSerialPort(portName);
    configureForPortOk = openPort(m_port);
}

void FlashHelper::updateFlashNew()
{
    if (!configureForPortOk) {
        emit _flashStatusChanged(PortCannotOpen);
        return;
    }
    connect(m_port, SIGNAL(readyRead()),
            this, SLOT(SLOT_ReadByteFromBuffer()));

    mFileLen = mByteToWrite.length();
    QByteArray tmpBuffLen;tmpBuffLen.resize(4);
    tmpBuffLen[0] = (char)(mFileLen >> 24);
    tmpBuffLen[1] = (char)((mFileLen >> 16) & 0xff);
    tmpBuffLen[2] = (char)((mFileLen >> 8) & 0xff);
    tmpBuffLen[3] = (char)(mFileLen & 0xff);

    m_port->write("*goto3007\r");
    m_port->waitForBytesWritten(50);
    m_port->write("*boot2\r");
    m_port->waitForBytesWritten(50);
    qDebug() << "erase";
    SLEEP(100)
    writeByte(tmpBuffLen,0x01,0x01);
    mTimer.start();

    emit _flashStatusChanged(StartEraseFlash);
}

void FlashHelper::SLOT_ReadByteFromBuffer()
{

        mPortLock->lock();
        if(!m_port || !m_port->isOpen())return;
        qint64 mNumOfByte = m_port->bytesAvailable();
        //qDebug() << "jum in SLOT_ReadByteFromBuffer" << mNumOfByte;
        if(mNumOfByte > 4){
            QByteArray mBuff = m_port->read(mNumOfByte);
            mPortLock->unlock();
            for(int pos = 0; pos < mBuff.size();pos++)
            {
                unsigned char mData =(unsigned char)mBuff[pos];

                switch (state) {
                case 0: if (mData == 0xB5) state++; break;
                case 1: if (mData == 0x62) state++; else state = 0; break;
                case 2: mId = mData; cka += mData; ckb += cka; state++; break;
                case 3: tmpData = mData; cka += mData; ckb += cka; state++; break;
                case 4: rcka = mData; state++; break;
                case 5: rckb = mData;
                    if (cka == rcka && ckb == rckb)
                    {
                        int iFlashStt = 0;
                        int tmpp = 0;
                        QByteArray tmpBuff;
                        responFlag = true;
                        switch (mId) {
                            case 0x01:
                                switch(tmpData)
                                {
                                case 0x01: iFlashStt = EraseFlashFail;
                                    qDebug() << "erase fail";
                                    break;
                                case 0x02: iFlashStt = StartWriteFirmwareData ;
                                     qDebug() << "erase ok";
                                    break;
                                case 0x03:
                                    qDebug() << "package" << tmpData;
                                    //emit _flashFinish(0);
                                    break;
                                case 0x04:
                                            m_port->write("*flash\r");
                                            qDebug() << "flash ok";
                                            iFlashStt = StartTestNewFirmware;
                                            if(mTimer.isActive())mTimer.stop();
//                                            emit _flashFinish(10);
                                            break;
                                case 0x05: iFlashStt = JumpFail;break;
                                case 6: iFlashStt = ToltalByteWRong;break;
                                case 7: break;
                                }
                                break;
                        case 0x02:
                                if ((mFileLen - tmpData * mBuffSize) > mBuffSize) mBuffLen = mBuffSize;
                                else mBuffLen = mFileLen - tmpData * mBuffSize;
                                if (mBuffLen == mBuffSize)
                                    tmpp = ((int)tmpData+1) * 100 * mBuffSize / mFileLen;
                                else tmpp = (((int)tmpData ) * mBuffSize + mBuffLen) * 100 / mFileLen;
                                 emit _flashProgressChanged(tmpp);
                                 qDebug() << "Percent flash " << tmpp;
                                tmpBuff.resize(mBuffLen);
                                for(int i = 0; i < mBuffLen;i++)
                                {
                                    tmpBuff[i] = mByteToWrite[tmpData*mBuffSize + i];
                                }
                                writeByte(tmpBuff,0x02,tmpData);
                                 break;
                        case 0xaa:
                                if(tmpData == 0x01)mBuffSize = 1024;
                                else if(tmpData == 0x02)mBuffSize = 2048;
                                break;
                        case 0x04:
                                qDebug() << "Current package error at" << tmpData;
                                break;
                        case 0x03:
                                int percent = tmpData * 100 * mBuffSize / mFileLen;
                                emit _flashProgressChanged(percent);
                                qDebug() << "Percent erase " << percent;
                                break;
                        }
                       emit emitFlashStatus(iFlashStt);
                    }
                    state = 0; rcka = 0; rckb = 0; cka = 0; ckb = 0;
                    break;
                }
            }
        }
        else {
            mPortLock->unlock();
        }
}

void FlashHelper::emitFlashStatus(int status)
{
    emit _flashStatusChanged(status);
}

QString FlashHelper::getPortName() const
{
    return (m_port) ? m_port->portName() : QString();
}

QSerialPort *FlashHelper::getPort() const
{
    return m_port;
}

void FlashHelper::closePort()
{
    if (m_port) {
        if (m_port->isOpen()) m_port->close();
        delete m_port;
        m_port = 0;
    }
}

void FlashHelper::SLOT_timeOut()
{
    if(responFlag){
        timeOutToRespon = 0;
        responFlag = false;
    }
    else timeOutToRespon++;
    if(timeOutToRespon >= 20){

        responFlag = false;
        if(mTimer.isActive())mTimer.stop();
        qDebug() << "flash timeout";
//        emit _flashFinish(timeOutToRespon);
        emit _flashStatusChanged(Timeout);
        timeOutToRespon = 0;
    }
}

void FlashHelper::write4Byte(QByteArray data, char mesClass, char mesID)
{
    char cka = 0, ckb = 0;
    QByteArray tmpArr;
    tmpArr.resize(11);
    tmpArr[0] = 181;
    tmpArr[1] =  98;
    tmpArr[2] = mesClass;
    tmpArr[3] = mesID;
    tmpArr[4] = 4;
    for(int i = 0; i < 4; i++) {
        tmpArr[5+i] = data[i];
    }
    for(int i = 0; i < 7; i++) {
        cka += tmpArr[2+i];
        ckb += cka;
    }
    tmpArr[9] = cka;
    tmpArr[10] = ckb;
    m_port->write(tmpArr);
    m_port->waitForBytesWritten(10);
}

void FlashHelper::writeByte(QByteArray mData, char mClass, char mId)
{

    char cka = 0, ckb = 0;
    QByteArray tmpBuff;tmpBuff.resize(mData.length() + 8);
    tmpBuff[0] = 0xb5;
    tmpBuff[1] = 0x62;
    tmpBuff[2] = mClass; cka += mClass; ckb += cka;
    tmpBuff[3] = mId; cka += mId; ckb += cka;
    tmpBuff[4] = (char)((mData.length() >> 8) & 0xff); cka += tmpBuff[4]; ckb += cka;
    tmpBuff[5] = (char)(mData.length() & 0xff); cka += tmpBuff[5]; ckb += cka;
    for (int i = 0; i < mData.length(); i++)
    {
        tmpBuff[6 + i] = mData[i];
        cka += mData[i];
        ckb += cka;
    }
    tmpBuff[mData.length() + 6] = cka;
    tmpBuff[mData.length() + 7] = ckb;
    if(m_port)
    m_port->write(tmpBuff);
}
