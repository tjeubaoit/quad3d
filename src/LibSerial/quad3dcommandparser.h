#ifndef QUAD3DCOMMANDPARSER_H
#define QUAD3DCOMMANDPARSER_H

#include "abstractcommandparser.h"
#include "libserial_global.h"

class LIBSERIALSHARED_EXPORT Quad3DCommandParser : public AbstractCommandParser
{
public:
    explicit Quad3DCommandParser(QObject *parent = 0);
    ~Quad3DCommandParser() {}

    virtual bool nextResult(qint16 *address, qint16 *value, char *ex = 0);
    virtual quint32 generateCommand(qint16 address, qint16 value, char *result);
    virtual quint32 generateCommand(qint16 address, char *result);

private:
    bool isStartOfNewMessage();
};

#endif // QUAD3DCOMMANDPARSER_H
