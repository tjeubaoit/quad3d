#include "quad3dportchecker.h"
#include "quad3dcommandparser.h"

#include <QSerialPort>
#include <QThread>
#include <QDebug>

#define ADDR_TEST 112

bool Quad3DPortChecker::checkPort(QSerialPort *port)
{
    Quad3DCommandParser cmdParser;
    char result[32];
    quint32 len = cmdParser.generateCommand(ADDR_TEST, result);
    if (len <= 0) return false;

    if (port->write(result, len) < 0) {
        qDebug() << "write to serial port error";
        return false;
    }
    SLEEP(50)

    if (port->waitForReadyRead(1000)) {
        char tmp[1024];
        while (!port->atEnd()) {
            int nBytes = port->read(tmp, sizeof(tmp));
            if (nBytes > 0) {
                cmdParser.appendData(tmp, nBytes);
            }
            SLEEP(10)
        }

        qint16 addr = 0, val = 0;
        return cmdParser.nextResult(&addr, &val) && (addr == ADDR_TEST);
    }
    qDebug() << "pid and vid correct but no response";

    return false;
}
