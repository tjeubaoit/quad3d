#ifndef ABSTRACTPORTCHECKER_H
#define ABSTRACTPORTCHECKER_H

#include "libserial_global.h"

class QSerialPort;

class LIBSERIALSHARED_EXPORT AbstractPortChecker
{
public:
    virtual ~AbstractPortChecker() {}
    virtual bool checkPort(QSerialPort *port) = 0;
};

#endif // ABSTRACTPORTCHECKER_H
