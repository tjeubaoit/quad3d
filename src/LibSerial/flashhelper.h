#ifndef FLASHHELPER_H
#define FLASHHELPER_H

#include "libserial_global.h"

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QTimer>

class QSerialPort;
class QVariant;

class LIBSERIALSHARED_EXPORT FlashHelper : public QObject
{
    Q_OBJECT
public:
    enum FlashStatus
    {
        PortCannotOpen = 301,

        StartEraseFlash = 401,
        StartWriteFirmwareData = 402,
        StartTestNewFirmware = 403,
        FlashResultOk = 200,

        FlashResultError = 302,
        EraseFlashFail = 303,
        JumpFail = 304,
        ToltalByteWRong = 305,
        Timeout = 306
    };

    explicit FlashHelper(QObject *parent = 0);
    virtual ~FlashHelper();

    void setFlashData(const QByteArray &data);
    void setPortName(const QString &portName);
    void emitFlashStatus(int status);

    QString getPortName() const;
    QSerialPort *getPort() const;
    void closePort();

public slots:
    void updateFlashNew();
    void SLOT_ReadByteFromBuffer();
    void SLOT_timeOut();

signals:
    void _flashStatusChanged(int);
    void _flashProgressChanged(int);
    void _flashFinish(int);

private:
    void write4Byte(QByteArray,char,char);
    void writeByte(QByteArray,char,char);
    QMutex *mPortLock;
    QSerialPort *m_port;
    QByteArray mByteToWrite;
    QTimer mTimer;
    int mFileLen;
    int mBuffSize;
    int mBuffLen;
    int mBuffType;
    int m_currentFlashPercent;
    int timeOutToRespon;
    bool responFlag;
    unsigned char cka ,ckb ,state , mId ,tmpData;
    unsigned char rcka ,rckb ;
    bool configureForPortOk;
};

#endif // FLASHHELPER_H
