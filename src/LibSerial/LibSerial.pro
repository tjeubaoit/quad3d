#-------------------------------------------------
#
# Project created by QtCreator 2014-11-17T10:30:02
#
#-------------------------------------------------

QT       -= gui
QT       += core serialport

TARGET = vtserial
TEMPLATE = lib

DEFINES += LIBSERIAL_LIBRARY

SOURCES += \
    quad3dcommandparser.cpp \
    serialbuffer.cpp \
    quad3dserialtransfer.cpp \
    abstractserialtransfer.cpp \
    serialmanager.cpp \
    flashhelper.cpp \
    quad3dportchecker.cpp \
    utils.cpp

HEADERS +=\
        libserial_global.h \
    abstractcommandparser.h \
    quad3dcommandparser.h \
    serialbuffer.h \
    abstractserialtransfer.h \
    quad3dserialtransfer.h \
    serialmanager.h \
    serialmanager_p.h \
    flashhelper.h \
    abstractportchecker.h \
    quad3dportchecker.h \
    abstractserialfactory.h \
    abstractserialmanager.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/release/ -lvtio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/debug/ -lvtio
else:unix: LIBS += -L$$OUT_PWD/../IOHelper/ -lvtio

INCLUDEPATH += $$PWD/../IOHelper
DEPENDPATH += $$PWD/../IOHelper
