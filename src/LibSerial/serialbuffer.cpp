#include "serialbuffer.h"

#include <QDebug>

SerialBuffer::SerialBuffer()
{
    memset(m_buffer, 0, sizeof(m_buffer));
    m_head = 0;
    m_tail = 0;
}

void SerialBuffer::append(const char *data, int len)
{
    if ((quint32)(m_tail + len) < sizeof(m_buffer)) {
#ifdef Q_OS_WIN32
        memcpy_s(m_buffer + m_tail, sizeof(m_buffer) - m_tail, data, len);
#else
        memcpy(m_buffer + m_tail, data, len);
#endif
        m_tail += len;
    }
    else if ((size_t)(size() + len) <= sizeof(m_buffer)) {
        char tmp[sizeof(m_buffer)] = { 0 };
#ifdef Q_OS_WIN32
        memcpy_s(tmp, sizeof(tmp), m_buffer + m_head, m_tail - m_head);
        memset(m_buffer + m_head, 0, m_tail - m_head);
        memcpy_s(m_buffer, sizeof(m_buffer), tmp, m_tail - m_head);
        memcpy_s(m_buffer + m_tail - m_head, sizeof(m_buffer) - m_tail + m_head, data, len);
#else
        memcpy(tmp, m_buffer + m_head, m_tail - m_head);
        memset(m_buffer + m_head, 0, m_tail - m_head);
        memcpy(m_buffer, tmp, m_tail - m_head);
        memcpy(m_buffer + m_tail - m_head, data, len);
#endif

        m_tail = m_tail - m_head + len;
        m_head = 0;

        Q_ASSERT(m_tail >= m_head);
        Q_ASSERT((quint32)m_tail <= sizeof(m_buffer));
    }
    else {
        qDebug() << "buffer overflow, current size" << size()
                    << "new data size" << len;
    }
}

void SerialBuffer::remove(int numberElementsToDelete)
{
    if (size() == 0) return;
    else if (m_head + numberElementsToDelete > m_tail) {
        numberElementsToDelete = m_tail - m_head;
        qDebug() << "Too many elements to delete";
    }

    memset(m_buffer + m_head, 0, numberElementsToDelete);
    m_head += numberElementsToDelete;

    Q_ASSERT(m_head <= m_tail);
    Q_ASSERT((quint32)m_head <= sizeof(m_buffer));
}

char *SerialBuffer::data()
{
    return m_buffer + m_head;
}

const char *SerialBuffer::constData() const
{
    return m_buffer + m_head;
}

char SerialBuffer::at(int pos) const
{
    Q_ASSERT((size_t)(m_head + pos) <= sizeof(m_buffer));

    return m_buffer[m_head + pos];
}

int SerialBuffer::size() const
{
    return m_tail - m_head;
}
