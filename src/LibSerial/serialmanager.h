#ifndef SERIALMANAGER_H
#define SERIALMANAGER_H

#include "abstractserialfactory.h"
#include "quad3dserialtransfer.h"
#include "libserial_global.h"

#include <QThread>
#include <QStringList>

class SerialManagerPrivate;
class QSerialPort;

class LIBSERIALSHARED_EXPORT SerialManager : public QThread, AbstractSerialFactory
{
    Q_OBJECT
public:
    enum PortStatus
    {
        PortConnected = 100,
        PortDisconneted = -100,
        PortConnecting = 200,
        PortFlashing = 0
    };

    enum PortEvent
    {
        PortListAvailableChanged = 1
    };

    explicit SerialManager(QObject *parent = 0);
    ~SerialManager();

    bool isPortConnected() const;
    QSerialPort* getPort() const;
    QString getPortName() const;
    QStringList getListPortName() const;

    void addProductId(const QVariant&);
    void addVendorId(const QVariant&);

Q_SIGNALS:
    void _portStatusChanged(int);
    void _portEventChanged(int, const QVariant&);

    void _responseToUpdateUi(const QVariant&);
    void _responseToUpdateUi(qint32, qint32);

public slots:
    void readSingleAddress(const QVariant&);
    void writeConfig(const QVariant&, const QVariant&, bool blocking = false);

    QStringList scanCom();

    bool checkPort(const QString&, bool connectIfSuccess = true);
    bool checkPort(QSerialPort *, bool connectIfSuccess = true);

    bool isValidForUpdateFirmware(const QString&);

    void closeConnection();

protected slots:
    virtual void onConnect();
    virtual void onDisconnect();

protected:
    virtual AbstractSerialTransfer *createSerialTransfer();
    virtual AbstractPortChecker *createPortChecker();

    SerialManagerPrivate* const d;
    friend class SerialManagerPrivate;

};

#endif // SERIALMANAGER_H
