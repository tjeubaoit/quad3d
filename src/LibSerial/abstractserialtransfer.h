#ifndef ABSTRACTSERIALTRANSFER_H
#define ABSTRACTSERIALTRANSFER_H

#include "libserial_global.h"

#include <QObject>

class QSerialPort;
class QMutex;

class LIBSERIALSHARED_EXPORT AbstractSerialTransfer : public QObject
{
    Q_OBJECT

public:
    AbstractSerialTransfer(QObject *parent = 0);
    ~AbstractSerialTransfer();

    QSerialPort* getPort() const;
    void setPort(QSerialPort *port);

    void setPortMutex(QMutex* portMutex);
    void lockMutex();
    void unlockMutex();

Q_SIGNALS:
    void _responseToUpdateUi(const QVariant&);
    void _responseToUpdateUi(qint32, qint32);

public slots:
    virtual void writeRequestSingleAddress(const QVariant&) = 0;
    virtual void writeApplyConfig(const QVariant&, const QVariant&) = 0;

    virtual void readResponseData() = 0;

protected:
    class AbstractSerialTransferPrivate;
    AbstractSerialTransferPrivate* const d;
};

#endif // ABSTRACTSERIALTRANSFER_H
