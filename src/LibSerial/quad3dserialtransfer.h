#ifndef QUAD3DSERIALTRANSFER_H
#define QUAD3DSERIALTRANSFER_H

#include "abstractserialtransfer.h"
#include "libserial_global.h"

class QSerialPort;
class QMutex;

class LIBSERIALSHARED_EXPORT Quad3DSerialTransfer : public AbstractSerialTransfer
{
public:
    Quad3DSerialTransfer(QObject *parent = 0);
    ~Quad3DSerialTransfer();

    void writeRequestSingleAddress(const QVariant&);
    void writeApplyConfig(const QVariant&, const QVariant&);

    void readResponseData();

protected:
    class Quad3DSerialTransferPrivate;
    Quad3DSerialTransferPrivate* const d;
};

#endif // QUAD3DSERIALTRANSFER_H
