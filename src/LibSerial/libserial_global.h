#ifndef LIBSERIAL_GLOBAL_H
#define LIBSERIAL_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QMutex>

#if defined(LIBSERIAL_LIBRARY)
#  define LIBSERIALSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBSERIALSHARED_EXPORT Q_DECL_IMPORT
#endif

#ifndef NO_USE_GLOBAL_FUNCTIONS
#   define SLEEP(delay) QThread::currentThread()->msleep(delay);
#   define TO_BYTES(str) QString(str).toUtf8();
#endif

class SyncHelper
{
public:
    static QMutex g_mutex;
};

class QSerialPort;

bool configureForPort(QSerialPort *port);
bool openPort(QSerialPort* port);

#endif // LIBSERIAL_GLOBAL_H
