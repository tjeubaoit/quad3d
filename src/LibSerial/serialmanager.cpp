#include "serialmanager.h"
#include "serialmanager_p.h"
#include "flashhelper.h"
#include "quad3dcommandparser.h"
#include "quad3dportchecker.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QCoreApplication>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

QMutex SyncHelper::g_mutex;


/**
 * @brief Quad3DSerialManagerPrivate::Quad3DSerialManagerPrivate
 * @param q_ptr
 * @param parent
 */
SerialManagerPrivate::SerialManagerPrivate(SerialManager* q_ptr, QObject* parent)
    : QObject(parent),
    q(q_ptr),
    transfer(q->createSerialTransfer()),
    comPort(NULL)
{
    transfer->moveToThread(&readerThread);

    connect(transfer, SIGNAL(_responseToUpdateUi(QVariant)),
            q, SIGNAL(_responseToUpdateUi(QVariant)));
    connect(transfer, SIGNAL(_responseToUpdateUi(qint32,qint32)),
            q, SIGNAL(_responseToUpdateUi(qint32,qint32)));
}

SerialManagerPrivate::~SerialManagerPrivate()
{
    if (transfer)
        delete transfer;
}

void SerialManagerPrivate::startWorkerThread()
{
    transfer->setPort(comPort);
    transfer->setPortMutex(&SyncHelper::g_mutex);

    if (!readerThread.isRunning()) {
        readerThread.start(QThread::HighPriority);
        comPort->moveToThread(&readerThread);
    }
}

void SerialManagerPrivate::stopWorkerThreads()
{
    if (readerThread.isRunning()) {
        readerThread.quit();
        readerThread.wait(1000);
    }

    transfer->setPort(NULL);
}

void SerialManagerPrivate::handleSerialPortError(
        const QSerialPort::SerialPortError &error)
{
    qDebug() << "error code" << error;
    switch (error)
    {
    case QSerialPort::NotOpenError:
    case QSerialPort::DeviceNotFoundError:
    case QSerialPort::PermissionError:
    case QSerialPort::OpenError:
    case QSerialPort::ParityError:
    case QSerialPort::FramingError:
    case QSerialPort::BreakConditionError:
    case QSerialPort::ResourceError:
    case QSerialPort::UnknownError:
    case QSerialPort::WriteError:
    case QSerialPort::ReadError:
    case QSerialPort::UnsupportedOperationError:
        q->onDisconnect();
        break;
    case QSerialPort::TimeoutError:
    case QSerialPort::NoError:
        return;
    }
}

void SerialManagerPrivate::closeAndDestroyPort()
{
    if (comPort != NULL) {
        if (comPort->isOpen()) {
            comPort->close();
        }
        delete comPort;
        comPort = 0;
    }
}


/**
 * @brief SerialManager::SerialManager
 * @param ui
 * @param parent
 */
SerialManager::SerialManager(QObject *) :
    d(new SerialManagerPrivate(this, this))
{
    qRegisterMetaType<QSerialPort::SerialPortError>("QSerialPort::SerialPortError");
}

SerialManager::~SerialManager()
{
    d->stopWorkerThreads();
    d->closeAndDestroyPort();

    delete d;
}

bool SerialManager::isPortConnected() const
{
    return (d->comPort != 0 && d->comPort->isOpen());
}

QSerialPort *SerialManager::getPort() const
{
    return d->comPort;
}

QString SerialManager::getPortName() const
{
    return d->comPort != 0 ? d->comPort->portName() : QString();
}

QStringList SerialManager::getListPortName() const
{
    QStringList portList;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        portList.append(info.portName());
    }
    return portList;
}

void SerialManager::addProductId(const QVariant &producId)
{
    d->productIds.append((quint16) producId.toInt());
}

void SerialManager::addVendorId(const QVariant &vendorId)
{
    d->vendorIds.append((quint16) vendorId.toInt());
}

QStringList SerialManager::scanCom()
{
    QStringList list;

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        if (!d->productIds.contains(info.productIdentifier())
                || !d->vendorIds.contains(info.vendorIdentifier())) {

#ifdef RELEASE_VERSION
            qDebug() << "product id or vendor id not match"
                      << info.vendorIdentifier() << " "
                      << info.productIdentifier();
            continue;
#endif
        }
        if (info.isNull()) {
            qDebug() << info.portName() << " null";
            continue;
        }
        else if (!info.isValid()) {
            qDebug() << info.portName() << " invalid";
            continue;
        }
        else if (info.isBusy()) {
            qDebug() << info.portName() << " busy";
            continue;
        }

        list.append(info.portName());
    }

    return list;
}

bool SerialManager::checkPort(const QString &name, bool keepConnectIfSuccess)
{
    QSerialPort *port = new QSerialPort(name);
    bool ok = checkPort(port, keepConnectIfSuccess);
    if (!ok || !keepConnectIfSuccess) {
        if (port->isOpen()) {
            port->close();
        }
        delete port;
    }

    return ok;
}

bool SerialManager::checkPort(QSerialPort *port, bool keepConnectIfSuccess)
{
    if (!port) return false;
    AbstractPortChecker *portChecker = createPortChecker();
    bool ok = false;
    if (openPort(port) && portChecker->checkPort(port)) {
        if (keepConnectIfSuccess) {
            d->comPort = port;
            onConnect();
        }
        ok = true;
    }
    delete portChecker;

    return ok;
}

bool SerialManager::isValidForUpdateFirmware(const QString &name)
{
#ifdef RELEASE_VERSION
    QSerialPortInfo info(name);
    if (!d->productIds.contains(info.productIdentifier())
            || !d->vendorIds.contains(info.vendorIdentifier()))
    {
        return false;
    }
#endif

    QSerialPort port(name);
    bool ok = openPort(&port);
    if (port.isOpen()) port.close();

    return ok;
}

void SerialManager::closeConnection()
{
    onDisconnect();
}

void SerialManager::onConnect()
{
    emit _portStatusChanged(SerialManager::PortConnected);

    connect(d->comPort, SIGNAL(readyRead()),
            d->transfer, SLOT(readResponseData()), Qt::UniqueConnection);

    connect(d->comPort, SIGNAL(error(QSerialPort::SerialPortError)),
            d, SLOT(handleSerialPortError(QSerialPort::SerialPortError)),
            (Qt::ConnectionType) (Qt::QueuedConnection | Qt::UniqueConnection));

    d->startWorkerThread();
#ifdef QT_DEBUG
    qDebug() << "connected";
#endif
}

void SerialManager::onDisconnect()
{
    emit _portStatusChanged(SerialManager::PortDisconneted);

    if (isPortConnected()) {
        disconnect(d->comPort, SIGNAL(readyRead()), 0, 0);
        disconnect(d->comPort, SIGNAL(error(QSerialPort::SerialPortError)), 0, 0);

        QCoreApplication::removePostedEvents(this);
    }

    d->stopWorkerThreads();
    d->closeAndDestroyPort();
#ifdef QT_DEBUG
    qDebug() << "disconnected";
#endif
}

void SerialManager::readSingleAddress(const QVariant &data)
{
    Q_ASSERT(d->comPort->isOpen());
    d->transfer->writeRequestSingleAddress(data);
}

void SerialManager::writeConfig(const QVariant &addr, const QVariant &val, bool blocking)
{
    Q_ASSERT(d->comPort->isOpen());
    QCoreApplication::removePostedEvents(this);
    Quad3DSerialTransfer *quad3dTransfer =
            reinterpret_cast<Quad3DSerialTransfer*> (d->transfer);
    QFuture<void> future = QtConcurrent::run(
                quad3dTransfer, &Quad3DSerialTransfer::writeApplyConfig, addr, val);
    if (blocking)
        future.waitForFinished();
}

AbstractSerialTransfer *SerialManager::createSerialTransfer()
{
    return new Quad3DSerialTransfer;
}

AbstractPortChecker *SerialManager::createPortChecker()
{
    return new Quad3DPortChecker;
}






