#ifndef QUAD3DPORTCHECKER_H
#define QUAD3DPORTCHECKER_H

#include "abstractportchecker.h"
#include "libserial_global.h"

class LIBSERIALSHARED_EXPORT Quad3DPortChecker : public AbstractPortChecker
{
public:
    Quad3DPortChecker() : AbstractPortChecker() {}

    bool checkPort(QSerialPort *port);
};

#endif // QUAD3DPORTCHECKER_H
