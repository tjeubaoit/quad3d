#ifndef SERIALMANAGER_P_H
#define SERIALMANAGER_P_H

#include <QThread>
#include <QSerialPort>

class SerialManager;
class AbstractSerialTransfer;

class SerialManagerPrivate : public QObject
{
    Q_OBJECT

public:
    SerialManagerPrivate(SerialManager* q_ptr, QObject* parent = 0);
    ~SerialManagerPrivate();

    SerialManager* const q;
    friend class SerialManager;

    AbstractSerialTransfer *transfer;
    QThread readerThread;

    QSerialPort *comPort;

    QList<quint16> productIds;
    QList<quint16> vendorIds;

    bool openPort(QSerialPort*);
    bool configureForPort(QSerialPort*);

    void startWorkerThread();
    void stopWorkerThreads();
    void closeAndDestroyPort();

private slots:
    virtual void handleSerialPortError(const QSerialPort::SerialPortError &);
};

#endif // SERIALMANAGER_P_H
