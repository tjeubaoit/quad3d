#include "abstractserialtransfer.h"

#include <QSerialPort>
#include <QMutex>

class AbstractSerialTransfer::AbstractSerialTransferPrivate
{
public:
    QSerialPort *port;
    QMutex *mutex;
};

AbstractSerialTransfer::AbstractSerialTransfer(QObject *parent)
    : QObject(parent),
      d(new AbstractSerialTransferPrivate)
{
}

AbstractSerialTransfer::~AbstractSerialTransfer()
{
    delete d;
}

QSerialPort *AbstractSerialTransfer::getPort() const
{
    return d->port;
}

void AbstractSerialTransfer::setPort(QSerialPort *port)
{
    d->port = port;
}

void AbstractSerialTransfer::setPortMutex(QMutex *portMutex)
{
    d->mutex = portMutex;
}

void AbstractSerialTransfer::lockMutex()
{
    d->mutex->lock();
}

void AbstractSerialTransfer::unlockMutex()
{
    d->mutex->unlock();
}
