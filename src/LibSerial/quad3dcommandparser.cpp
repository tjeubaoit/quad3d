#include "quad3dcommandparser.h"

#include <QDebug>
#include <stdio.h>

#define ACTION_READ 0
#define ACTION_WRITE 1

typedef struct
{
    union {
        struct {
            quint8 sa;
            quint8 sb;
            union {
                struct {
                    quint32 address : 10;
                    quint32 num_asks : 5;
                    quint32 action : 1;
                };
                char ask_info_array[4];
            } __attribute__((packed, aligned(1)));
            union {
                char value_array[8];
                qint32 value;
            } __attribute__((packed, aligned(1)));
            quint8 cka;
            quint8 ckb;
        };
        char data[16];
    };
} __attribute__((packed, aligned(1))) CommandMessage;

template <typename T>
void reverseArray(T *arr, size_t len)
{
    for (size_t i = 0; i < len/2; i++) {
        T tmp = *(arr + i);
        *(arr + i) = *(arr + len - i - 1);
        *(arr + len - i - 1) = tmp;
    }
}

void updateChecksum(char *arr, size_t len, quint8 &cka, quint8 &ckb)
{
    for (size_t i = 0; i < len; i++) {
        cka += arr[i];
        ckb += cka;
    }
}

Quad3DCommandParser::Quad3DCommandParser(QObject *parent)
    : AbstractCommandParser(parent)
{
}

bool Quad3DCommandParser::nextResult(qint16 *address, qint16 *value, char *ex)
{
    int i = 0, size = d->serialBuffer.size();
    while (i++ < size && !isStartOfNewMessage()) {
        d->serialBuffer.remove(1);
    }

    if ((quint8)d->serialBuffer.at(0) == 170 && (quint8)d->serialBuffer.at(1) == 204) {
        Q_ASSERT(ex != 0);
        quint8 len = (quint8)d->serialBuffer.at(2);
        if (d->serialBuffer.size() >= len + 3) {
            memcpy(ex, d->serialBuffer.constData() + 3, len);
            d->serialBuffer.remove(len + 3);
            return true;
        }
        return false;
    }

    CommandMessage mes;
    memset(&mes, 0, sizeof(mes));
    quint8 cka = 0, ckb = 0;

    if (d->serialBuffer.size() < 4) {
#ifdef QT_DEBUG
//        qDebug() << "header not enough, return";
#endif
        return false;
    }

    mes.sa = d->serialBuffer.at(0);
    mes.sb = d->serialBuffer.at(1);

#ifdef Q_OS_WIN32
    memcpy_s(mes.ask_info_array, sizeof(mes.ask_info_array),
             d->serialBuffer.constData() + 2, 2);
#else
    memcpy(mes.ask_info_array, d->serialBuffer.constData() + 2, 2);
#endif
    updateChecksum(mes.ask_info_array, 2, cka, ckb);
    reverseArray(mes.ask_info_array, 2);

    if (mes.num_asks*2 + 6 > d->serialBuffer.size()) {
#ifdef QT_DEBUG
//        qDebug() << "data not enough, return";
#endif
        return false;
    }

    const quint32 len = mes.num_asks*2 + 6;
    char *tmp = new char[len];
#ifdef Q_OS_WIN32
    memcpy_s(tmp, len, d->serialBuffer.constData(), len);
#else
    memcpy(tmp, d->serialBuffer.constData(), len);
#endif
    d->serialBuffer.remove(len);

    memset(mes.value_array, 0, sizeof(mes.value_array));
#ifdef Q_OS_WIN32
    memcpy_s(mes.value_array, sizeof(mes.value_array), tmp + 4, mes.num_asks*2);
#else
    memcpy(mes.value_array, tmp + 4, mes.num_asks*2);
#endif
    mes.cka = tmp[len - 2];
    mes.ckb = tmp[len - 1];

    delete[] tmp;

    updateChecksum(mes.value_array, mes.num_asks*2, cka, ckb);
    if (cka != mes.cka || ckb != mes.ckb) {
#ifdef QT_DEBUG
//        qDebug() << "checksum not match, return";
#endif
        return false;
    }

    reverseArray(mes.value_array, mes.num_asks*2);

    if (address != 0 && value != 0) {
        *address = mes.address;
        *value = mes.value;
    }

    return true;
}

quint32 Quad3DCommandParser::generateCommand(qint16 address, qint16 value, char *result)
{
    CommandMessage mes;
    memset(&mes, 0, sizeof(mes));

    mes.action = ACTION_WRITE;
    mes.num_asks = 1;
    mes.address = address;
    mes.value = value;

    const size_t len = mes.num_asks*2 + 6;
    const size_t datalen = mes.num_asks*2;

    memset(result, 0, len);
    result[0] = 170;
    result[1] = 202;

    reverseArray(mes.ask_info_array, 2);
    reverseArray(mes.value_array, datalen);
#ifdef Q_OS_WIN32
    memcpy_s(result + 2, 2, mes.ask_info_array, 2);
    memcpy_s(result + 4, datalen, mes.value_array, datalen);
#else
    memcpy(result + 2, mes.ask_info_array, 2);
    memcpy(result + 4, mes.value_array, datalen);
#endif

    updateChecksum(result + 2, datalen + 2, mes.cka, mes.ckb);
    result[len - 2] = mes.cka;
    result[len - 1] = mes.ckb;

    return len;
}

quint32 Quad3DCommandParser::generateCommand(qint16 address, char *result)
{
    CommandMessage mes;
    memset(&mes, 0, sizeof(mes));

    mes.action = ACTION_READ;
    mes.num_asks = 1;
    mes.address = address;

    memset(result, 0, 4);
    result[0] = 170;
    result[1] = 202;

    reverseArray(mes.ask_info_array, 2);
#ifdef Q_OS_WIN32
    memcpy_s(result + 2, 2, mes.ask_info_array, 2);
#else
    memcpy(result + 2, mes.ask_info_array, 2);
#endif

    return 4;
}

bool Quad3DCommandParser::isStartOfNewMessage()
{
    quint8 byteLeft = (quint8)d->serialBuffer.at(0);
    quint8 byteRight = (quint8)d->serialBuffer.at(1);
    return (byteLeft == 170 && (byteRight == 202 || byteRight == 204));
}
