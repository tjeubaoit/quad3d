TEMPLATE = subdirs

SUBDIRS += CustomWidgets \
    AdaptorViews \
    MainMvcApplication \
    LibSerial \
    IOHelper \
    CustomFrame \
    LiveGraph
