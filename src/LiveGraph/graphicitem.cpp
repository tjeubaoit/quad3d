#include "graphicitem.h"
#include "abstractgraphitemdata.h"

#include <QPainter>
#include <QPen>
#include <QPointF>
#include <QWidget>
#include <QDebug>
#include <QToolTip>


class GraphicItem::GraphicItemPrivate
{
public:
    QList<AbstractGraphItemData*> data;
    QString hexColor;
    int maxNumberData;
    int minVal, maxVal;
    bool hovering;
};

GraphicItem::GraphicItem(const QString &hexColor, int count, int min, int max)
    : d(new GraphicItemPrivate)
{
    d->hexColor = hexColor;
    d->minVal = min;
    d->maxVal = max;
    d->maxNumberData = count;
}

GraphicItem::~GraphicItem()
{
    delete d;
}

void GraphicItem::draw(const QRect& container, QPainter *painter)
{
    QList<QPointF> listPoints;
    for (int i = 0; i < d->data.size(); i++) {
        double val = d->data[i]->getValue().toDouble();
        if (val < 0) listPoints.append(QPointF(-1, -1));
        else {
            double dx = (double)(d->maxNumberData - (d->data.size() - i - 1))
                    / d->maxNumberData * container.width();
            double dy = (d->maxVal - val)
                    / (d->maxVal - d->minVal) * container.height();
            listPoints.append(QPointF(dx, dy));
        }
    }

    for (int i = 0; i < listPoints.size() - 1; i++) {
        double y1 = listPoints.at(i).y();
        double y2 = listPoints.at(i + 1).y();
        if (y1 < 0 || y2 < 0)
            continue;
        painter->drawLine(listPoints.at(i), listPoints.at(i + 1));
    }
}

void GraphicItem::drawInfoText(const QRect &rect, QPainter *painter)
{
    if (dataCount() > 0) {
        painter->setBrush(QColor(d->hexColor));
        painter->drawRect(rect);
        QString txt = d->data.last()->getToolTipText().last();
        painter->drawText(rect.x() + 10, rect.y() + rect.height(), txt);
    }
}

void GraphicItem::addRecord(AbstractGraphItemData *itemData)
{
    d->data.append(itemData);
    if (d->data.size() > d->maxNumberData + 1) {
        d->data.removeFirst();
    }
}

void GraphicItem::addLastRecord()
{
    if (!d->data.isEmpty()) {
        AbstractGraphItemData *lastItem = d->data.last();
        addRecord(lastItem);
    }
}

QString GraphicItem::hexColor() const
{
    return d->hexColor;
}

void GraphicItem::setHexColor(const QString &hexColor)
{
    d->hexColor = hexColor;
}

int GraphicItem::maxNumberData() const
{
    return d->maxNumberData;
}

void GraphicItem::setMaxNumberData(int maxNumData)
{
    d->maxNumberData = maxNumData;
}

void GraphicItem::setRange(int min, int max)
{
    d->minVal = min;
    d->maxVal = max;
}

int GraphicItem::dataCount() const
{
    return d->data.size();
}

QString GraphicItem::getToolTipAtPos(int dx, const QRect &container)
{
    QString result;
    double tmp = (d->maxNumberData - (dx * d->maxNumberData/(double)container.width()));
    int index = (int)(d->data.size() - tmp - 1);
    if (index >= 0 && index < d->data.size()) {
        QStringList list = d->data.at(index)->getToolTipText();
        result.append(list.first()).append("\n").append(list.last());
    }
    return result;
}

bool GraphicItem::isHovering() const
{
    return d->hovering;
}

void GraphicItem::setHovering(bool isHovering)
{
    d->hovering = isHovering;
}




