#-------------------------------------------------
#
# Project created by QtCreator 2014-09-03T11:08:31
#
#-------------------------------------------------

QT       += widgets

TARGET = vtlivegraph
TEMPLATE = lib

DEFINES += LIVEGRAPH_LIBRARY

SOURCES += livegraph.cpp \
    graphicitem.cpp

HEADERS += livegraph.h\
        livegraph_global.h \
    abstractgraphitemdata.h \
    graphicitem.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES +=
