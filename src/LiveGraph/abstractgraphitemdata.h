#ifndef ABSTRACTGRAPHITEMDATA_H
#define ABSTRACTGRAPHITEMDATA_H

#include <QVariant>
#include <QStringList>

class AbstractGraphItemData
{
public:
    virtual QVariant getValue() const = 0;
    virtual QStringList getToolTipText() const = 0;
    virtual bool larger(const AbstractGraphItemData *) const = 0;
};
#endif // ABSTRACTGRAPHITEMDATA_H
