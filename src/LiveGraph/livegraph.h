#ifndef LIVEGRAPH_H
#define LIVEGRAPH_H

#include "livegraph_global.h"
#include <QWidget>

class GraphicItem;
class AbstractGraphItemData;

class LIVEGRAPHSHARED_EXPORT LiveGraph : public QWidget
{

public:
    enum HeaderLocation
    {
        TopLeft = 1,
        TopRight = 2,
        BottomLeft = 3,
        BottomRight = 4,
        Center = 5
    };

    enum GraphType
    {
        Online,
        Offline
    };

    LiveGraph(QWidget* parent = 0);
    ~LiveGraph();

    void setShowTextLastValsAtTop(bool arg);
    void setNoDrawBorder(bool arg);

    void setHeader(HeaderLocation location, const QString &text);
    int itemCounts() const;

public Q_SLOTS:
    void insertItem(int index, int maxPoints, int min, int max);
    void addNewItem(int maxPoints, int min, int max);
    void appendDataToItem(int index, AbstractGraphItemData *itemData);
    void appendLastDataToItem(int index);

protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void drawFrame(QPainter *painter);
    virtual void drawTextTitle(QPainter *painter);
    virtual void drawGraphicItems(QPainter *painter);

    virtual void mouseMoveEvent(QMouseEvent *event);

private:
    class LiveGraphPrivate;
    LiveGraphPrivate* const d;

};

#endif // LIVEGRAPH_H
