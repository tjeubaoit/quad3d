#ifndef APPCONFIGMANAGER_H
#define APPCONFIGMANAGER_H

#include <iohelper_global.h>
#include <QHash>
#include <QString>
#include <QVariant>

class IOHELPERSHARED_EXPORT AppConfigManager
{
public:
    explicit AppConfigManager(const QString&);
    ~AppConfigManager();

    void saveConfigToFile();

    bool hasKey(const QString&);

    QString get(const QString&, const QString& = QString());
    void put(const QString&, const QString&);

private:
    class AppConfigManagerPrivate;
    AppConfigManagerPrivate* const d;
};

#endif // APPCONFIGMANAGER_H
