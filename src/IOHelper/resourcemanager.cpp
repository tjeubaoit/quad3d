#include "resourcemanager.h"
#include <QFile>
#include <QXmlStreamReader>
#include <QDebug>
#include <QCoreApplication>

class ResourceManager::ResourceManagerPrivate
{
public:
    QHash<int, QString> hash;
};

ResourceManager::ResourceManager(const QString &xmlFilePath)
    : d(new ResourceManagerPrivate)
{
    QFile file(xmlFilePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QXmlStreamReader xmlReader(&file);
    while (!xmlReader.atEnd() && !xmlReader.hasError()) {
        QXmlStreamReader::TokenType token = xmlReader.readNext();
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement) {
            if (xmlReader.name() == "strings") {
                continue;
            }
            if (xmlReader.name() == "item") {
                QXmlStreamAttributes attrs = xmlReader.attributes();
                if (attrs.hasAttribute("key")) {
                    int key = attrs.value("key").toInt();
                    xmlReader.readNext();
                    if (xmlReader.tokenType() != QXmlStreamReader::Characters) {
                        continue;
                    }
                    d->hash.insert(key, xmlReader.text().toString());
                }
            }
        }
    }
}

ResourceManager *ResourceManager::instance(const QString &xmlFilePath)
{
    static ResourceManager* s_ResourceManagerSingleton = 0;
    if (s_ResourceManagerSingleton == 0) {
        s_ResourceManagerSingleton = new ResourceManager(xmlFilePath);
        s_ResourceManagerSingleton->setParent(QCoreApplication::instance());
    }
    return s_ResourceManagerSingleton;
}

ResourceManager::~ResourceManager()
{
    delete d;
}

QString ResourceManager::getValue(int key)
{
    return d->hash.value(key);
}
