#ifndef HELPER_H
#define HELPER_H

#include "iohelper_global.h"
#include <QMap>

QString IOHELPERSHARED_EXPORT qLoadHtmlString(const QString& fileName);

int IOHELPERSHARED_EXPORT qSaveParametersToFile(
        const QString& filePath, const QMap<qint32, qint32>& hash);

int IOHELPERSHARED_EXPORT qLoadParametersFromFile(
        const QString& filePath, QMap<qint32, qint32>* hash);

#endif // HELPER_H
