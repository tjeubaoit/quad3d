#-------------------------------------------------
#
# Project created by QtCreator 2014-07-01T09:32:56
#
#-------------------------------------------------

QT       += core network xml

QT       -= gui

TARGET = vtio
TEMPLATE = lib

DEFINES += IOHELPER_LIBRARY

SOURCES += \
    downloadhelper.cpp \
    resourcemanager.cpp \
    appconfigmanager.cpp \
    filehelper.cpp

HEADERS +=\
        iohelper_global.h \
    downloadhelper.h \
    filehelper.h \
    resourcemanager.h \
    appconfigmanager.h \
    downloadhelper_p.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
