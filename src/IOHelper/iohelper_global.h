#ifndef IOHELPER_GLOBAL_H
#define IOHELPER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(IOHELPER_LIBRARY)
#   define IOHELPERSHARED_EXPORT Q_DECL_EXPORT
#   define SLEEP(ms) QThread::currentThread()->msleep(ms);
#else
#  define IOHELPERSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // IOHELPER_GLOBAL_H
