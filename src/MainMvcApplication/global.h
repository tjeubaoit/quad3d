#ifndef GLOBAL_H
#define GLOBAL_H

#define APPLICATION_NAME "Apollo 3D Monster Configuration 0.2.25"
#define APPLICATION_ORGANIZATION "Monster"
#define APPLICATION_VERSION "0.2.25 beta"
#define APP_PROFILE_FILE_EXTENSION ".afp"
#define APP_PROFILE_FILE_PREFIX "conf/profile"
#define APP_CONFIGURATION_FILE_PATH "conf/app.afc"

#define URL_APP_ICON ":/Apollo.ico"

#define MAX_TAB_COUNT 5

#define PRODUCT_ID 24577
#define VENDOR_ID 1027

#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 600
#define WINDOW_TITLE_HEIGHT 40

#define TAB_WIDGET_WIDTH 940
#define TAB_WIDGET_HEIGHT 420

#define RX_MONITOR_WIDTH 210
#define RX_MONITOR_HEIGHT 210
#define RX_SENSOR_WIDTH 18
#define RX_SENSOR_HEIGHT 18

#define RX_MIN_VAL 920
#define RX_MAX_VAL 2120
#define RX_CENTER_VAL 1520

#define TRIM_MIN -100
#define TRIM_MAX 100

#define SLEEP(delay) QThread::currentThread()->msleep(delay);
#define FILL(text, len) while (text.length() < len) { text.insert(0, "0"); }
#define TO_BYTES(str) QString(str).toUtf8();
#define qRes(txt) ResourceManager::instance()->getValue(txt)


class Quad3D
{
public:
    enum RxType
    {
        Tradition = 1,
        SBus = 2,
        PPM = 3,
        DSM2 = 4,
        DSMX = 5
    };

    enum Tabs
    {
        TabRx = 0,
        TabConfig = 1,
        TabPidTuning = 2,
        TabSensor = 3,
        TabFirmware = 4
    };

    enum FlyMode
    {
        Mode_2D = 1,
        Mode_3D = 2
    };

    enum MotorRotationDirectionType
    {
        MotorRotationType_X = 1,
        MotorRotationType_Plus = 0
    };
};

#endif // GLOBAL_H
