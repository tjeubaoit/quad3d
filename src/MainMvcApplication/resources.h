#ifndef RESOURCES_H
#define RESOURCES_H

#define style_label_connected "QLabel { border-image:url(images/connected.png); }"
#define style_label_disconnected "QLabel { border-image:url(images/disconnected.png); }"

#define url_checked_icon "images/button-checked.png"
#define url_highlight_button_config_1 "images/config/quadx200.png"
#define url_highlight_button_config_2 "images/config/quad+200.png"
#define url_highlight_button_config_3 "images/config/hexx200.png"
#define url_highlight_button_config_4 "images/config/hex+200.png"
#define url_highlight_button_config_5 "images/config/hexy6200.png"
#define url_highlight_button_config_6 "images/config/hexv6200.png"

#define url_xml_strings_en ""
#define url_splash_screen ""

#endif // RESOURCES_H
