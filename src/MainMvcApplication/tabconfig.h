#ifndef TABCONFIG_H
#define TABCONFIG_H

#include "ui_tabconfig.h"
#include "basetabwidget.h"

#include <QWidget>

class ButtonGroup;
class QPixmap;

class TabConfig : public BaseTabWidget, public Ui::TabConfig
{
    Q_OBJECT

public:
    explicit TabConfig(QWidget *parent = 0);
    ~TabConfig();

    ButtonGroup *btGroupConfig;
    QPixmap *m_highLightPixmap[6];

Q_SIGNALS:
    void _motorRotationConfiguration_changed(int);

protected:
    bool eventFilter(QObject *, QEvent *);

private Q_SLOTS:
    void buttonGroupAdaptor_button_clicked(QObject*, int, bool*);
    void buttonGroup_button_toggled(int, bool);
    void comboBox_currentIndexChanged(int);

    void cbMotorTest_activated(int);
    void updateHighLight(int currentConfigId);

private:
    void updateThrottleWidgets(QSlider *slider, QLineEdit *edit);
};

#endif // TABCONFIG_H
