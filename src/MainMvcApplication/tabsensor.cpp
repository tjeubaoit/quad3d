#include "tabsensor.h"
#include "quad3dglview.h"
#include "abstractgraphitemdata.h"
#include "livegraph.h"
#include "global.h"

class QuadSensorLiveGraphItemData : public AbstractGraphItemData
{
public:
    QuadSensorLiveGraphItemData(int angle)
    {
        m_angle = angle + 180;
    }

    QVariant getValue() const { return m_angle; }

    QStringList getToolTipText() const
    {
        QStringList list;
        list.append("Angle: " + QString::number(m_angle));
        return list;
    }

    bool larger(const AbstractGraphItemData *) const { return true; }

private:
    int m_angle;
};

class Quad3dLiveGraph : public LiveGraph
{
public:
    Quad3dLiveGraph(QWidget *parent = 0) : LiveGraph(parent) {}

protected:
    void mouseMoveEvent(QMouseEvent *) {}
};

void GetDefaultSurfaceFormat(QSurfaceFormat &surface)
{
    surface.setVersion(4, 2);
    surface.setProfile(QSurfaceFormat::CompatibilityProfile);
    surface.setDepthBufferSize(24);
    surface.setRedBufferSize(8);
    surface.setGreenBufferSize(8);
    surface.setBlueBufferSize(8);
    surface.setAlphaBufferSize(8);
    surface.setStencilBufferSize(8);
    surface.setSamples(4);
    surface.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
    surface.setSwapInterval(1);
    surface.setOption(QSurfaceFormat::DeprecatedFunctions);
}

TabSensor::TabSensor(QWidget *parent) :
    BaseTabWidget(parent)
{
    setupUi(this);

    initLiveGraphView();
    initOpenGlView();

    connect(quad3dView, SIGNAL(_angle_changed(int,float)),
            this, SLOT(glView_angleChanged(int,float)));
}

TabSensor::~TabSensor()
{
}

void TabSensor::motorRotationConfiguration_changed(int type)
{
    if (type == Quad3D::MotorRotationType_Plus) {
        quad3dView->setOriginalAngleZ(0);
    } else if (type == Quad3D::MotorRotationType_X) {
        quad3dView->setOriginalAngleZ(45);
    }
}

void TabSensor::glView_angleChanged(int kind, float val)
{
    if (kind == Quad3DGlView::Angle_X) {
        int intVal = (int) val;
        liveGraph_x->appendDataToItem(0, new QuadSensorLiveGraphItemData(intVal));
        liveGraph_x->repaint();
        lbSensorAngle_x->setText(QString::number(intVal));
    }
    else if (kind == Quad3DGlView::Angle_Y) {
        int intVal = (int) val;
        liveGraph_y->appendDataToItem(0, new QuadSensorLiveGraphItemData(intVal));
        liveGraph_y->repaint();
        lbSensorAngle_y->setText(QString::number(intVal));
    }
}

void TabSensor::initLiveGraphView()
{
    liveGraph_x = new Quad3dLiveGraph(this);
    liveGraph_x->setShowTextLastValsAtTop(false);
    liveGraph_x->setNoDrawBorder(false);
    liveGraph_x->setHeader(LiveGraph::TopLeft, "180");
    liveGraph_x->setHeader(LiveGraph::BottomRight, "0");
    liveGraph_x->setHeader(LiveGraph::BottomLeft, "-180 / 1s");
    liveGraph_x->setGeometry(frameGraph1->geometry());
    liveGraph_x->addNewItem(20, 0, 360);

    liveGraph_y = new Quad3dLiveGraph(this);
    liveGraph_y->setShowTextLastValsAtTop(false);
    liveGraph_y->setNoDrawBorder(false);
    liveGraph_y->setHeader(LiveGraph::TopLeft, "180");
    liveGraph_y->setHeader(LiveGraph::BottomRight, "0");
    liveGraph_y->setHeader(LiveGraph::BottomLeft, "-180 / 1s");
    liveGraph_y->setGeometry(frameGraph2->geometry());
    liveGraph_y->addNewItem(20, 0, 360);
}

void TabSensor::initOpenGlView()
{
#ifdef Q_OS_LINUX
    quad3dView = new Quad3DGlView;
#else
    QSurfaceFormat surface;
    GetDefaultSurfaceFormat(surface);
    quad3dView = new Quad3DGlView(surface);
#endif
    quad3dView->resize(frame3D->size());
    quad3dView->show();
    quad3dView->hide();
    quad3dView->setOriginalAngleZ(45);

    QWidget *glContainer = QWidget::createWindowContainer(quad3dView, this);
    glContainer->setGeometry(frame3D->geometry());
}
