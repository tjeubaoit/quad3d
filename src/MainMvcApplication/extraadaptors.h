#ifndef EXTRAADAPTORS_H
#define EXTRAADAPTORS_H

#include "abstractfieldadaptor.h"
#include "sliderwithlineeditadaptor.h"
#include "checkablebuttonadaptor.h"
#include "buttongroupadaptor.h"
#include "labeladaptor.h"

#include "doubledirectslider.h"
#include "rxmonitorview.h"
#include "buttongroup.h"
#include "quad3dglview.h"

#include "global.h"

#include <QComboBox>
#include <QLineEdit>
#include <QSlider>
#include <QButtonGroup>
#include <QLabel>
#include <QDebug>


class ButtonGroupWithVerifyAdaptor : public ButtonGroupAdaptor
{
    Q_OBJECT
public:
    ButtonGroupWithVerifyAdaptor(QObject* field, QObject* parent = 0)
        : ButtonGroupAdaptor(field, parent)
    {}

    virtual void setValue(const QVariant& value)
    {
        ButtonGroupAdaptor::setValue(value);
        ButtonGroup* btgroup = qobject_cast<ButtonGroup*> (m_field);

        int id = GET_VALUE(value);
        if (btgroup->lastButtonCheckedId() != id) {
            btgroup->setLastButtonChecked(btgroup->button(id));
        }
    }

Q_SIGNALS:
    void _buttonClicked(QObject *, int, bool*);

protected:
    virtual void buttonClicked(int id)
    {
        ButtonGroup *btGroup = qobject_cast<ButtonGroup*> (m_field);
        if (btGroup->lastButtonCheckedId() == id) {
            return;
        }
        else m_readOnly = true;

        bool isAccepted = false;
        emit _buttonClicked(m_field, id, &isAccepted);

        if (isAccepted) {
            btGroup->setLastButtonChecked(btGroup->button(id));
            emit _adaptorValueChanged(getValue());
        }
        else if (btGroup->lastButtonChecked() != 0) {
            btGroup->lastButtonChecked()->setChecked(true);
        }

        m_readOnly = false;
    }
};


class CheckableButtonWithVerifyAdaptor : public CheckableButtonAdaptor
{
    Q_OBJECT
public:
    CheckableButtonWithVerifyAdaptor(QObject* field, QObject* parent = 0)
        : CheckableButtonAdaptor(field, parent)
    {}

Q_SIGNALS:
    void _clicked(QObject *, bool, bool*);

protected:
    virtual void clicked(bool checked)
    {
        m_readOnly = true;

        bool isAccepted = false;
        emit _clicked(m_field, checked, &isAccepted);
        if (isAccepted) {
            emit _adaptorValueChanged(getValue());
        } else {
            QAbstractButton *btt = qobject_cast<QAbstractButton*> (m_field);
            btt->toggle();
        }

        m_readOnly = false;
    }
};


class DoubleDirectSliderAdaptor : public SliderWithLineEditAdaptor
{
public:
    DoubleDirectSliderAdaptor(QObject* field, QObject* extraField,
                              QObject* parent = 0)
        : SliderWithLineEditAdaptor(field, extraField, parent)
    {

    }

    QVariant getValue() const
    {
        return SliderWithLineEditAdaptor::getValue().toInt() + 100;
    }

    void setValue(const QVariant &value)
    {
        SliderWithLineEditAdaptor::setValue(GET_VALUE(value) - 100);
    }
};


class RxMonitorAdaptor : public AbstractFieldAdaptor
{
public:
    enum Direction { Horizontal, Vertical };

    RxMonitorAdaptor(RxMonitorAdaptor::Direction direction,
                        QObject* field, QObject* parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
        m_direction = direction;
    }

    QVariant getValue() const { return MINLONG; }

    void setValue(const QVariant &value)
    {
        int val = GET_VALUE(value);
        RxMonitorView *rx = qobject_cast<RxMonitorView*> (m_field);

        if (m_direction == Horizontal) {
            rx->x = val;
        }
        else if (m_direction == Vertical) {
            rx->y = val;
        }
        else return;

        if (rx->need_move) {
            rx->moveSensor();
        } else {
            rx->need_move = true;
        }
    }

private:
    Direction m_direction;
};


class Quad3DGlAdaptor : public AbstractFieldAdaptor
{
public:
    enum Direction { Direction_X, Direction_Y, Direction_Z };

    Quad3DGlAdaptor(Direction direction, QObject *field, QObject *parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
        m_direction = direction;
    }

    QVariant getValue() const { return MINLONG; }

    void setValue(const QVariant &value)
    {
        Quad3DGlView *view = qobject_cast<Quad3DGlView*> (m_field);
        if (m_direction == Direction_X)
            view->setAngleX(value.toFloat());
        else if (m_direction == Direction_Y)
            view->setAngleY(value.toFloat());
        else if (m_direction == Direction_Z)
            view->setAngleZ(value.toFloat());
    }

private:
    Direction m_direction;
};


class CalibLabelStatusAdaptor : public AbstractFieldAdaptor
{
public:
    CalibLabelStatusAdaptor(QObject *field, QObject *parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
    }

    QVariant getValue() const { return MINLONG; }

    void setValue(const QVariant &value)
    {
        QLabel *lb = qobject_cast<QLabel*> (m_field);
        if (value.toInt() == 0) {
            lb->setText("Fail");
        }
        else if (value.toInt() == 99){
            lb->setText("Ok");
        }
        else if (value.toInt() == 69) {
            lb->setText("Calib - ing");
        }
    }
};


class VersionInfoLabelAdaptor : public LabelAdaptor
{
public:
    VersionInfoLabelAdaptor(QObject *field, QObject *parent = 0)
        : LabelAdaptor(field, parent)
    {
    }

    void setValue(const QVariant &value)
    {
        QLabel *lb = qobject_cast<QLabel*> (m_field);
        int versionNumber = value.toInt();
        QString text = QString("%1. %2. %3").arg(versionNumber/100)
                .arg((versionNumber%100)/10).arg(versionNumber%10);
        lb->setText(text);
    }
};

#endif // EXTRAADAPTORS_H
