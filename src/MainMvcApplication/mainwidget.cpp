#include "mainwidget.h"
#include "global.h"
#include "tabrx.h"
#include "tabpidturning.h"
#include "tabsensor.h"
#include "tabconfig.h"
#include "tabfirmware.h"
#include "resources.h"

#include <QPropertyAnimation>

#define ANIMATION_DURATION_WHEN_CHANGE_TAB 200


MainWidget::MainWidget(QWidget *parent) :
    CustomFrame(parent)
{
    setupUi(this);

    CustomFrame::setWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    CustomFrame::setWindowTitleSize(WINDOW_WIDTH, WINDOW_TITLE_HEIGHT);
    CustomFrame::setWindowTitle(APPLICATION_NAME);
    CustomFrame::setBackgroundImage("images/background_dark.png");

    tabRx = new TabRx(rx);
    tabPidTurning = new TabPidTurning(pid_turning);
    tabSensor = new TabSensor(sensor);
    tabConfig = new TabConfig(config);
    tabFirmware = new TabFirmware(firmware);

    tabRx->move(btnTabPos->pos());
    tabPidTurning->move(btnTabPos->pos());
    tabSensor->move(btnTabPos->pos());
    tabConfig->move(btnTabPos->pos());
    tabFirmware->move(btnTabPos->pos());

    tabRx->setWindow(this);
    tabPidTurning->setWindow(this);
    tabSensor->setWindow(this);
    tabConfig->setWindow(this);
    tabFirmware->setWindow(this);

    progressDialog = new ProgressDialog(this);
    progressDialog->hide();

    btnManualConnect->hide();
}

MainWidget::~MainWidget()
{
}

void MainWidget::registerUiConnections()
{
    connect(mainTab, SIGNAL(currentChanged(int)),
            this, SLOT(animationWhenTabChange(int)));

    connect(cbPortNames, SIGNAL(activated(int)),
            this, SLOT(animationShowHideButtonManualConnect()));
}

void MainWidget::setToolTipForWidgets()
{

}

void MainWidget::updateLabelPortStatus(bool isConnected)
{
    if (isConnected) {
        lbPortStatus->setStyleSheet(style_label_connected);
    } else {
        lbPortStatus->setStyleSheet(style_label_disconnected);
    }
}

void MainWidget::updatePortNameConnected(const QString &portName)
{
    if (portName.isEmpty())
        cbPortNames->setCurrentIndex(0);
    else
        cbPortNames->setCurrentText(portName);
}

void MainWidget::updateListPorts(const QStringList &texts)
{
    QString txt = cbPortNames->currentText();
    cbPortNames->clear();
    cbPortNames->addItems(texts);
    cbPortNames->insertItem(0, "Auto");
    cbPortNames->setCurrentText(txt);
}

void MainWidget::showProgressDialog(ProgressDialog::Type state,
                                        QString content)
{
    progressDialog->show();
    progressDialog->setContent(content);
    progressDialog->changeState(state);
}

void MainWidget::hideProgressDialog()
{
    progressDialog->hide();
}

void MainWidget::animationWhenTabChange(int tabIndex)
{
    QWidget *widget = NULL;
    switch(tabIndex)
    {
    case Quad3D::TabRx:
        widget = tabRx;
        break;
    case Quad3D::TabConfig:
        widget = tabConfig;
        break;
    case Quad3D::TabPidTuning:
        widget = tabPidTurning;
        break;
    case Quad3D::TabSensor:
        widget = tabSensor;
        break;
    case Quad3D::TabFirmware:
        widget = firmware;
        break;
    default:
        return;
    }

    QPropertyAnimation *anim = new QPropertyAnimation(widget, "pos");

    anim->setKeyValueAt(0, QPointF(widget->x() + 15, widget->y()));
    anim->setKeyValueAt(0.8, QPointF(widget->x() - 3, widget->y()));
    anim->setKeyValueAt(1, widget->pos());
    anim->setDuration(ANIMATION_DURATION_WHEN_CHANGE_TAB);

    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void MainWidget::animationShowHideButtonManualConnect()
{
    if (cbPortNames->currentIndex() != 0) {

        if (btnManualConnect->isVisible()) {
            return;
        }

        btnManualConnect->show();

        QPropertyAnimation *anim = new QPropertyAnimation(btnManualConnect, "pos");
        anim->setStartValue(btnManualConnect->pos());
        anim->setEndValue(QPoint(btnManualConnect->x(), btnManualConnect->y() - 41));
        anim->setDuration(150);

        anim->start(QAbstractAnimation::DeleteWhenStopped);
    }
    else {
        if (! btnManualConnect->isVisible()) {
            return;
        }

        QPropertyAnimation *anim = new QPropertyAnimation(btnManualConnect, "pos");
        anim->setStartValue(btnManualConnect->pos());
        anim->setEndValue(QPoint(btnManualConnect->x(), btnManualConnect->y() + 41));
        anim->setDuration(150);

        connect(anim, SIGNAL(finished()), btnManualConnect, SLOT(hide()));

        anim->start(QAbstractAnimation::DeleteWhenStopped);
    }
}
