#include "tabrx.h"
#include "ui_tabrx.h"
#include "rxmonitorview.h"
#include "global.h"

#include <QPropertyAnimation>

#define BIND_DURATION_ANIMATION 200


TabRx::TabRx(QWidget *parent) :
    BaseTabWidget(parent)
{
    setupUi(this);

    rxViewLeft = new RxMonitorView(RX_MONITOR_WIDTH, RX_MONITOR_HEIGHT,
                                   RX_SENSOR_WIDTH, RX_SENSOR_HEIGHT,
                                   this->stackedWidget->widget(0));
    rxViewLeft->move(btnRxLeft->pos());
    rxViewLeft->setRangeValue(RX_MIN_VAL, RX_MAX_VAL);
    rxViewLeft->setLocation(RX_LOCATION_LEFT);
    rxViewLeft->setObjectName(QStringLiteral("rxViewLeft"));

    rxViewRight = new RxMonitorView(RX_MONITOR_WIDTH, RX_MONITOR_HEIGHT,
                                    RX_SENSOR_WIDTH, RX_SENSOR_HEIGHT,
                                    this->stackedWidget->widget(0));
    rxViewRight->move(btnRxRight->pos());
    rxViewRight->setRangeValue(RX_MIN_VAL, RX_MAX_VAL);
    rxViewRight->setLocation(RX_LOCATION_RIGHT);
    rxViewRight->setObjectName(QStringLiteral("rxViewRight"));

    btGroupRxType = new QButtonGroup(this);
    btGroupRxType->addButton(rbTxTraditional, Quad3D::Tradition);
    btGroupRxType->addButton(rbTxSbus, Quad3D::SBus);
    btGroupRxType->addButton(rbTxPpm, Quad3D::PPM);
    btGroupRxType->addButton(rbTxDsm2, Quad3D::DSM2);
    btGroupRxType->addButton(rbTxDsmx, Quad3D::DSMX);

    connect(rxViewLeft, SIGNAL(_sensorValueChanged(qint32, qint32)),
            this, SLOT(rxView_sensor_value_changed(qint32, qint32)));
    connect(rxViewRight, SIGNAL(_sensorValueChanged(qint32, qint32)),
            this, SLOT(rxView_sensor_value_changed(qint32, qint32)));

    connect(sliderGear, SIGNAL(valueChanged(int)),
            this, SLOT(slider_valueChanged(int)));
    connect(sliderAux, SIGNAL(valueChanged(int)),
            this, SLOT(slider_valueChanged(int)));

    connect(btGroupRxType, SIGNAL(buttonToggled(int,bool)),
            this, SLOT(animationWhenBindRx()));

    connect(cbTxMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(cbTxMode_current_index_changed(int)));

    connect(cbViewMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(cbViewMode_current_index_changed(int)));

    btnBind->hide();
    cbTxMode->setCurrentIndex(1);

    m_rxChannelMapping.insertMulti(rxViewLeft, sliderThrottle);
    m_rxChannelMapping.insertMulti(rxViewLeft, sliderRudder);
    m_rxChannelMapping.insertMulti(rxViewRight, sliderAileron);
    m_rxChannelMapping.insertMulti(rxViewRight, sliderElevator);
}

TabRx::~TabRx()
{
}

void TabRx::animationWhenBindRx()
{
    int rxType = btGroupRxType->checkedId();
    if (rxType == Quad3D::DSM2 || rxType == Quad3D::DSMX) {

        if (btnBind->isVisible()) {
            return;
        }

        btnBind->show();

        QPropertyAnimation *anim = new QPropertyAnimation(btnBind, "pos");
        anim->setStartValue(btnBind->pos());
        anim->setEndValue(QPoint(btnBind->x(), btnBind->y() + 72));
        anim->setDuration(BIND_DURATION_ANIMATION);

        anim->start(QAbstractAnimation::DeleteWhenStopped);
    }
    else {
        if (! btnBind->isVisible()) {
            return;
        }

        QPropertyAnimation *anim = new QPropertyAnimation(btnBind, "pos");
        anim->setStartValue(btnBind->pos());
        anim->setEndValue(QPoint(btnBind->x(), btnBind->y() - 72));
        anim->setDuration(BIND_DURATION_ANIMATION);

        connect(anim, SIGNAL(finished()), btnBind, SLOT(hide()));

        anim->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

void TabRx::rxView_sensor_value_changed(qint32 x, qint32 y)
{
    QString xStr, yStr;

    if (x < RX_MIN_VAL || x > RX_MAX_VAL) {
        xStr = "∞";
    } else {
        xStr = QString::number(x - RX_CENTER_VAL);
    }

    if (y < RX_MIN_VAL || y > RX_MAX_VAL) {
        yStr = "∞";
    } else {
        yStr = QString::number(y - RX_CENTER_VAL);
    }

    RxMonitorView *view = qobject_cast<RxMonitorView*> (sender());
    if (view == rxViewLeft) {
        lbRxLeft_x->setText(xStr);
        lbRxLeft_y->setText(yStr);
    }
    else if (view == rxViewRight) {
        lbRxRight_x->setText(xStr);
        lbRxRight_y->setText(yStr);
    }

    foreach (QWidget *w, m_rxChannelMapping.values(view)) {
        QSlider *slider = qobject_cast<QSlider*> (w);
        if (slider == sliderThrottle) {
            slider->setValue(y);
            lbSliderThrottle->setText(QString::number(y) + " µs");
            if (view == rxViewLeft) {
                lbRxLeft_y->setText(QString::number(y));
            }
            else if (view == rxViewRight) {
                lbRxRight_y->setText(QString::number(y));
            }
        }
        else if (slider == sliderRudder) {
            slider->setValue(x - RX_CENTER_VAL);
            lbSliderRudder->setText(QString::number(x - RX_CENTER_VAL));
        }
        else if (slider == sliderAileron) {
            slider->setValue(x - RX_CENTER_VAL);
            lbSliderAileron->setText(QString::number(x - RX_CENTER_VAL));
        }
        else if (slider == sliderElevator) {
            slider->setValue(y - RX_CENTER_VAL);
            lbSliderElevator->setText(QString::number(y - RX_CENTER_VAL));
        }
    }
}

void TabRx::slider_valueChanged(int value)
{
    QSlider *slider = qobject_cast<QSlider*> (sender());

    if (slider == sliderGear) {
        lbSliderGear->setText(QString::number(value) + " µs");
    }
    else if (slider == sliderAux) {
        lbSliderAux->setText(QString::number(value) + " µs");
    }
}

void TabRx::cbTxMode_current_index_changed(int index)
{
    m_rxChannelMapping.clear();
    switch (index) {
    case 0:
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderRudder);
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderElevator);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderAileron);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderThrottle);
        break;

    case 1:
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderRudder);
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderThrottle);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderAileron);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderElevator);
        break;

    case 2:
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderAileron);
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderElevator);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderRudder);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderThrottle);
        break;

    case 3:
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderAileron);
        m_rxChannelMapping.insertMulti(rxViewLeft, sliderThrottle);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderRudder);
        m_rxChannelMapping.insertMulti(rxViewRight, sliderElevator);
        break;

    default: return;
    }
}

void TabRx::cbViewMode_current_index_changed(int index)
{
    stackedWidget->setCurrentIndex(index);
    cbTxMode->setVisible(!index);
    lbTxMode->setVisible(!index);
}
