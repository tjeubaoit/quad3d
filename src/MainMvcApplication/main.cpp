#include "maincontroller.h"
#include "mainwidget.h"
#include "resourcemanager.h"
#include "global.h"
#include "resources.h"

#include <QApplication>
#include <QDebug>
#include <QMainWindow>
#include <QSplashScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName(APPLICATION_NAME);
    a.setApplicationVersion(APPLICATION_VERSION);
    a.setOrganizationName(APPLICATION_ORGANIZATION);
    a.setWindowIcon(QIcon(URL_APP_ICON));

#ifdef RELEASE_VERSION
    QSplashScreen splash(QPixmap(url_splash_screen), Qt::WindowStaysOnTopHint);
    splash.show();
    ResourceManager::instance(url_xml_strings_en);
//    splash.showMessage(res->getValue(3), Qt::AlignRight | Qt::AlignBottom, Qt::white);

    MainController controller;
    QTimer::singleShot(2000, &controller, SLOT(showMainWindow()));
    QTimer::singleShot(2000, &splash, SLOT(hide()));
#else
    MainController controller;
    controller.showMainWindow();
#endif

    return a.exec();
}



