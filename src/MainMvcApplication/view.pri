SOURCES += tabrx.cpp \
    mainwidget.cpp \
    tabpidturning.cpp \
    tabsensor.cpp \
    tabconfig.cpp \
    basetabwidget.cpp \
    tabfirmware.cpp

HEADERS  += tabrx.h \
    mainwidget.h \
    tabpidturning.h \
    basetabwidget.h \
    tabsensor.h \
    tabconfig.h \
    extraadaptors.h \
    tabfirmware.h

FORMS    += \
    tabrx.ui \
    mainwidget.ui \
    tabpidturning.ui \
    tabsensor.ui \
    tabconfig.ui \
    tabfirmware.ui
