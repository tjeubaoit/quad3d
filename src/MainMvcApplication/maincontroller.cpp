#include "maincontroller.h"
#include "mainwidget.h"
#include "global.h"
#include "tabrx.h"
#include "tabsensor.h"
#include "tabfirmware.h"
#include "tabpidturning.h"
#include "tabconfig.h"
#include "serialmanager.h"
#include "parametermodel.h"
#include "adaptormodel.h"
#include "modelbuilder.h"
#include "extraadaptors.h"
#include "basetabwidget.h"
#include "lineeditadaptor.h"
#include "address.h"

#include <QTimer>
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QStandardPaths>


MainController::MainController(QObject *parent) :
    AbstractSerialController(parent),
    ui(new MainWidget),
    m_serialManager(new SerialManager),
    m_serialThread(m_serialManager),
    m_firmwareController(ui, m_serialManager)
{
    setupUi();
    intializeModels();
    registerConnections();
    loadApplicationConfig();

    m_serialManager->addProductId(PRODUCT_ID);
    m_serialManager->addVendorId(VENDOR_ID);

    m_serialThread.start(QThread::HighestPriority);
}

MainController::~MainController()
{
    saveApplicationConfig();

    if (m_serialThread.isRunning()) {
        m_serialThread.quit();
        m_serialThread.wait(1000);
    }
    m_serialManager->deleteLater();

    m_paramModel->deleteLater();
    m_adaptorModel->deleteLater();

    delete m_appConfig;
}

void MainController::onConnect()
{
    ui->updateLabelPortStatus(true);
    ui->cbPortNames->setEnabled(false);
    ui->updatePortNameConnected(m_serialManager->getPortName());

    m_updateHelper.starLoop();
}

void MainController::onDisconnect()
{
    ui->updateLabelPortStatus(false);
    ui->cbPortNames->setEnabled(true);

    m_updateHelper.endLoop();
}

void MainController::showMainWindow()
{
    m_window.show();
}

void MainController::handleParamsChanged(const QVariant &val)
{
    ui->tabFirmware->lbSerialNumber->setText(val.toString());
    m_updateHelper.update();
}

void MainController::handleParamsChanged(qint32 addr, qint32 val)
{
    m_adaptorModel->setAdaptorValue(addr, val);
    m_paramModel->setParamValue(addr, val);
    m_updateHelper.update();
}

void MainController::handlePortStatusChanged(int status)
{
    switch (status) {
    case SerialManager::PortConnected:
        this->onConnect();
        break;

    case SerialManager::PortDisconneted:
        this->onDisconnect();
        break;
    }
}

void MainController::handlePortEventChanged(int eventType, const QVariant &data)
{
    if (eventType == SerialManager::PortListAvailableChanged) {
        ui->updateListPorts(data.toStringList());
    }
}

void MainController::requestUpdateAddress()
{
    const int tabIndex = ui->mainTab->currentIndex();
    QStringList list = m_updateHelper.getAddressList(tabIndex).split(",");

    foreach (QString text, list) {
        if (text.isEmpty()) continue;
        emit _needReadUpdateParams(text.toInt());
    }
    if (ui->tabFirmware->lbSerialNumber->text() == "N/A") {
        emit _needReadUpdateParams(QString::number(ADDR_SERIAL_NUMBER));
    }
}

void MainController::requestWriteConfigChanges(int addr, int val, bool isBlocking)
{
    m_paramModel->setParamValue(addr, val);
    qDebug() << "write -> " << addr << "=" << val;
    if (m_serialManager->isPortConnected() && addr > 0) {
        m_updateHelper.pause(WRITE_CONFIG_DELAY);
        m_serialManager->writeConfig(addr, val, isBlocking);
    }
}

void MainController::handleUpdateFlashFinish()
{
    this->grantControlAccessOf(m_serialManager);
    emit _needReconnect();
}

void MainController::mainTab_current_changed(int)
{

}

void MainController::btnSetCenter_clicked()
{
    ui->showProgressDialog(ProgressDialog::RingType, "Rx Set Center");
    QTimer::singleShot(SET_CENTRE_DELAY, ui, SLOT(hideProgressDialog()));

    requestWriteConfigChanges(ADDR_RX_SETCENTER, 1);
    m_updateHelper.pause(SET_CENTRE_DELAY);
}

void MainController::btnBind_clicked()
{
    int ret = QMessageBox::warning(ui, "Bind RX Satellite",
            "You need restart RC after bind to apply change. Do you want to continue ?",
            QMessageBox::Yes, QMessageBox::No);
    if (ret == QMessageBox::No) return;

    qint32 val = (Quad3D::DSM2 == ui->tabRx->btGroupRxType->checkedId()) ? 3 : 9;
    requestWriteConfigChanges(ADDR_RX_BIND, val);
    m_updateHelper.pause(BIND_RX_DELAY);
}

void MainController::cbTxMode_current_index_changed(int index)
{
    switch (index) {
    case 0:
        m_adaptorModel->addAdaptor(ADDR_RX_THROTTLE,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewRight));
        break;

    case 1:
        m_adaptorModel->addAdaptor(ADDR_RX_THROTTLE,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewRight));
        break;

    case 2:
        m_adaptorModel->addAdaptor(ADDR_RX_THROTTLE,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewLeft));
        break;

    case 3:
        m_adaptorModel->addAdaptor(ADDR_RX_THROTTLE,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->tabRx->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->tabRx->rxViewLeft));
        break;

    default:
        return;
    }
}

void MainController::btnSetGainDefault_clicked()
{
    int ret = QMessageBox::question(ui, "Default Gain",
                          "Set all gain value to default ?",
                          QMessageBox::Ok, QMessageBox::Cancel);
    if (ret == QMessageBox::Ok) {
        requestWriteConfigChanges(ADDR_GAIN_DEFAULT, 1, true);
    }
}

void MainController::btnCalib_clicked()
{
    requestWriteConfigChanges(ADDR_CALIB_ACCEL, 1);
}

void MainController::btnFirmware_clicked()
{
    QString firmwarePath = m_appConfig->get("last-firmware-dir",
                                            QCoreApplication::applicationDirPath());
    QString filePath = QFileDialog::getOpenFileName(
                ui, "Open firmware files", firmwarePath, "*.vtk");

    QFile file(filePath);
    if (!file.exists()) return;
    if (!file.open(QIODevice::ReadOnly)) return;
    if (!file.fileName().endsWith(".vtk")) return;

    m_appConfig->put("last-firmware-dir", filePath);

    QByteArray data = file.readAll();
    if (data.isEmpty()) return;
    file.close();

    m_updateHelper.endLoop();
    QCoreApplication::removePostedEvents(this);

    QString portName;
    if (m_serialManager->isPortConnected()) {
        portName = m_serialManager->getPortName();
        m_serialManager->closeConnection();
    }

    m_firmwareController.grantControlAccessOf(m_serialManager);
    m_firmwareController.setFirmwareData(data);
    m_firmwareController.enterUpdateLoop(portName);
}

void MainController::btnManualConnect_clicked()
{
    emit _needManualConnect(ui->cbPortNames->currentText());
}

void MainController::cbPortNames_activated(int index)
{
    if (index == 0)
        emit _needReconnect();
}

void MainController::intializeModels()
{
    ModelBuilder builder(ui);
    m_adaptorModel = builder.getAdaptorModel();
    m_paramModel = builder.getParameterModel();

    foreach (QObject* obj, m_adaptorModel->getListAdaptors()) {
        if (qobject_cast<SliderAdaptor*> (obj)) {

            SliderAdaptor* adaptor = qobject_cast<SliderAdaptor*> (obj);
            QSlider* slider = qobject_cast<QSlider*> (adaptor->inputField());

            BaseTabWidget *ui = qobject_cast<BaseTabWidget*> (slider->parentWidget());
            Q_ASSERT(ui);

            connect(slider, SIGNAL(valueChanged(int)),
                    ui, SLOT(slider_valueChanged(int)));

            if (qobject_cast<SliderWithLineEditAdaptor*> (obj)) {

                SliderWithLineEditAdaptor *sa =
                        qobject_cast<SliderWithLineEditAdaptor*> (obj);
                QLineEdit *edit = sa->getLineEdit();

                connect(edit, SIGNAL(textEdited(QString)),
                        ui, SLOT(lineEdit_textEdited(QString)));
                connect(edit, SIGNAL(returnPressed()),
                        ui, SLOT(hideLineEditAlert()));
                connect(sa->getLineEditAdaptor(), SIGNAL(_editLostFocus(QObject*)),
                        ui, SLOT(hideLineEditAlert()));
            }
        }
        else if (qobject_cast<ButtonGroupWithVerifyAdaptor*> (obj)) {

            ButtonGroupWithVerifyAdaptor* adaptor =
                    qobject_cast<ButtonGroupWithVerifyAdaptor*> (obj);
            ButtonGroup* btgroup = qobject_cast<ButtonGroup*> (
                        adaptor->inputField());

            BaseTabWidget *ui = qobject_cast<BaseTabWidget*> (btgroup->parent());
            Q_ASSERT(ui);

            connect(adaptor, SIGNAL(_buttonClicked(QObject*,int,bool*)),
                    ui, SLOT(buttonGroupAdaptor_button_clicked(QObject*,int,bool*)));
        }
    }

    connect(m_adaptorModel, SIGNAL(_adaptorValueChanged(int,int,bool)),
            this, SLOT(requestWriteConfigChanges(int,int,bool)));

    m_updateHelper.setUpdateInterval(TIMER_UPDATE_UI);
    m_updateHelper.setErrorInterval(5000);
}

void MainController::registerConnections()
{
    connect(ui->mainTab, SIGNAL(currentChanged(int)),
            this, SLOT(mainTab_current_changed(int)));

    // in-bound signal connections

    connect(this, SIGNAL(_needReconnect()),
            &m_serialThread, SLOT(tryReconnect()));

    connect(this, SIGNAL(_needManualConnect(QString)),
            &m_serialThread, SLOT(tryManualConnect(QString)));

    connect(this, SIGNAL(_needReadUpdateParams(QVariant)),
            m_serialManager, SLOT(readSingleAddress(QVariant)));

    // out-bound signal connections

    connect(&m_serialThread, SIGNAL(started()),
            &m_serialThread, SLOT(tryAutoConnect()));

    connect(m_serialManager, SIGNAL(_portStatusChanged(int)),
            this, SLOT(handlePortStatusChanged(int)));
    connect(m_serialManager, SIGNAL(_portEventChanged(int,QVariant)),
            this, SLOT(handlePortEventChanged(int,QVariant)));

    connect(m_serialManager, SIGNAL(_responseToUpdateUi(qint32,qint32)),
            this, SLOT(handleParamsChanged(qint32,qint32)));
    connect(m_serialManager, SIGNAL(_responseToUpdateUi(QVariant)),
            this, SLOT(handleParamsChanged(QVariant)));

    connect(&m_updateHelper, SIGNAL(paramValues_need_update()),
            this, SLOT(requestUpdateAddress()));
    connect(&m_updateHelper, SIGNAL(paramValues_out_of_date()),
            this, SIGNAL(_needReconnect()));

    // ui connections

    connect(ui->tabRx->btnSetCenter, SIGNAL(clicked()),
            this, SLOT(btnSetCenter_clicked()));
    connect(ui->tabRx->btnBind, SIGNAL(clicked()),
            this, SLOT(btnBind_clicked()));
    connect(ui->tabRx->cbTxMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(cbTxMode_current_index_changed(int)));

    connect(ui->btnManualConnect, SIGNAL(clicked()),
            this, SLOT(btnManualConnect_clicked()));
    connect(ui->cbPortNames, SIGNAL(activated(int)),
            this, SLOT(cbPortNames_activated(int)));

    connect(ui->tabConfig, SIGNAL(_motorRotationConfiguration_changed(int)),
            ui->tabSensor, SLOT(motorRotationConfiguration_changed(int)));

    connect(ui->tabPidTurning->btnSetGainDefault, SIGNAL(clicked()),
            this, SLOT(btnSetGainDefault_clicked()));

    connect(ui->tabSensor->btnCalib, SIGNAL(clicked()),
            this, SLOT(btnCalib_clicked()));

    connect(ui->tabFirmware->btnFirmware, SIGNAL(clicked()),
            this, SLOT(btnFirmware_clicked()));

    // update firmware connections

    connect(&m_firmwareController, SIGNAL(_needUpdateFlash(QByteArray,QString)),
            &m_serialThread, SLOT(startUpdateFirmware(QByteArray,QString)));
    connect(&m_firmwareController, SIGNAL(_taskFinished()),
            this, SLOT(handleUpdateFlashFinish()));
}

void MainController::setupUi()
{
    ui->registerUiConnections();
    ui->setToolTipForWidgets();

    ui->mainTab->setCurrentIndex(0);
    ui->updateLabelPortStatus(false);

    m_window.setWindowFlags(Qt::FramelessWindowHint
                            | Qt::WindowMinimizeButtonHint);
    m_window.setCentralWidget(ui);
}

void MainController::loadApplicationConfig()
{
    QString path = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0];
    QDir dir(path);
    if (!dir.exists()) {
        dir.mkpath(path);
    }
    m_appConfig = new AppConfigManager(path + "/apollo3d.afc");

    if (m_appConfig->hasKey("txmode")) {
        int index = m_appConfig->get("txmode").toInt();
        if (index < ui->tabRx->cbTxMode->count())
            ui->tabRx->cbTxMode->setCurrentIndex(index);
    }
    if (m_appConfig->hasKey("lastpos")) {
        QStringList list = m_appConfig->get("lastpos").split(",");
        QPoint point(list[0].toInt(), list[1].toInt());
        m_window.move(point);
    }
}

void MainController::saveApplicationConfig()
{
    m_appConfig->put("txmode", QString::number(ui->tabRx->cbTxMode->currentIndex()));
    QString posText = QString("%1,%2").arg(m_window.pos().x()).arg(m_window.pos().y());
    m_appConfig->put("lastpos", posText);
    m_appConfig->saveConfigToFile();
}
