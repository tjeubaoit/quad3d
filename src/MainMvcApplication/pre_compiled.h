#ifndef PRE_COMPILED_H
#define PRE_COMPILED_H

#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QReadWriteLock>
#include <QtCore/QSemaphore>
#include <QtCore/QWaitCondition>

#include <QtCore/QTextStream>
#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtCore/QHash>
#include <QtCore/QVector>
#include <QtCore/QList>
#include <QtCore/QSet>
#include <QtCore/QVariant>

#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>
#include <QtWidgets/QMenu>
#include <QtWidgets/QToolTip>

#include "global.h"
#include "address.h"
#include "basetabwidget.h"
#include "extraadaptors.h"
#include "global.h"
#include "maincontroller.h"
#include "mainwidget.h"
#include "modelbuilder.h"
#include "parametermodel.h"
#include "resources.h"
#include "tabconfig.h"
#include "tabpidturning.h"
#include "tabrx.h"
#include "tabsensor.h"
#include "ui_tabconfig.h"
#include "ui_tabpidturning.h"
#include "ui_tabrx.h"
#include "ui_tabsensor.h"

#endif // PRE_COMPILED_H
