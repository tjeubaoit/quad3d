#ifndef PARAMUPDATEHELPER_H
#define PARAMUPDATEHELPER_H

#include <QObject>
#include <QTimer>

class ParamUpdateHelper : public QObject
{
    Q_OBJECT
public:
    ParamUpdateHelper(QObject *parent = 0);
    virtual ~ParamUpdateHelper();

    QString getAddressList(int);
    void update();
    void clearErrorChecking();

    void setUpdateInterval(int ms);
    void setErrorInterval(int ms);

Q_SIGNALS:
    void paramValues_out_of_date();
    void paramValues_need_update();

public Q_SLOTS:
    void starLoop();
    void endLoop();
    void pause(int);

private:
    QTimer m_updateTimer, m_checkErrorTimer;
    int *m_updateCounters;
    int m_updateInterval, m_checkErrorInterval;

    void updateCounters(int id);
};

#endif // PARAMUPDATEHELPER_H
