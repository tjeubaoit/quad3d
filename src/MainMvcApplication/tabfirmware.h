#ifndef TABFIRMWARE_H
#define TABFIRMWARE_H

#include "ui_tabfirmware.h"
#include "basetabwidget.h"

class TabFirmware : public BaseTabWidget, public Ui::TabFirmware
{
    Q_OBJECT

public:
    explicit TabFirmware(QWidget *parent = 0);
    ~TabFirmware();
};

#endif // TABFIRMWARE_H
