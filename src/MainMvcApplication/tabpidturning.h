#ifndef TABPIDTURNING_H
#define TABPIDTURNING_H

#include "ui_tabpidturning.h"
#include "basetabwidget.h"

class TabPidTurning : public BaseTabWidget, public Ui::TabPidTurning
{
    Q_OBJECT

public:
    explicit TabPidTurning(QWidget *parent = 0);
    ~TabPidTurning();
};

#endif // TABPIDTURNING_H
