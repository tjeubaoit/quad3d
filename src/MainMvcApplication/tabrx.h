#ifndef TABRX_H
#define TABRX_H

#include "ui_tabrx.h"
#include "basetabwidget.h"

#include <QMap>

class RxMonitorView;
class QButtonGroup;

class TabRx : public BaseTabWidget, public Ui::TabRx
{
    Q_OBJECT

public:
    RxMonitorView *rxViewLeft, *rxViewRight;
    QButtonGroup *btGroupRxType;
    QMap<QWidget*, QWidget*> m_rxChannelMapping;

    explicit TabRx(QWidget *parent = 0);
    ~TabRx();

public Q_SLOTS:
    void animationWhenBindRx();

private Q_SLOTS:
    void slider_valueChanged(int value);

    void rxView_sensor_value_changed(qint32 x, qint32 y);

    void cbTxMode_current_index_changed(int);

    void cbViewMode_current_index_changed(int);
};

#endif // TABRX_H
