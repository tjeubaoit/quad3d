#include "abstractserialcontroller.h"
#include "serialmanager.h"

void AbstractSerialController::grantControlAccessOf(SerialManager *serialManager)
{
    disconnect(serialManager, SIGNAL(_portEventChanged(int,QVariant)), 0, 0);
    disconnect(serialManager, SIGNAL(_portStatusChanged(int)), 0, 0);

    connect(serialManager, SIGNAL(_portEventChanged(int,QVariant)),
            this, SLOT(handlePortEventChanged(int,QVariant)));
    connect(serialManager, SIGNAL(_portStatusChanged(int)),
            this, SLOT(handlePortStatusChanged(int)));
}
