#include "tabconfig.h"
#include "resources.h"
#include "buttongroup.h"
#include "global.h"
#include <iostream>

#include <QPixmap>
#include <QDebug>
#include <QMessageBox>

static const QString config_highlight_url[] = {
    url_highlight_button_config_1,
    url_highlight_button_config_2,
    url_highlight_button_config_3,
    url_highlight_button_config_4,
    url_highlight_button_config_5,
    url_highlight_button_config_6
};
#define NUMBER_CONFIG_OPTIONS sizeof(config_highlight_url)/sizeof(config_highlight_url[0])

class Quad3DButtonGroup : public ButtonGroup
{
public:
    Quad3DButtonGroup(const QString &urlButtonChecked, QWidget *parent = 0)
        : ButtonGroup(urlButtonChecked, parent)
    {
    }

protected:
    QPoint getCheckedIconPosition()
    {
        QAbstractButton *lastChecked = lastButtonChecked();
        return lastChecked->pos();
    }
};

TabConfig::TabConfig(QWidget *parent) : BaseTabWidget(parent)
{
    setupUi(this);

    btGroupConfig = new Quad3DButtonGroup(url_checked_icon, this);
    btGroupConfig->addButton(btnConfig1, 1);
    btGroupConfig->addButton(btnConfig2, 2);
    btGroupConfig->addButton(btnConfig3, 3);
    btGroupConfig->addButton(btnConfig4, 4);
    btGroupConfig->addButton(btnConfig5, 5);
    btGroupConfig->addButton(btnConfig6, 6);

    btGroupConfig->checkedIcon()->setFixedSize(btnConfig1->size());
    btnConfig1->setChecked(true);
    btGroupConfig->setExclusive(true);
    btGroupConfig->setLastButtonChecked(btnConfig1);

    btnConfig1->installEventFilter(this);
    btnConfig2->installEventFilter(this);
    btnConfig3->installEventFilter(this);
    btnConfig4->installEventFilter(this);
    btnConfig5->installEventFilter(this);
    btnConfig6->installEventFilter(this);

    for (size_t i = 0; i < sizeof(m_highLightPixmap)/sizeof(m_highLightPixmap[0]); i++) {
        Q_ASSERT(i < NUMBER_CONFIG_OPTIONS);
        m_highLightPixmap[i] = new QPixmap(config_highlight_url[i]);
    }
    lbConfigHighLight->setPixmap(*m_highLightPixmap[0]);

    cbFlyMode->addItem("2D", Quad3D::Mode_2D);
    cbFlyMode->addItem("3D", Quad3D::Mode_3D);

    cbMotorTest->addItem("No Test", 0);
    cbMotorTest->addItem("Test Motor 1", 1);
    cbMotorTest->addItem("Test Motor 2", 2);
    cbMotorTest->addItem("Test Motor 3", 3);
    cbMotorTest->addItem("Test Motor 4", 4);
    cbMotorTest->addItem("Test Motor 5", 5);
    cbMotorTest->addItem("Test Motor 6", 6);
    cbMotorTest->addItem("Test All", 9);

    updateThrottleWidgets(sliderThrottleMiddle, editThrottleMiddle);

    connect(btGroupConfig, SIGNAL(buttonToggled(int,bool)),
            this, SLOT(buttonGroup_button_toggled(int,bool)));
    connect(cbMotorTest, SIGNAL(activated(int)),
            this, SLOT(cbMotorTest_activated(int)));
    connect(cbFlyMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(comboBox_currentIndexChanged(int)));
}

TabConfig::~TabConfig()
{
    for (size_t i = 0; i < sizeof(m_highLightPixmap)/sizeof(m_highLightPixmap[0]); i++) {
        delete m_highLightPixmap[i];
    }
}

bool TabConfig::eventFilter(QObject *obj, QEvent *evt)
{
    if (evt->type() == QEvent::HoverEnter) {
        QAbstractButton *btn = qobject_cast<QAbstractButton*> (obj);
        updateHighLight(btGroupConfig->id(btn) - 1);
    }
    else if (evt->type() == QEvent::HoverLeave) {
        updateHighLight(btGroupConfig->checkedId() - 1);
    }

    return BaseTabWidget::eventFilter(obj, evt);
}

void TabConfig::buttonGroupAdaptor_button_clicked(QObject *obj, int id, bool *accept)
{
    QAbstractButton *button = (qobject_cast<ButtonGroup*> (obj))->button(id);
    QString msgTitle = "Config fly type";
    QString msgText;

    if (button == btnConfig1) msgText = "Change fly type to QuadX";
    else if (button == btnConfig2) msgText = "Change fly type to Quad+";
    else if (button == btnConfig3) msgText = "Change fly type to HexX";
    else if (button == btnConfig4) msgText = "Change fly type to Hex+";
    else if (button == btnConfig5) msgText = "Change fly type to HexY6";
    else if (button == btnConfig6) msgText = "Change fly type to HexV6";

    int ret = QMessageBox::question(0, msgTitle, msgText,
                                    QMessageBox::Ok, QMessageBox::Cancel);
    *accept = (QMessageBox::Ok == ret);
}

void TabConfig::buttonGroup_button_toggled(int id, bool checked)
{
    if (checked) {
        qDebug() << "ok" << id;
        emit _motorRotationConfiguration_changed(id % 2);
    }
}

void TabConfig::comboBox_currentIndexChanged(int)
{
    updateThrottleWidgets(sliderThrottleMiddle, editThrottleMiddle);
    checkLimitAngle2D->setVisible(cbFlyMode->currentData().toInt() == Quad3D::Mode_2D);
}

void TabConfig::cbMotorTest_activated(int)
{
    int index = cbMotorTest->currentData().toInt();
    if ((index == 5 || index == 6) && btGroupConfig->checkedId() < 3) {
        QMessageBox::warning(getWindow(), "Warning",
                             "This test mode not available with current fly type config",
                             QMessageBox::Ok);
    }
}

void TabConfig::updateHighLight(int currentConfigId)
{
    if (currentConfigId >= 0) {
        Q_ASSERT((size_t)currentConfigId < NUMBER_CONFIG_OPTIONS);
        lbConfigHighLight->setPixmap(*(m_highLightPixmap[currentConfigId]));
        repaint();
    }
}

void TabConfig::updateThrottleWidgets(QSlider *slider, QLineEdit *edit)
{
    if (cbFlyMode->currentData().toInt() == 2) {
        slider->setMinimum(1300);
        slider->setMaximum(1700);
    } else {
        slider->setMinimum(900);
        slider->setMaximum(1300);
    }

    int center = (slider->maximum() - slider->minimum()) / 2 + slider->minimum();
    slider->setValue(center);
    edit->setText(QString::number(center));
    edit->setValidator(new QIntValidator(slider->minimum(), slider->maximum()));
    edit->installEventFilter(this);
}
