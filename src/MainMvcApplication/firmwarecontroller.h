#ifndef FIRMWARECONTROLLER_H
#define FIRMWARECONTROLLER_H

#include "abstractserialcontroller.h"

#include <QObject>
#include <QByteArray>

class MainWidget;
class SerialManager;

class FirmwareController : public AbstractSerialController
{
    Q_OBJECT
public:    
    FirmwareController(MainWidget *ui, SerialManager *serialManager);
    virtual ~FirmwareController() {}

    void enterUpdateLoop(const QString &portName = QString());
    void setFirmwareData(const QByteArray &);

Q_SIGNALS:
    void _needUpdateFlash(const QByteArray&,  const QString &);
    void _taskFinished();

public Q_SLOTS:
    void handleFlashProgressChanged(int);
    void handleFlashStatusChanged(int);

private:
    MainWidget *ui;
    SerialManager *m_serialManager;
    QByteArray m_firmwareData;
};

#endif // FIRMWARECONTROLLER_H
