#ifndef TABSENSOR_H
#define TABSENSOR_H

#include "basetabwidget.h"
#include "ui_tabsensor.h"

class Quad3DGlView;
class LiveGraph;

class TabSensor : public BaseTabWidget, public Ui::TabSensor
{
    Q_OBJECT

public:
    Quad3DGlView *quad3dView;
    LiveGraph *liveGraph_x, *liveGraph_y;

    explicit TabSensor(QWidget *parent = 0);
    ~TabSensor();

public Q_SLOTS:
    void motorRotationConfiguration_changed(int type);

private Q_SLOTS:
    void glView_angleChanged(int kind, float val);

private:
    void initLiveGraphView();
    void initOpenGlView();
};

#endif // TABSENSOR_H
