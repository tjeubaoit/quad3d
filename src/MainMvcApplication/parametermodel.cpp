#include "parametermodel.h"
#include "filehelper.h"
#include <QDebug>

ParameterModel::ParameterModel(QObject *parent)
    : QObject(parent)
{
}

ParameterModel::~ParameterModel()
{
    foreach (Parameter* const p, getParamsList()) {
        delete p;
    }
}

int ParameterModel::size() const
{
    return m_params.size();
}

QList<Parameter*> ParameterModel::getParamsList() const
{
    return m_params.values();
}

void ParameterModel::insertParameter(int addr, Parameter *param)
{
    if (param != 0) {
        if (m_params.contains(addr)) {
            Parameter *p = m_params[addr];
            delete p;
        }
        m_params.insert(addr, param);
    }
}

int ParameterModel::loadParamsFromFile(const QString& filePath)
{
    QMap<int, int> mapBuffer;
    int ret = qLoadParametersFromFile(filePath, &mapBuffer);
    if (ret == 0) {
        foreach (const int key, mapBuffer.keys()) {
            if (m_params.contains(key)) {
                m_params[key]->Value = mapBuffer.value(key);
            }
        }
        return 0;
    }
    return -1;
}

void ParameterModel::saveParamsToFile(const QString& filePath)
{
    QMap<int, int> mapBuffer;
    foreach (const Parameter* p, getParamsList()) {
        if (p != 0) {
            mapBuffer.insert(p->Address, p->Value);
        }
    }
    qSaveParametersToFile(filePath, mapBuffer);
}

int ParameterModel::getParamValue(int addr) const
{
    Q_ASSERT(m_params.contains(addr));
    return m_params.value(addr)->Value;
}

void ParameterModel::setParamValue(int addr, int val)
{
    if (m_params.contains(addr)) {
        m_params[addr]->Value = val;
    }
}


