#include "serialmanagerthread.h"
#include "serialmanager.h"
#include "flashhelper.h"
#include "abstractserialcontroller.h"

#include <QDebug>
#include <QWaitCondition>

SerialManagerThread::SerialManagerThread(SerialManager *serialManager, QObject *parent)
    : QThread(parent),
      m_serialManager(serialManager),
      m_autoConnectMode(true)
{
    moveToThread(this);
    m_serialManager->moveToThread(this);
    m_scanComTimer.moveToThread(this);

    m_scanComTimer.setInterval(1000);

    connect(&m_scanComTimer, SIGNAL(timeout()),
            this, SLOT(scanComTimer_timeout()));
}

SerialManagerThread::~SerialManagerThread()
{
    if (m_scanComTimer.isActive()) {
        m_scanComTimer.stop();
    }
}

void SerialManagerThread::tryAutoConnect()
{
    connect(m_serialManager, SIGNAL(_portStatusChanged(int)),
            this, SLOT(handlePortStatusChanged(int)), Qt::UniqueConnection);

    m_autoConnectMode = true;
    m_scanComTimer.start();
}

void SerialManagerThread::tryManualConnect(const QString &name)
{
    connect(m_serialManager, SIGNAL(_portStatusChanged(int)),
            this, SLOT(handlePortStatusChanged(int)), Qt::UniqueConnection);

    m_autoConnectMode = false;
    m_scanComTimer.stop();

    m_serialManager->checkPort(name);
    m_lastPortNameManualConnect = name;
}

void SerialManagerThread::tryReconnect()
{
    m_serialManager->closeConnection();

    if (m_autoConnectMode) {
        qDebug() << "auto connection timer started";
        m_scanComTimer.start();

        connect(m_serialManager, SIGNAL(_portStatusChanged(int)),
                this, SLOT(handlePortStatusChanged(int)), Qt::UniqueConnection);
    }
    else {
        qDebug() << "reconnect to last port" << m_lastPortNameManualConnect;
        tryManualConnect(m_lastPortNameManualConnect);
    }
}

void SerialManagerThread::startUpdateFirmware(const QByteArray& firmwareData,
                                              const QString &portName)
{
    if (m_scanComTimer.isActive()) m_scanComTimer.stop();

    AbstractSerialController *serialController =
            qobject_cast<AbstractSerialController*> (sender());
    Q_ASSERT(serialController);

    FlashHelper *flashHelper = new FlashHelper;
    connect(flashHelper, SIGNAL(_flashProgressChanged(int)),
            serialController, SLOT(handleFlashProgressChanged(int)));
    connect(flashHelper, SIGNAL(_flashStatusChanged(int)),
            serialController, SLOT(handleFlashStatusChanged(int)));
    connect(flashHelper, SIGNAL(_flashStatusChanged(int)),
            this, SLOT(handleFlashStatusChanged(int)), Qt::QueuedConnection);

    if (m_serialManager->isPortConnected()) {
        m_serialManager->closeConnection();
    }
    if (! m_serialManager->isValidForUpdateFirmware(portName)) {
        emit flashHelper->_flashStatusChanged(FlashHelper::PortCannotOpen);
        return;
    }

    flashHelper->setFlashData(firmwareData);
    flashHelper->setPortName(portName);

    flashHelper->updateFlashNew();
}

void SerialManagerThread::scanComTimer_timeout()
{
    foreach (const QString &portName, m_serialManager->scanCom()) {
        if (m_serialManager->checkPort(portName)) break;
    }
}

void SerialManagerThread::handlePortStatusChanged(int status)
{
    if (status == SerialManager::PortConnected) {
        qDebug() << "serial manager thread recv signal port connected";
        m_scanComTimer.stop();
    }
    else if (status == SerialManager::PortDisconneted && m_autoConnectMode) {
        qDebug() << "serial manager thread recv signal port disconnected";
        m_scanComTimer.start();
    }
}

void SerialManagerThread::handleFlashStatusChanged(int status)
{
    FlashHelper *flashHelper = qobject_cast<FlashHelper*> (sender());
    if (!flashHelper) return;
    QString portName;
    int count = 0;

    switch (status)
    {
    case FlashHelper::StartTestNewFirmware:
        portName = flashHelper->getPortName();
        flashHelper->closePort();
        while (count++ < 3) {
            SLEEP(3000); // wait for device started to checking new firmware
            if (m_serialManager->checkPort(portName, false)) {
                emit flashHelper->_flashStatusChanged(FlashHelper::FlashResultOk);
                return;
            }
        }
        emit flashHelper->_flashStatusChanged(FlashHelper::FlashResultError);
        return;

    case FlashHelper::EraseFlashFail:
    case FlashHelper::JumpFail:
    case FlashHelper::ToltalByteWRong:
    case FlashHelper::Timeout:
    case FlashHelper::FlashResultError:
    case FlashHelper::FlashResultOk:
        delete flashHelper;
        break;

    default:
        return;
    }
}
