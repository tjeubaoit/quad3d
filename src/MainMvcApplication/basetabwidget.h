#ifndef BASETABWIDGET_H
#define BASETABWIDGET_H

#include <QWidget>

class LineEditAlert;

class BaseTabWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BaseTabWidget(QWidget *parent = 0);
    virtual ~BaseTabWidget() {}

    /**
     * @brief setWindow Set the root widget have a window which this widget live in
     * @param widget widget to set is a window
     */
    void setWindow(QWidget * widget) { rootWidget = widget; }

protected Q_SLOTS:
    virtual void buttonGroup_button_toggled(int, bool) {}
    virtual void buttonGroupAdaptor_button_clicked(QObject*, int, bool*) {}

    virtual void slider_valueChanged(int) {}
    virtual void comboBox_currentIndexChanged(int) {}
    virtual void lineEdit_textEdited(const QString&);

    void showLineEditAlert(const QPoint& pos);
    void hideLineEditAlert();

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void mousePressEvent(QMouseEvent *event);
    QWidget *getWindow() const { return  rootWidget; }

private:
    LineEditAlert *lineEditAlert;
    QWidget *rootWidget;
};

#endif // BASETABWIDGET_H
