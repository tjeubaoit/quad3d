#include "paramupdatehelper.h"
#include "global.h"

#include <QStringList>
#include <QDebug>

static const char* QUAD_3D_ADDR[] =
{
    "111,112,113,114,115,116,117:101",
    ":401,402",
    ":201,202,203,204,205,206,207,208",
    "301,302,303:",
    ":503,502"
};

ParamUpdateHelper::ParamUpdateHelper(QObject *parent)
    : QObject(parent),
      m_updateCounters(new int[MAX_TAB_COUNT]),
      m_updateInterval(50),
      m_checkErrorInterval(5000)
{
    m_checkErrorTimer.setSingleShot(true);

    connect(&m_checkErrorTimer, SIGNAL(timeout()),
            this, SIGNAL(paramValues_out_of_date()));

    connect(&m_updateTimer, SIGNAL(timeout()),
            this, SIGNAL(paramValues_need_update()));

    connect(this, SIGNAL(paramValues_out_of_date()),
            this, SLOT(endLoop()));
}

ParamUpdateHelper::~ParamUpdateHelper()
{
    delete[] m_updateCounters;
}

QString ParamUpdateHelper::getAddressList(int index)
{
    Q_ASSERT(index < MAX_TAB_COUNT);
    QString tmp = QUAD_3D_ADDR[index];
    updateCounters(index);

    if (m_updateCounters[index] % 20 == 0) {
        return tmp.replace(":", ",");
    } else {
        return tmp.split(":").first();
    }
}

void ParamUpdateHelper::update()
{
    m_checkErrorTimer.start();
}

void ParamUpdateHelper::clearErrorChecking()
{
    if (m_checkErrorTimer.isActive())
        m_checkErrorTimer.stop();
}

void ParamUpdateHelper::setUpdateInterval(int ms)
{
    m_updateInterval = ms;
}

void ParamUpdateHelper::setErrorInterval(int ms)
{
    m_checkErrorInterval = ms;
}

void ParamUpdateHelper::starLoop()
{
    m_updateTimer.start(m_updateInterval);
    m_checkErrorTimer.start(m_checkErrorInterval);
}

void ParamUpdateHelper::endLoop()
{
    if (m_updateTimer.isActive())
        m_updateTimer.stop();

    if (m_checkErrorTimer.isActive())
        m_checkErrorTimer.stop();
}

void ParamUpdateHelper::pause(int ms)
{
    if (m_updateTimer.isActive()) {
        m_updateTimer.stop();
        QTimer::singleShot(ms, &m_updateTimer, SLOT(start()));
    }
}

void ParamUpdateHelper::updateCounters(int id)
{
    Q_ASSERT(id < MAX_TAB_COUNT);
    for (qint8 i = 0; i < MAX_TAB_COUNT; i++) {
        if (i == id) {
            m_updateCounters[id] += 1;
            if (m_updateCounters[id] >= 200) {
                m_updateCounters[id] = 1;
            }
            continue;
        }
        m_updateCounters[i] = -1;
    }
}
