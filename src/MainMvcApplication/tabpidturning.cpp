#include "tabpidturning.h"

#include <QDebug>

TabPidTurning::TabPidTurning(QWidget *parent) :
    BaseTabWidget(parent)
{
    setupUi(this);

    editPitch_i->setValidator(new QIntValidator(0, 100));
    editPitch_d->setValidator(new QIntValidator(0, 100));
    editPitch_p->setValidator(new QIntValidator(0, 100));
    editRoll_i->setValidator(new QIntValidator(0, 100));
    editRoll_d->setValidator(new QIntValidator(0, 100));
    editRoll_p->setValidator(new QIntValidator(0, 100));
    editYaw->setValidator(new QIntValidator(0, 100));
    editRollPitch->setValidator(new QIntValidator(0, 100));

    editPitch_d->installEventFilter(this);
    editPitch_p->installEventFilter(this);
    editPitch_i->installEventFilter(this);
    editRoll_d->installEventFilter(this);
    editRoll_p->installEventFilter(this);
    editRoll_i->installEventFilter(this);
    editYaw->installEventFilter(this);
    editRollPitch->installEventFilter(this);
}

TabPidTurning::~TabPidTurning()
{
}
