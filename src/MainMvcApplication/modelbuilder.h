#ifndef MODELBUILDER_H
#define MODELBUILDER_H

#include "abstractadaptorfactory.h"

#include <QHash>

class ParameterModel;
class AdaptorModel;
class MainWidget;
class QObject;

class ModelBuilder : public AbstractAdaptorFactory
{
public:
    explicit ModelBuilder(MainWidget* ui);

    ParameterModel* getParameterModel();
    AdaptorModel* getAdaptorModel();

protected:
    AbstractFieldAdaptor *createAdaptor(int, QObject*, int = 0, ...);

private:
    QHash<int, QObject*> m_hash;

    MainWidget *ui;
    ParameterModel* m_paramModel;
    AdaptorModel * m_adaptorModel;

};

#endif // MODELBUILDER_H
