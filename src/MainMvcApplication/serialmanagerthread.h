#ifndef SERIALMANAGERTHREAD_H
#define SERIALMANAGERTHREAD_H

#include <QThread>
#include <QTimer>

class SerialManager;
class QWaitCondition;
class FlashHelper;

class SerialManagerThread : public QThread
{
    Q_OBJECT
public:
    explicit SerialManagerThread(SerialManager *serialManager, QObject *parent = 0);
    virtual ~SerialManagerThread();

public slots:
    void tryAutoConnect();
    void tryManualConnect(const QString&);
    void tryReconnect();

    void startUpdateFirmware(const QByteArray &, const QString&);

private slots:
    void scanComTimer_timeout();

    void handlePortStatusChanged(int);

    void handleFlashStatusChanged(int);

private:
    QTimer m_scanComTimer;

    SerialManager *m_serialManager;

    bool m_autoConnectMode;
    QString m_lastPortNameManualConnect;
};

#endif // SERIALMANAGERTHREAD_H
