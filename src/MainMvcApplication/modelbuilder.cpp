#include "modelbuilder.h"
#include "address.h"
#include "mainwidget.h"
#include "adaptormodel.h"
#include "parametermodel.h"
#include "tabrx.h"
#include "tabpidturning.h"
#include "tabsensor.h"
#include "tabconfig.h"
#include "tabfirmware.h"

#include "extraadaptors.h"
#include "lineeditadaptor.h"
#include "checkboxadaptor.h"
#include "comboboxadaptor.h"
#include "slideradaptor.h"

#include <cstdarg>
#include <QList>

#define BUILD_MODEL(addr, obj) m_adaptorModel->addAdaptor(addr, createAdaptor(addr, obj));
#define BUILD_MODEL_X(addr, obj, extraObj) \
    m_adaptorModel->addAdaptor(addr, createAdaptor(addr, obj, 1, extraObj));


ModelBuilder::ModelBuilder(MainWidget *ui)
    : ui(ui),
      m_paramModel(new ParameterModel),
      m_adaptorModel(new AdaptorModel)
{
    // tab rx

    BUILD_MODEL(ADDR_RX_TYPE, ui->tabRx->btGroupRxType);

    BUILD_MODEL(ADDR_RX_ELEVATOR, ui->tabRx->rxViewRight);
    BUILD_MODEL(ADDR_RX_RUDDER, ui->tabRx->rxViewLeft);
    BUILD_MODEL(ADDR_RX_THROTTLE, ui->tabRx->rxViewLeft);
    BUILD_MODEL(ADDR_RX_AILERONS, ui->tabRx->rxViewRight);

    BUILD_MODEL(ADDR_RX_GEAR, ui->tabRx->sliderGear);
    BUILD_MODEL(ADDR_RX_AUX_1, ui->tabRx->sliderAux);

    // tab pid-turning

    BUILD_MODEL_X(ADDR_GAIN_PITCH_I, ui->tabPidTurning->sliderPitch_i,
                  ui->tabPidTurning->editPitch_i);
    BUILD_MODEL_X(ADDR_GAIN_PITCH_D, ui->tabPidTurning->sliderPitch_d,
                  ui->tabPidTurning->editPitch_d);
    BUILD_MODEL_X(ADDR_GAIN_PITCH_P, ui->tabPidTurning->sliderPitch_p,
                  ui->tabPidTurning->editPitch_p);

    BUILD_MODEL_X(ADDR_GAIN_ROLL_I, ui->tabPidTurning->sliderRoll_i,
                  ui->tabPidTurning->editRoll_i);
    BUILD_MODEL_X(ADDR_GAIN_ROLL_D, ui->tabPidTurning->sliderRoll_d,
                  ui->tabPidTurning->editRoll_d);
    BUILD_MODEL_X(ADDR_GAIN_ROLL_P, ui->tabPidTurning->sliderRoll_p,
                  ui->tabPidTurning->editRoll_p);

    BUILD_MODEL_X(ADDR_GAIN_ROLL_PITCH, ui->tabPidTurning->sliderRollPitch,
                  ui->tabPidTurning->editRollPitch);

    BUILD_MODEL_X(ADDR_GAIN_YAW, ui->tabPidTurning->sliderYaw,
                  ui->tabPidTurning->editYaw);

    // tab sensor

    BUILD_MODEL(ADDR_SENSOR_ROLL, ui->tabSensor->quad3dView);
    BUILD_MODEL(ADDR_SENSOR_PITCH, ui->tabSensor->quad3dView);
    BUILD_MODEL(ADDR_SENSOR_MAGNETIC, ui->tabSensor->quad3dView);
    BUILD_MODEL(ADDR_CALIB_ACCEL, ui->tabSensor->lbCalibStatus);

    // tab config

    BUILD_MODEL(ADDR_FLY_TYPE, ui->tabConfig->btGroupConfig);
    BUILD_MODEL(ADDR_FLY_MODE, ui->tabConfig->cbFlyMode);
    BUILD_MODEL(ADDR_MOTOR_TEST, ui->tabConfig->cbMotorTest);
    BUILD_MODEL(ADDR_ANGLE_LIMIT_2D, ui->tabConfig->checkLimitAngle2D);

    BUILD_MODEL_X(ADDR_THROTTLE_MIDDLE, ui->tabConfig->sliderThrottleMiddle,
                  ui->tabConfig->editThrottleMiddle);

    // tab firmware

    BUILD_MODEL(ADDR_SERIAL_NUMBER, ui->tabFirmware->lbSerialNumber);
    BUILD_MODEL(ADDR_HARDWARE_VERSION, ui->tabFirmware->lbHardwareVersion);
    BUILD_MODEL(ADDR_FIRMWARE_VERSION, ui->tabFirmware->lbFirmwareVersion);
}

ParameterModel *ModelBuilder::getParameterModel()
{
    foreach (int addr, m_adaptorModel->getKeys()) {
        switch (addr) {
        // ignore some read only address
        case ADDR_RX_AILERONS:
        case ADDR_RX_AUX_2:
        case ADDR_RX_ELEVATOR:
        case ADDR_RX_GEAR:
        case ADDR_RX_AUX_1:
        case ADDR_RX_RUDDER:
        case ADDR_RX_THROTTLE:
        case ADDR_RX_SETCENTER:
            break;

        default:
            Parameter *p = new Parameter;
            p->Address = addr;
            p->Value = m_adaptorModel->getAdaptorValue(addr);
            m_paramModel->insertParameter(addr, p);
        }
    }

    return m_paramModel;
}

AdaptorModel *ModelBuilder::getAdaptorModel()
{
    return m_adaptorModel;
}

/**
 * implements abstract factory method
 * @brief createAdaptor
 * @return
 */
AbstractFieldAdaptor *ModelBuilder::createAdaptor(int addr, QObject* obj, int nArgs, ...)
{
    QList<QObject*> extraArgs;
    va_list ap;
    va_start(ap, nArgs);
    for(int i = 1; i <= nArgs; i++) {
        extraArgs.append(va_arg(ap, QObject*));
    }
    va_end(ap);

    switch (addr)
    {
    case ADDR_RX_THROTTLE:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, obj);
    case ADDR_RX_RUDDER:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, obj);
    case ADDR_RX_ELEVATOR:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, obj);
    case ADDR_RX_AILERONS:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, obj);

    case ADDR_GAIN_ROLL_I:
    case ADDR_GAIN_ROLL_D:
    case ADDR_GAIN_ROLL_P:
    case ADDR_GAIN_PITCH_I:
    case ADDR_GAIN_PITCH_D:
    case ADDR_GAIN_PITCH_P:
    case ADDR_GAIN_ROLL_PITCH:
    case ADDR_GAIN_YAW:
    case ADDR_THROTTLE_MIDDLE:
        Q_ASSERT(extraArgs[0] != 0);
        return new SliderWithLineEditAdaptor(obj, extraArgs[0]);

    case ADDR_SENSOR_PITCH:
        return new Quad3DGlAdaptor(Quad3DGlAdaptor::Direction_X, obj);
    case ADDR_SENSOR_ROLL:
        return new Quad3DGlAdaptor(Quad3DGlAdaptor::Direction_Y, obj);
    case ADDR_SENSOR_MAGNETIC:
        return new Quad3DGlAdaptor(Quad3DGlAdaptor::Direction_Z, obj);
    case ADDR_CALIB_ACCEL:
        return new CalibLabelStatusAdaptor(obj);

    case ADDR_HARDWARE_VERSION:
    case ADDR_FIRMWARE_VERSION:
        return new VersionInfoLabelAdaptor(obj);

    default:
        if (qobject_cast<QLineEdit*> (obj)) {
            return new LineEditAdaptor(obj);
        }
        else if (qobject_cast<QSlider*> (obj)) {
            return new SliderAdaptor(obj);
        }
        else if (qobject_cast<QCheckBox*> (obj)) {
            return new CheckBoxAdaptor(obj);
        }
        else if (qobject_cast<QComboBox*> (obj)) {
            return new ComboBoxAdaptor(obj);
        }
        else if (qobject_cast<DoubleDirectSlider*> (obj)) {
            DoubleDirectSlider* dbSlider =
                    qobject_cast<DoubleDirectSlider*> (obj);
            return new DoubleDirectSliderAdaptor(dbSlider->getSlider(),
                                                 dbSlider->getLineEdit());
        }
        else if (qobject_cast<ButtonGroup*> (obj)) {
            return new ButtonGroupWithVerifyAdaptor(obj);
        }
        else if (qobject_cast<QButtonGroup*> (obj)) {
            return new ButtonGroupAdaptor(obj);
        }
        else if (qobject_cast<QToolButton*> (obj)) {
            return new CheckableButtonWithVerifyAdaptor(obj);
        }
        else if (qobject_cast<QLabel*> (obj)) {
            return new LabelAdaptor(obj);
        }
        else Q_ASSERT(false);
    }

    return 0;
}
