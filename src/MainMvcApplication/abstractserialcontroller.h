#ifndef ABSTRACTSERIALCONTROLLER_H
#define ABSTRACTSERIALCONTROLLER_H

#include <QObject>

class SerialManager;

class AbstractSerialController : public QObject
{
    Q_OBJECT
public:
    explicit AbstractSerialController(QObject *parent = 0)
        : QObject(parent)
    {}

    void grantControlAccessOf(SerialManager *serialManager);

public slots:
    virtual void handleParamsChanged(qint32, qint32) {}
    virtual void handlePortStatusChanged(int) {}
    virtual void handlePortEventChanged(int, const QVariant&) {}

    virtual void handleFlashProgressChanged(int) {}
    virtual void handleFlashStatusChanged(int) {}
};

#endif // ABSTRACTSERIALCONTROLLER_H
