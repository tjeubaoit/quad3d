#include "firmwarecontroller.h"
#include "serialmanager.h"
#include "flashhelper.h"
#include "progressdialog.h"
#include "mainwidget.h"

#include <QFile>
#include <QCoreApplication>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>


FirmwareController::FirmwareController(MainWidget *ui, SerialManager *serialManager)
    : AbstractSerialController(0)
{
    this->ui = ui;
    this->m_serialManager = serialManager;
}

void FirmwareController::enterUpdateLoop(const QString &portName)
{
    Q_ASSERT(!m_firmwareData.isEmpty());

    QString name = portName;
    if (portName.isEmpty()) {
        bool ok;
        QStringList portList = m_serialManager->getListPortName();
        name = QInputDialog::getItem(ui, "Cannot auto detect port",
                                     "Select port:", portList, 0, false, &ok);

        if (!ok || portList.isEmpty()) {
            ui->progressDialog->hide();
            emit _taskFinished();
            return;
        }
    }

    ui->showProgressDialog(ProgressDialog::RingType, "Checking port");
    emit _needUpdateFlash(m_firmwareData, name);
}

void FirmwareController::setFirmwareData(const QByteArray &data)
{
    m_firmwareData = data;
}

void FirmwareController::handleFlashStatusChanged(int status)
{
    switch (status)
    {
    case FlashHelper::PortCannotOpen:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, "Error",
                              "Un expected error, cannot open port for update firmware",
                              QMessageBox::Ok);
        enterUpdateLoop();
        break;

    case FlashHelper::StartEraseFlash:
        ui->progressDialog->setContent("Erasing flash");
        ui->progressDialog->changeState(ProgressDialog::RingType);
        break;

    case FlashHelper::StartWriteFirmwareData:
        ui->progressDialog->changeState(ProgressDialog::BarType);
        ui->progressDialog->setContent("Writing firmware data");
        break;

    case FlashHelper::StartTestNewFirmware:
        ui->progressDialog->changeState(ProgressDialog::RingType);
        ui->progressDialog->setContent("Checking new firmware");
        break;

    case FlashHelper::EraseFlashFail:
    case FlashHelper::JumpFail:
    case FlashHelper::ToltalByteWRong:
    case FlashHelper::FlashResultError:
    case FlashHelper::Timeout:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, "Error",
                              "Update firmware failed", QMessageBox::Ok);
        emit _taskFinished();
        break;

    case FlashHelper::FlashResultOk:
        ui->hideProgressDialog();
        QMessageBox::information(ui, "Success",
                                 "Update firmware successfully", QMessageBox::Ok);
        emit _taskFinished();
        break;
    }
}

void FirmwareController::handleFlashProgressChanged(int percent)
{
    ui->progressDialog->setProgressBarValue(percent);
    ui->progressDialog->repaint();
}
