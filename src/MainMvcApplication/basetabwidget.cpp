#include "basetabwidget.h"
#include "lineeditalert.h"

#include <QLineEdit>
#include <QKeyEvent>
#include <QApplication>

BaseTabWidget::BaseTabWidget(QWidget *parent)
    : QWidget(parent)
{
    lineEditAlert = 0;
}

void BaseTabWidget::lineEdit_textEdited(const QString &)
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (sender());
    QPoint point(-120, edit->height() + 2);
    showLineEditAlert(edit->mapTo(this, point));
}

void BaseTabWidget::showLineEditAlert(const QPoint &pos)
{
    if (lineEditAlert == 0) {
        lineEditAlert = new LineEditAlert(this);
        lineEditAlert->setText("Press Enter to send");
        lineEditAlert->setFixedSize(170, 45);
    }
    lineEditAlert->move(pos);
    lineEditAlert->showMe();

}

void BaseTabWidget::hideLineEditAlert()
{
    lineEditAlert->hideMe();
}

bool BaseTabWidget::eventFilter(QObject *obj, QEvent *event)
{
    if (qobject_cast<QLineEdit*> (obj) && event->type() == QEvent::KeyPress
                && reinterpret_cast<QKeyEvent*> (event)->key() == Qt::Key_Escape) {

        QWidget *widget =  QApplication::focusWidget();
        if (widget != 0) {
            widget->clearFocus();
        }
    }

    return false;
}

void BaseTabWidget::mousePressEvent(QMouseEvent *event)
{
    QWidget::mousePressEvent(event);
    QWidget *widget =  QApplication::focusWidget();
    if (widget != 0) {
        widget->clearFocus();
    }
}
