#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "customframe.h"
#include "ui_mainwidget.h"
#include "progressdialog.h"

#include <QWidget>

class TabRx;
class TabPidTurning;
class TabSensor;
class TabConfig;
class TabFirmware;

class MainWidget : public CustomFrame, public Ui::MainWidget
{
    Q_OBJECT

public:
    TabRx *tabRx;
    TabPidTurning *tabPidTurning;
    TabSensor *tabSensor;
    TabConfig *tabConfig;
    TabFirmware *tabFirmware;

    ProgressDialog *progressDialog;

    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

    void registerUiConnections();
    void setToolTipForWidgets();

public Q_SLOTS:
    void updateLabelPortStatus(bool);
    void updatePortNameConnected(const QString& = "");
    void updateListPorts(const QStringList&);

    void showProgressDialog(ProgressDialog::Type state = ProgressDialog::RingType,
                            QString content = "");
    void hideProgressDialog();

    void animationWhenTabChange(int tabIndex);
    void animationShowHideButtonManualConnect();
};

#endif // MAINWIDGET_H
