#-------------------------------------------------
#
# Project created by QtCreator 2014-11-17T10:09:01
#
#-------------------------------------------------

QT       += core gui 3d

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = apollo3d
TEMPLATE = app

#CONFIG += precompile_header
#PRECOMPILED_HEADER = pre_compiled.h

include(model.pri)
include(view.pri)

SOURCES += main.cpp\    
    maincontroller.cpp \
    firmwarecontroller.cpp \
    abstractserialcontroller.cpp

HEADERS  += address.h \
    resources.h \
    global.h \
    pre_compiled.h \
    maincontroller.h \
    firmwarecontroller.h \
    abstractserialcontroller.h

RC_ICONS = Apollo.ico


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibSerial/release/ -lvtserial
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibSerial/debug/ -lvtserial
else:unix: LIBS += -L$$OUT_PWD/../LibSerial/ -lvtserial

INCLUDEPATH += $$PWD/../LibSerial
DEPENDPATH += $$PWD/../LibSerial


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../AdaptorViews/release/ -lvtadaptorviews
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../AdaptorViews/debug/ -lvtadaptorviews
else:unix: LIBS += -L$$OUT_PWD/../AdaptorViews/ -lvtadaptorviews

INCLUDEPATH += $$PWD/../AdaptorViews
DEPENDPATH += $$PWD/../AdaptorViews

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CustomFrame/release/ -lvtcustomframe
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CustomFrame/debug/ -lvtcustomframe
else:unix: LIBS += -L$$OUT_PWD/../CustomFrame/ -lvtcustomframe

INCLUDEPATH += $$PWD/../CustomFrame
DEPENDPATH += $$PWD/../CustomFrame

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/release/ -lvtio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/debug/ -lvtio
else:unix: LIBS += -L$$OUT_PWD/../IOHelper/ -lvtio

INCLUDEPATH += $$PWD/../IOHelper
DEPENDPATH += $$PWD/../IOHelper

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LiveGraph/release/ -lvtlivegraph
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LiveGraph/debug/ -lvtlivegraph
else:unix: LIBS += -L$$OUT_PWD/../LiveGraph/ -lvtlivegraph

INCLUDEPATH += $$PWD/../LiveGraph
DEPENDPATH += $$PWD/../LiveGraph

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CustomWidgets/release/ -lvtcustomwidgets
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CustomWidgets/debug/ -lvtcustomwidgets
else:unix: LIBS += -L$$OUT_PWD/../CustomWidgets/ -lvtcustomwidgets

INCLUDEPATH += $$PWD/../CustomWidgets
DEPENDPATH += $$PWD/../CustomWidgets

RESOURCES += \
    quad3d.qrc
