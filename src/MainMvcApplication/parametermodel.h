#ifndef PARAMETERMODEL_H
#define PARAMETERMODEL_H

#include "parameter.h"

#include <QObject>
#include <QHash>

class ParameterModel : public QObject
{
    Q_OBJECT
public:
    explicit ParameterModel(QObject *parent = 0);
    ~ParameterModel();

    int size() const;
    QList<Parameter*> getParamsList() const;
    int getParamValue(int addr) const;

    int loadParamsFromFile(const QString& filePath);
    void saveParamsToFile(const QString& filePath);

public slots:
    void insertParameter(int addr, Parameter* param);
    void setParamValue(int addr, int val);

private:
    QHash<int, Parameter*> m_params;

};

#endif // PARAMETERMODEL_H
