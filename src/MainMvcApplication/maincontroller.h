#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include "abstractserialcontroller.h"
#include "paramupdatehelper.h"
#include "serialmanagerthread.h"
#include "firmwarecontroller.h"
#include "appconfigmanager.h"

#include <QMainWindow>

#define BIND_RX_DELAY 500
#define SET_CENTRE_DELAY 1000
#define WRITE_CONFIG_DELAY 500
#define TIMER_UPDATE_UI 50


class MainWidget;
class AdaptorModel;
class ParameterModel;
class SerialManager;


class MainController : public AbstractSerialController
{
    Q_OBJECT
public:
    explicit MainController(QObject *parent = 0);
    ~MainController();

Q_SIGNALS:
    void _needReconnect();
    void _needManualConnect(const QString&);
    void _needReadUpdateParams(const QVariant&);

public Q_SLOTS:
    void showMainWindow();

    void handleParamsChanged(const QVariant&);
    void handleParamsChanged(qint32, qint32);
    void handlePortStatusChanged(int);
    void handlePortEventChanged(int, const QVariant&);

    void requestUpdateAddress();
    void requestWriteConfigChanges(int, int, bool = false);

    void handleUpdateFlashFinish();

protected:
    void onConnect();
    void onDisconnect();

private Q_SLOTS:
    void mainTab_current_changed(int);

    void btnSetCenter_clicked();
    void btnBind_clicked();
    void cbTxMode_current_index_changed(int);

    void btnSetGainDefault_clicked();

    void btnCalib_clicked();

    void btnFirmware_clicked();

    void btnManualConnect_clicked();
    void cbPortNames_activated(int);

private:
    void intializeModels();
    void registerConnections();
    void setupUi();
    void loadApplicationConfig();
    void saveApplicationConfig();

    MainWidget *ui;
    QMainWindow m_window;

    SerialManager *m_serialManager;
    SerialManagerThread m_serialThread;

    AdaptorModel *m_adaptorModel;
    ParameterModel *m_paramModel;

    ParamUpdateHelper m_updateHelper;
    FirmwareController m_firmwareController;
    AppConfigManager *m_appConfig;
};

#endif // MAINCONTROLLER_H
