#ifndef ADDRESS_H
#define ADDRESS_H


#define ADDR_RX_TYPE 101
#define ADDR_RX_BIND 102

#define ADDR_RX_AILERONS 111
#define ADDR_RX_ELEVATOR 112
#define ADDR_RX_RUDDER 114
#define ADDR_RX_AUX_1 116
#define ADDR_RX_THROTTLE 113
#define ADDR_RX_GEAR 115
#define ADDR_RX_AUX_2 117

#define ADDR_RX_SETCENTER 118

#define ADDR_GAIN_ROLL_I 202
#define ADDR_GAIN_ROLL_P 201
#define ADDR_GAIN_ROLL_D 203
#define ADDR_GAIN_PITCH_I 205
#define ADDR_GAIN_PITCH_P 204
#define ADDR_GAIN_PITCH_D 206
#define ADDR_GAIN_YAW 208
#define ADDR_GAIN_ROLL_PITCH 207
#define ADDR_GAIN_DEFAULT 209

#define ADDR_SENSOR_ROLL 302
#define ADDR_SENSOR_PITCH 301
#define ADDR_SENSOR_MAGNETIC 303
#define ADDR_CALIB_ACCEL 304

#define ADDR_FLY_MODE 401
#define ADDR_FLY_TYPE 402
#define ADDR_MOTOR_TEST 403
#define ADDR_THROTTLE_MIDDLE 404
#define ADDR_ANGLE_LIMIT_2D 405

#define ADDR_SERIAL_NUMBER 501
#define ADDR_HARDWARE_VERSION 502
#define ADDR_FIRMWARE_VERSION 503

#endif // ADDRESS_H
