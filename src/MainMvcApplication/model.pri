HEADERS  += parameter.h \
    parametermodel.h \
    paramupdatehelper.h \
    modelbuilder.h \
    serialmanagerthread.h

SOURCES += parametermodel.cpp \
    paramupdatehelper.cpp \
    modelbuilder.cpp \
    serialmanagerthread.cpp
