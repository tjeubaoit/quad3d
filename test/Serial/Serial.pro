#-------------------------------------------------
#
# Project created by QtCreator 2014-11-18T14:47:26
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_commandparsertest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_commandparsertest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

DEFINES += UNIT_TEST

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../build/LibSerial/release/ -llibSerial
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../build/LibSerial/debug/ -llibSerial
else:unix: LIBS += -L$$PWD/../../build/LibSerial/ -llibSerial

INCLUDEPATH += $$PWD/../../src/LibSerial
DEPENDPATH += $$PWD/../../src/LibSerial
