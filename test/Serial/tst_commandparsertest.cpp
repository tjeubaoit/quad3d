#include <QString>
#include <QtTest>
#include <QDebug>
#include "quad3dcommandparser.h"
#include "serialbuffer.h"

class CommandParserTest : public QObject
{
    Q_OBJECT

public:
    CommandParserTest();

private Q_SLOTS:
    void initTestCase() {}
    void cleanupTestCase() {}

    void testParseCommand();
    void testParseCommand_data();

    void testGenerateCommand();
    void testGenerateCommand_data();

    void testSerialBuffer();
    void testSerialBuffer_data();
};

CommandParserTest::CommandParserTest()
{
}

void CommandParserTest::testParseCommand()
{
    QFETCH(qint32, address);
    QFETCH(qint32, value);

    Quad3DCommandParser creator;
    qint32 addr = -1, val = -1;
    char testData[16];
    quint32 len = creator.generateCommand(address, value, testData);
    Quad3DCommandParser parser;
    parser.appendData(testData, len);
    parser.nextResult(&addr, &val);

    QCOMPARE(addr, address);
    QCOMPARE(val, value);
}

void CommandParserTest::testParseCommand_data()
{
    QTest::addColumn<qint32>("address");
    QTest::addColumn<qint32>("value");

    QTest::newRow("ok1") << 101 << 1520;
    QTest::newRow("ok2") << 102 << 1;
    QTest::newRow("ok3") << 103 << 0;
    QTest::newRow("ok4") << 201 << 12;
    QTest::newRow("ok5") << 202 << 124;
    QTest::newRow("ok6") << 301 << 140;
    QTest::newRow("ok7") << 305 << 1920;
    QTest::newRow("ok8") << 404 << 323;
    QTest::newRow("ok9") << 412 << 920;
    QTest::newRow("ok10") << 511 << 22;
    QTest::newRow("ok11") << 521 << 150;
    QTest::newRow("ok12") << 555 << 5;
    QTest::newRow("ok13") << 500 << 4000;
}

void CommandParserTest::testGenerateCommand()
{
//    QFETCH(qint32, address);

//    Quad3DCommandParser creator;
//    qint32 addr = -1, val = -1;
//    char testData[16];
//    quint32 len = creator.generateCommand(address, testData);
//    Quad3DCommandParser parser;
//    parser.appendData(testData, len);
//    parser.nextResult(&addr, &val);

//    QCOMPARE(addr, address);
//    QCOMPARE(val, 0);
}

void CommandParserTest::testGenerateCommand_data()
{
//    QTest::addColumn<qint32>("address");

//    QTest::newRow("ko1") << 101;
//    QTest::newRow("ko2") << 102;
//    QTest::newRow("ko3") << 103;
//    QTest::newRow("ko4") << 201;
//    QTest::newRow("ko5") << 202;
//    QTest::newRow("ko6") << 203;
//    QTest::newRow("ko7") << 204;
//    QTest::newRow("ko8") << 301;
//    QTest::newRow("ko9") << 302;
//    QTest::newRow("ko0") << 303;
}

void CommandParserTest::testSerialBuffer()
{
//    QFETCH(char*, array);
//    QFETCH(int, len);
//    QFETCH(int, head);
//    QFETCH(int, tail);

    char array[] = { 170, 202, 101, 136};
    char array1[] = { 1, 2, 3, 4};

    SerialBuffer buffer;

    buffer.append(array, sizeof(array));
    quint8 tmp[8];
    memset(tmp, 0, sizeof(tmp));
    memcpy_s(tmp, 8, buffer.constData(), 4);

    QVERIFY2(tmp[0] == 170, "append ok");
    QVERIFY2(buffer.size() == 4, "size after append");

    buffer.remove(2);
    memset(tmp, 0, sizeof(tmp));
    memcpy_s(tmp, 8, buffer.constData(), 2);

    QVERIFY2(tmp[0] == 101, "remove ok");
    QVERIFY2(buffer.at(0) == 101, "itemAt ok");
    QVERIFY2(buffer.size() == 2, "size after remove");

    buffer.append(array1, sizeof(array1));
    memset(tmp, 0, sizeof(tmp));
    memcpy_s(tmp, 8, buffer.constData(), buffer.size());

    QVERIFY2(tmp[2] == 1, "append ex ok");
    QVERIFY2(buffer.at(2) == 1, "itemAT ex ok");
    QVERIFY2(buffer.size() == 6, "size after append ex");
}

void CommandParserTest::testSerialBuffer_data()
{
//    QTest::addColumn<char*>("array");
//    QTest::addColumn<int>("len");
//    QTest::addColumn<int>("head");
//    QTest::addColumn<int>("tail");

//    char array[] = { 170, 202, 101, 136};

//    QTest::newRow("appendData") << array << 4 << 4 << 4;
}

QTEST_APPLESS_MAIN(CommandParserTest)

#include "tst_commandparsertest.moc"
